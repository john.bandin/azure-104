# Lab 1: Monitoring On-Premises Connectivity Using PowerShell

## Objectives:
- Configure Azure Network Watcher using PowerShell.
- Use PowerShell to monitor the health of a VPN gateway.

## Prerequisites:
- You must have Azure PowerShell module installed.
- You should have at least Contributor access to your Azure subscription.

## Instructions:

### Step 1: Install Azure PowerShell
If not already installed, you can install the Azure PowerShell module by running:
```powershell
Install-Module -Name Az -AllowClobber -Scope CurrentUser
```

### Step 2: Login to Azure

Login to your Azure account and select the subscription:

```powershell
Connect-AzAccount
Select-AzSubscription -SubscriptionName "<Your Subscription Name>"
```

### Step 3: Enable Network Watcher

First, check if Network Watcher is enabled in your region. If not, enable it:

```powershell
$region = "<Your Region>"
$networkWatcher = Get-AzNetworkWatcher -Location $region
if (-not $networkWatcher) {
    $networkWatcher = New-AzNetworkWatcher -Name "NetworkWatcher_$region" -Location $region
}
```

### Step 4: Monitor VPN Gateway
Use Network Watcher's VPN diagnostics to monitor the health of your VPN gateway:

```powershell
$gateway = Get-AzVirtualNetworkGateway -Name "<Your VPN Gateway Name>"
$resourceGroup = "<Your Resource Group Name>"
Start-AzNetworkWatcherResourceTroubleshooting -NetworkWatcher $networkWatcher -TargetResourceId $gateway.Id -ResourceGroupName $resourceGroup
```

## Conclusion:
This lab demonstrated how to set up Azure Network Watcher and initiate VPN diagnostics using PowerShell to monitor on-premises connectivity.
