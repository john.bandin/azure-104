
# Lesson 2.2.8: Applying and Managing Tags Using the Azure Portal

## Objective
Learn how to apply and manage tags on Azure resources using the Azure portal.

## Prerequisites
- Access to the Azure portal with administrative privileges.
- At least one Azure resource created that you can tag.


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. search for and select the resource you want to tag, such as a virtual machine, storage account, or resource group.

Navigate to the specific resource page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/22640bfb-753a-4ac7-b400-47471097dcf0/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=509,103)


3\. Click your VM. Mine for example is "trepa-lab-vm"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/dc073ecf-154d-460e-ab68-c94bf09a0a9b/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=27,163)


4\. On the resource blade, find and click on "Tags" in the settings menu.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/d064adf3-61dc-4fbc-a1ae-549ca527f9a9/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=217,145)


5\. Enter the tag name and value, like the examples listed below. (My photos have different examples and thats okay because tags are built for administrative purposes and are unique to each environment)

- Tag name: Project

- Value: AZ-104 Boot Camp

- Tag name: Department

- Value: IT

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/e7bd1140-d301-437c-8f77-a0b4e374983e/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=438,198)


6\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c1a91598-78a7-4b54-a6d7-7522683dd5b0/ascreenshot.jpeg?tl_px=39,0&br_px=1416,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,255)


7\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/0073467b-e64f-43e6-9a0c-4dbee4d3ea31/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=363,561)


8\. And now you can view the tags you created.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/463dafd0-30cf-4b76-ac1b-603f331abf0c/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=497,412)


## Verify the Tags

1. Stay on the "Tags" page or refresh the page to see the newly created tags listed.

## Conclusion
You have successfully applied tags to a resource using the Azure portal. These tags will help you organize, manage, and track your resources according to the criteria you set.
