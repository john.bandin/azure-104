# Lesson 3.1.3 Generate SAS tokens in Azure

## Prerequisites

- Access to Azure Portal with permissions to manage storage accounts.

## Instructions

1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click "Storage accounts" or search "storage accounts" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/c1c4928e-94fa-4dec-b4b5-7a14df74b783/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=330,63)


3\. Click the storage account you want to generate the SAS tokens for. We will be choosing our "trepatechstoragetest01" storage account.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/e3f7ceed-2c94-4aae-a4b3-4fe01774bc33/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,262)


4\. We will close the Primary Navigation pane on the left to clean up our screen.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/02bf62e5-e4e1-4156-ab71-cf00021af2d9/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=196,38)


5\. Now we scroll down on the left navigation pane and click "shared access signature" underneath the "security + networking" tab.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ab0a5c06-7721-4e0e-9f6f-852f11f1f2c1/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=137,272)


6\. First we will select the "resource type" for this SAS token. We will set this SAS token for a "service" resource type for this demonstration.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6cc8d4f7-dc93-43a3-a2e7-4e028a93558b/ascreenshot.jpeg?tl_px=0,46&br_px=859,527&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=302,212)


7\. Now we will adjust some of the permissions by unclicking the "permanent delete" and "immutable storage" radio buttons.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/bf5845b6-0294-40ad-8be4-d56d54c5c8c0/ascreenshot.jpeg?tl_px=190,1&br_px=1909,962&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,277)


8\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/636bfe77-669b-4201-a685-a28435ade808/ascreenshot.jpeg?tl_px=38,0&br_px=1757,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,272)


9\. Next we will set the "start" and "expiry" dates. We will make this SAS token good for 1 year. \*\*\*NOTE\*\*\* this is just for demonstration purposes. Get with your SOC or Security Team on best practice for these tokens valid lifetimes.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/2c0bfa35-d143-4a33-8436-6201a817693d/ascreenshot.jpeg?tl_px=22,428&br_px=882,909&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


10\. Click the "MM/DD/YYYY" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/65a455b0-ff8d-487c-906a-814d96a78e06/ascreenshot.jpeg?tl_px=0,465&br_px=859,946&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


11\. Next we will input the allowed IP addresses. This will be the source IP addresses that can use this SAS token. Just for this demonstration we will use our Public IP address. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/df34b59e-a1dd-4348-a079-2c63b489d3f2/ascreenshot.jpeg?tl_px=36,487&br_px=895,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,215)


12\. In a new tab, navigate search "what is my public IP"


13\. Click "What Is My IP? Best Way To Check Your Public IP Address"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/e6936b5b-97f6-415c-b121-2c2208226448/ascreenshot.jpeg?tl_px=0,2&br_px=1075,603&force_format=png&wat_scale=95&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=492,265)


14\. Copy your Public IP address to your clipboard.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ba1ca071-3553-4405-945e-c5e4553ad480/ascreenshot.jpeg?tl_px=921,0&br_px=1781,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,181)


15\. Now paste the IP address in the input field "Allowed IP addresses"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/c5a4fa9d-43b4-4840-8026-cebb9abe6e97/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=376,362)


16\. Now we will set which "signing key" we will use.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/725e6a70-b571-4b09-ba66-cd49ed87902f/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=344,301)


17\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/11723358-e892-4185-952d-c9e22a82c942/ascreenshot.jpeg?tl_px=0,46&br_px=859,527&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=383,212)


18\. Click "Generate SAS and connection string"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/0745f547-f8d6-4be7-a065-f66d3960f659/ascreenshot.jpeg?tl_px=18,487&br_px=877,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,407)


## Conclusion

These labs provide basic guidance on generating Shared Access Signatures through the Azure Portal. Customize the parameters and permissions based on your specific requirements. Always ensure to follow best practices for security, especially with regard to expiration dates and permissions.