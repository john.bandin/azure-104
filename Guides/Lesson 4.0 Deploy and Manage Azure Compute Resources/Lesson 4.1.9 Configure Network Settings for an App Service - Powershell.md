# Lab 1: Configure Networking Settings for Azure App Service Using PowerShell

## Objective:

Learn how to configure Azure App Service to integrate with Azure Virtual Network using PowerShell.

## Prerequisites:
- Azure PowerShell module installed
- Existing Azure App Service and Azure Virtual Network

## Instructions

### Step 1: Log in to Azure

Open your PowerShell console and execute the following command to log in to your Azure account:

```powershell
Connect-AzAccount
```

### Step 2: Configure VNet Integration

Integrate your App Service with an existing Virtual Network:

```powershell
# Replace variables with your specific names and IDs
$resourceGroupName = 'MyResourceGroup'
$appServiceName = 'MyAppService'
$vnetName = 'MyVNet'
$subnetName = 'defaultSubnet'

# Fetch the VNet and subnet
$vnet = Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroupName
$subnet = Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet

# Integrate App Service with VNet
Set-AzWebApp -Name $appServiceName -ResourceGroupName $resourceGroupName -VirtualNetwork $vnet -SubnetName $subnetName
```

### Step 3: Configure Access Restrictions

Restrict access to the App Service to allow only specific IP addresses:

```powershell
$rule1 = New-AzWebAppAccessRestrictionRule -Priority 1 -Name 'AllowOffice' -Action Allow -IpAddress '203.0.113.0/24'
$rule2 = New-AzWebAppAccessRestrictionRule -Priority 2 -Name 'DenyAll' -Action Deny -IpAddress '0.0.0.0/0'

Set-AzWebAppAccessRestrictionConfig -ResourceGroupName $resourceGroupName -Name $appServiceName -Rule $rule1, $rule2
```

## Conclusion

You have successfully configured VNet integration and access restrictions for your Azure App Service using PowerShell.