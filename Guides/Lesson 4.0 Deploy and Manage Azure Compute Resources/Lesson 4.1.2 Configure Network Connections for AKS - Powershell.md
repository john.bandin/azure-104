# Lab 1: Configure AKS Network Connections using PowerShell

## Objectives:

- Create an AKS cluster using Azure CNI networking.
- Configure network policies.

## Requirements:
- Azure subscription
- Azure CLI installed
- PowerShell installed

## Instructions

### Step 1: Log in to Azure:

Open your PowerShell and log in to the Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group:

Replace <ResourceGroupName> and <Location> with your desired resource group name and Azure region, respectively.

```powershell
az group create --name <ResourceGroupName> --location <Location>
```

### Step 3: Create a Virtual Network:

Create a virtual network and a subnet dedicated to the AKS cluster.

```powershell
az network vnet create --resource-group <ResourceGroupName> --name AKSVnet --address-prefix 10.0.0.0/8 --subnet-name AKSSubnet --subnet-prefix 10.240.0.0/16
```

### Step 4: Create an AKS Cluster with Azure CNI:

Replace <ClusterName> with your desired AKS cluster name. This step integrates the cluster with the created virtual network.

```powershell
az aks create --resource-group <ResourceGroupName> --name <ClusterName> --network-plugin azure --vnet-subnet-id /subscriptions/<SubscriptionId>/resourceGroups/<ResourceGroupName>/providers/Microsoft.Network/virtualNetworks/AKSVnet/subnets/AKSSubnet --docker-bridge-address 172.17.0.1/16 --dns-service-ip 10.0.0.10 --service-cidr 10.0.0.0/24 --generate-ssh-keys
```

### Step 5: Configure Network Policy:

To enable network policies, use Calico, which can be enabled during or after the cluster creation.

```powershell
az aks create --resource-group <ResourceGroupName> --name <ClusterName> --network-plugin azure --network-policy calico
```

### Step 6: Verify the Installation:

Once the cluster is set up, ensure that all nodes are ready.

```
kubectl get nodes
```

### Step 7: Clean-up Resources:

Delete resources if they are no longer needed to avoid charges.

```powershell
az group delete --name <ResourceGroupName> --yes --no-wait
```