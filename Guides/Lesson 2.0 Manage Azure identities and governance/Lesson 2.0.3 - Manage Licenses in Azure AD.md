# Manage Licenses in Azure Portal



1\.How to Manage Licenses in Azure Portal

Managing licenses through the Azure Portal involves assigning, unassigning, and monitoring usage. This guide covers the steps for managing Microsoft 365 and Azure AD licenses. 

## Prerequisites

\- Administrative access to the Azure portal.

 - Existing Microsoft 365 or Azure AD licenses to manage.


## Step 1: Access Azure Active Directory 

1\. Log in to the Azure Portal (<https://portal.azure.com>). 

2\. Navigate to **Azure Active Directory** from the sidebar. This area allows you to manage all aspects related to identity and access, including licenses.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/a69fe0e6-4659-4d8b-9531-8a2d0245dcd8/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=-8,-1)


3\. Click "Microsoft Entra ID"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/b99787e4-38a4-465f-b3c4-fa37de52f6a1/ascreenshot.jpeg?tl_px=0,286&br_px=859,767&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=93,212)


4\. \## Step 2: Navigate to Licenses 

1\. Within the Azure Active Directory pane, click on **Licenses**. This section displays all the licensing information related to your Azure and Microsoft 365 services. 

2\. Click on **All products** to view all the available licenses.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/c184c4e9-9cbb-4a2a-ab6f-7debaf886127/ascreenshot.jpeg?tl_px=0,454&br_px=859,935&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=107,212)


5\. Click "All products"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2e65c3d6-fe28-4fab-805b-d3179c3ee0ab/ascreenshot.jpeg?tl_px=0,65&br_px=859,546&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=60,212)


### Step 3: Manage Licenses

Assigning a License

From the **All products** list, select the license you want to manage.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/7ad6a32a-b196-48af-922a-fde082a89a02/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=275,202)


Click on **Assign** to add new users to this license.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/56e0c309-19f6-452f-9a4b-a4ae770e2487/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=376,136)


You will be prompted to select users. Choose the users or groups you wish to assign the license to and then configure optional settings, such as location. 

Review and click **Assign** to apply the licenses.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5fafe72f-6656-41ac-a1b6-af29a1a446ec/ascreenshot.jpeg?tl_px=0,4&br_px=859,485&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=124,212)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/6b19e453-207e-4047-b7f2-afb601054c00/ascreenshot.jpeg?tl_px=408,130&br_px=1268,611&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/681e57ef-b61c-418e-8c79-9096fffd4b25/ascreenshot.jpeg?tl_px=425,470&br_px=1285,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,425)


Click "Next : Assignment options >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/0b6dc90c-d42b-4f2a-b58a-cf59c74ed518/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=324,422)


Click "On"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/aac3be1c-688b-4668-aefe-ff1eb3878a37/ascreenshot.jpeg?tl_px=0,36&br_px=859,517&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=367,212)


Click "Next : Review + assign >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/f77a6bce-aa15-4cd3-aec8-66c8fec3e321/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=353,414)


Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/dea5041d-9b47-4556-b0e0-d08930ae2a2c/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=65,425)


Click this icon.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/3776e169-34aa-4fab-bb35-71b49022980f/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=277,203)


### Unassigning a License

 Navigate back to **Licenses** and select **All products**.

Click on the license from which you want to remove users.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/c2fb938f-ca8c-41c6-a347-3117a433e9f8/ascreenshot.jpeg?tl_px=7,0&br_px=867,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,203)


Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/34784298-9fbf-4a21-8705-5ba6a498d1f9/ascreenshot.jpeg?tl_px=0,32&br_px=859,513&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=278,212)


 Under the **Licensed users** tab, select the users from whom the license should be removed. 

Click **Remove licenses** to unassign the licenses from the selected users.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/bedf173f-2c57-4697-9ce3-8e609c7f36bf/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=397,130)


Click "Yes"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/fdb3f667-01f5-4ca9-8fc5-bb0b3721ed1e/ascreenshot.jpeg?tl_px=0,18&br_px=859,499&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=291,212)



