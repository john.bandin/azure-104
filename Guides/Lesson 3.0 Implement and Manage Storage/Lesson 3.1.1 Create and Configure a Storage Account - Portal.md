
# Lesson 3.1.1: Creating and Configuring a Storage Account using the Azure Portal

# Objective
In this lab, you will learn how to create and configure an Azure Storage Account using the Azure Portal.

# Prerequisites
- You must have an Azure subscription.

# Instructions

# Step 1: Access the Azure Portal


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


## Step 2: Create a Resource Group

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/f6aae3a8-0cad-4255-98ef-6ecb11d648d9/ascreenshot.jpeg?tl_px=231,0&br_px=1091,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,200)


3\. From the navigation pane click "Resource Groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/2071dce5-c28a-436a-b39b-c28018313708/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=36,126)


4\. Click "+create" on the top navigation pane under "Resource Groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6c9e918c-d457-44e4-a451-1464fbc6f967/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=23,82)


5\. Choose the Subscription you want with this "resource group".

Next give the "resource group" a unique name that fits your organization.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/7775c819-4a9f-4b69-8123-a7462f7de867/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=296,241)


6\. Type "testStorageGroup"


7\. Ensure you pick the region you want this "resource group" in. And then click "Next : Tags &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/2085b2d6-630b-4efd-9694-aabfc19be323/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=176,565)


8\. Create a "tag" if you want. We will use the same tags we put on all our resources for AZ-104 training.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/77cc5dbc-21f6-4d7f-820b-bdfd26e7a5ba/ascreenshot.jpeg?tl_px=0,71&br_px=859,552&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=159,212)


9\. Click "Project"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/d857bd28-657e-48cb-9e6b-55222d5451e5/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=44,214)


10\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/21e97189-75fb-4e65-a888-1a9afd3f12b9/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=150,157)


11\. Click "AZ-104 Training"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/1cc61a55-46ed-4c95-b1eb-dd322ffc5e9e/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=224,236)


12\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ca4773c9-2487-4c6e-86ea-c7ce9ac8dc83/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=212,562)


13\. Now click "create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/63826ddf-3e02-4f0b-935d-c6e70d56d303/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=37,408)


## Step 3: Create a storage account

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/d9b73d12-fb4f-46cd-a0af-fcbf6ba0a629/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=518,-20)


15\. In the Azure Portal, click on "Storage accounts" or search for "storage accounts"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/02357997-d562-4663-9605-7ff1e5ed7418/ascreenshot.jpeg?tl_px=1,0&br_px=1721,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,189)


16\. Now click "+create" in the top navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/647877d5-d3f5-45ea-9368-d0c9c493acca/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=10,94)


17\. Select the correct subscription where you just made the resource group in the previous steps. Now click on "Resource Group" drop-down and click the "Resource Group" you just made previously.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ed132319-2863-4a34-b76a-cf7c73b0c0c4/ascreenshot.jpeg?tl_px=18,189&br_px=877,670&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


18\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ef60c388-e9bb-45ed-9d6c-8479fda174f9/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=232,281)


19\. Now you will need to enter a globally unique name for your storage account. Storage Account use DNS, so yes this name will have to be unique amongst ALL the storage accounts in the Azure Public cloud. We will use "trepatechstorageaz104"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/473a1149-f3fc-48b1-a3cb-2f3311298e42/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=173,295)


20\. Now choose the region you want this "storage account" to live in. We will choose "(US) East US 2"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6ff0fcb9-b893-4cfc-b891-fa3f2af92db0/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=248,329)


21\. Click "(US) East US 2"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/d84948e4-61a6-4995-b8ba-fcefb371d32e/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=501,447)


22\. Now we must select what kind of "redundancy" we want. We will chose "Local Redundant Storage (LRS)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ab743c18-6c56-4146-a25c-7385cb8b7eb1/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=243,429)


23\. Click "Lowest-cost option with basic protection against server rack and drive failures. Recommended for non-critical scenarios."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/260be8b3-c590-413d-b67e-72fb5c7bd394/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=245,264)


24\. Now we will click "next" through the advanced options. We will explore these other features and options in later sections.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/fb00d901-6e22-4fe9-9fec-ed98967e78b1/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=72,559)


25\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/087458e5-2467-4e71-8ac7-5884f99edfbb/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=68,563)


26\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/b4a37dcf-fb11-43b2-baee-64c4313a0238/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=83,562)


27\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/9a0d9308-05d2-4940-9621-93cdee15b00e/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=72,563)


28\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/4244df01-3d9e-4ea6-ba1c-29be462c8b63/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=76,563)


29\. One thing we will input before we create this "storage account" is set the tags we have been using for all AZ-104 resource.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/825a1f19-9b69-4497-9fa6-35110e36ccd4/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=80,192)


30\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/43dc1a1b-59a2-40ea-a64b-571d903a8e41/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=67,558)


31\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/8f2c6a9f-0ce0-4681-8318-187bf749e2dd/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=189,404)


32\. Once the resource has finished being deployed/created we will click "Go to resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/a5a83091-5993-4a33-bff2-05b70a8a5699/ascreenshot.jpeg?tl_px=197,0&br_px=1917,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=912,63)


33\. Now at our new "storage account" overview page we will scroll down on the left navigation pane and go the "configuration" page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/b998e111-fc54-4bd8-8736-183e9485f0d1/ascreenshot.jpeg?tl_px=0,259&br_px=859,740&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=226,212)


34\. Click "Configuration"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/93b3b7cb-82d6-46c4-907e-1de43fab320e/ascreenshot.jpeg?tl_px=0,478&br_px=859,959&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=117,212)


35\. From here we can choose what access tier we will want to use.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/99a86078-f6fc-4453-8cfb-e43f3d44ec46/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=274,305)

### Step 4: Verify the Storage Account Creation

Navigate to your storage account's overview page to confirm it has been set up with your specified configurations.

## Conclusion

In this lab, you have learned how to create and configure a storage account using the Azure Portal. This includes setting properties like the access tier and verifying your configurations.
