# Lab 2: Configuring Sizing for Azure Container Instances Using the Azure Portal

## Objective
This lab will guide you through the process of setting up CPU and memory configurations for a container instance using the Azure portal.

## Prerequisites
- Azure subscription
- Access to the Azure portal

## Instructions

### Step 1: Log in to Azure Portal
Go to the [Azure Portal](https://portal.azure.com) and log in with your credentials.

### Step 2: Create a Container Instance
1. Click on "Create a resource".
2. Search for and select "Container Instances".
3. Click "Create".

### Step 3: Configure the Container
1. Select the appropriate subscription and either create a new resource group or select an existing one.
2. Enter a name for your container instance and choose the region.
3. Under "Container 1", specify the container image (e.g., `mcr.microsoft.com/azure-cli`).
4. Adjust the CPU and memory settings under the "Size" section.

### Step 4: Review and Create
1. Review all the settings.
2. Click "Review + create", and then "Create" after validation passes.

### Step 5: Verification
Once deployed, navigate to the container instance in the portal to confirm the settings match your specified configuration.

### Step 6: Clean Up
To avoid unnecessary charges, consider deleting the container instance and resource group after completing the lab.

## Conclusion
This lab provided hands-on experience with configuring CPU and memory settings for Azure Container Instances via the Azure Portal, enabling effective resource management for your containerized applications.
