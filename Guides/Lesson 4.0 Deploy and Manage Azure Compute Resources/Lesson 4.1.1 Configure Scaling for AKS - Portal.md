# Lab 2: Configure Scaling for AKS using the Azure Portal

## Objective:
Learn how to manually scale an AKS cluster and configure autoscaling for pods using the Azure Portal.

## Prerequisites:
- An Azure subscription
- An existing AKS cluster

## Instructions:


### Step 1: Login to Azure Portal

Open your web browser and navigate to [https://portal.azure.com](https://portal.azure.com). Log in using your Azure credentials.

### Step 2: Navigate to Your AKS Cluster

1. In the search bar at the top of the portal, type "Kubernetes services".
2. Click on "Kubernetes services" from the search results.
3. Select your AKS cluster from the list.

### Step 3: Manually Scale the Node Pool

1. In the AKS cluster resource, under "Settings", click on "Node pools".
2. Click on the node pool you wish to scale.
3. Under "Scaling", adjust the node count to the desired number using the slider or input box.
4. Click "Save" to apply the changes.

### Step 4: Enable Autoscaling on a Node Pool

1. Still within the node pool settings, find the "Enable autoscale" option and click it.
2. Set the minimum and maximum node count for autoscaling.
3. Click "Save" to enable autoscaling.

### Step 5: Configure Horizontal Pod Autoscaler (HPA)

1. Navigate back to the main page of your AKS cluster.
2. Under "Settings", click on "Workloads".
3. Select the deployment you wish to autoscale.
4. Under "Scaling", select "Autoscale".
5. Set the minimum and maximum pods, and define the target CPU or memory utilization.
6. Click "Save" to configure HPA.

## Conclusion

You have now learned how to manually scale node pools and configure autoscaling for pods in AKS using the Azure portal.
