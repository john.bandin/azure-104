## Lab 1: Configuring Azure Disk Encryption using PowerShell

**Objective**: Enable Azure Disk Encryption on a VM using PowerShell.

### Prerequisites
- Azure subscription
- Azure PowerShell module installed
- Existing Azure Virtual Machine
- Azure Key Vault

### Instructions

#### Step 1: Log in to Azure

Open PowerShell and execute the following command to log in to your Azure account:

```powershell
Connect-AzAccount
```

#### Step 2: Set Your Subscription

Identify and set the subscription where your resources are located:

```powershell
$subscriptionId = "<Your-Subscription-ID>"
Select-AzSubscription -SubscriptionId $subscriptionId

```

#### Step 3: Create a Key Vault

If you do not already have an Azure Key Vault, create one using the following commands:

```powershell
$resourceGroupName = "<Your-Resource-Group-Name>"
$location = "<Azure-Region>"
$keyVaultName = "<Your-Key-Vault-Name>"

New-AzKeyVault -Name $keyVaultName -ResourceGroupName $resourceGroupName -Location $location
```

#### Step 4: Enable Key Vault for Disk Encryption

Configure the Key Vault to be used for disk encryption by enabling the necessary access policies:

```powershell
Set-AzKeyVaultAccessPolicy -VaultName $keyVaultName -ResourceGroupName $resourceGroupName -EnabledForDiskEncryption
```

#### Step 5: Encrypt Your VM Disks

Now, specify your VM and apply disk encryption:

```powershell
$vmName = "<Your-VM-Name>"
Set-AzVMDiskEncryptionExtension -ResourceGroupName $resourceGroupName -VMName $vmName -DiskEncryptionKeyVaultUrl (Get-AzKeyVault -VaultName $keyVaultName).VaultUri -DiskEncryptionKeyVaultId (Get-AzKeyVault -VaultName $keyVaultName).ResourceId
```

#### Step 6: Verify Encryption Status

To ensure that the disks are encrypted, you can check the encryption status:

```powershell
Get-AzVmDiskEncryptionStatus -ResourceGroupName $resourceGroupName -VMName $vmName
```

## Conclusion

This lab has guided you through the process of enabling Azure Disk Encryption on a virtual machine using PowerShell. By completing this exercise, you have enhanced the security of your VM by encrypting its disks with keys managed in Azure Key Vault.