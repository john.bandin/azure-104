# Lesson 3.2.2: Configure Azure Blob Storage using PowerShell

![](/Images/Lesson%203.2.2%20Create%20Azure%20Blob%20Storage.png)

## Objectives

- Create a new storage account and a blob container
- Upload a blob to the container
- Retrieve the blob from the container

## Prerequisites

- Azure PowerShell module installed
- An Azure subscription

## Instructions

### Step 1: Login to Azure

Open PowerShell and run the following command to sign into your Azure account:

```powershell
Connect-AzAccount
```

### Step 2: Create a new storage account

Replace <ResourceGroupName>, <StorageAccountName>, and <Location> with your desired values.

```powershell
$resourceGroup = "<ResourceGroupName>"
$storageAccountName = "<StorageAccountName>"
$location = "<Location>"

# Create a new storage account
New-AzStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName -Location $location -SkuName "Standard_LRS"
```

### Step 3: Create a blob container

Replace <ContainerName> with your desired container name.

```powershell
$containerName = "<ContainerName>"

# Get the storage account context
$ctx = Get-AzStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName

# Create the blob container
New-AzStorageContainer -Name $containerName -Context $ctx.Context
```

### Step 4: Upload a blob

Replace <FilePath> with the path to the file you want to upload.

```powershell
$filePath = "<FilePath>"
$blobName = "exampleblob"

# Upload the blob
Set-AzStorageBlobContent -File $filePath -Container $containerName -Blob $blobName -Context $ctx.Context
```

### Step 5: Retrieve the blob

```powershell
# Retrieve the blob
Get-AzStorageBlobContent -Blob $blobName -Container $containerName -Context $ctx.Context
```

## Conclusion

In this lab, you've learned how to create and configure Azure Blob Storage using PowerShell, including how to upload and retrieve blobs.