# Lab 1: Configure Scaling for AKS using PowerShell

## Objective:

Learn how to manually scale an AKS cluster and configure autoscaling for pods using PowerShell.

## Prerequisites:

- An Azure subscription
- PowerShell with Azure module installed
- An existing AKS cluster

## Instructions:

### Step 1: Login to Azure

Open your PowerShell console and login to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Set the Context to Your AKS Cluster

Replace <ResourceGroupName> and <ClusterName> with your specific values.

```powershell
$resourceGroup = "<ResourceGroupName>"
$clusterName = "<ClusterName>"
Set-AzContext -ResourceGroupName $resourceGroup -Name $clusterName
```

### Step 3: Manually Scale the Node Pool

Specify the desired number of nodes in <DesiredNodeCount>.

```powershell
$nodePoolName = "agentpool"  # Default node pool name; replace if different
$desiredNodeCount = 3  # Example node count
AzAksNodePool scale --resource-group $resourceGroup --cluster-name $clusterName --name $nodePoolName --node-count $desiredNodeCount
```

### Step 4: Enable Autoscaling on a Node Pool

Set the minimum and maximum node count for autoscaling.

```powershell
$minCount = 1
$maxCount = 5
AzAksNodePool update --resource-group $resourceGroup --cluster-name $clusterName --name $nodePoolName --enable-cluster-autoscaler --min-count $minCount --max-count $maxCount
```

### Step 5: Step 5: Configure Horizontal Pod Autoscaler (HPA)

First, ensure you have a deployment to configure HPA. Replace <DeploymentName> with your deployment name.

```powershell
$deploymentName = "<DeploymentName>"
kubectl autoscale deployment $deploymentName --cpu-percent=50 --min=1 --max=10
```

## Conclusion

You have now learned how to manually scale node pools and configure autoscaling for pods in AKS using PowerShell.