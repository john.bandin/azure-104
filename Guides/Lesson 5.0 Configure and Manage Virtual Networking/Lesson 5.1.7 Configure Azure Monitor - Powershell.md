# Lab 1: Configuring Azure Monitor for Networks Using PowerShell

## Objectives
- Enable network monitoring features.
- Query and analyze network performance data.

## Prerequisites

- Azure PowerShell module installed.

- Existing Azure subscription and appropriate permissions to manage resources.

### Step 1: Login to Azure

Open your PowerShell terminal and authenticate to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Register Insights Provider

Ensure the Microsoft.Insights provider is registered.

```powershell
Register-AzResourceProvider -ProviderNamespace Microsoft.Insights
```

### Step 3: Enable Network Watcher

Enable Network Watcher in your Azure region.

```powershell
$region = 'YourRegionName'  # Example: 'eastus'
Enable-AzNetworkWatcher -Location $region
```

### Step 4: Configure NSG Flow Logs


```powershell
$storageAccount = 'YourStorageAccountName'
$networkWatcher = Get-AzNetworkWatcher | Where-Object { $_.Location -eq $region }
$nsg = Get-AzNetworkSecurityGroup -Name 'YourNSGName' -ResourceGroupName 'YourResourceGroupName'

Set-AzNetworkWatcherConfigFlowLog -NetworkWatcher $networkWatcher -TargetResourceId $nsg.Id -StorageAccountId $storageAccount -Enabled $true
```

