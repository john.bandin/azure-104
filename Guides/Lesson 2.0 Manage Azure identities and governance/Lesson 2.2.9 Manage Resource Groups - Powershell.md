# Lesson 2.2.9: Managing Resource Groups Using PowerShell

## Objective
Learn how to create, manage, and delete resource groups in Azure using PowerShell.

## Prerequisites
- Azure PowerShell module installed and configured on your machine.

## Step 1: Open PowerShell and Connect to Azure
Ensure you are connected to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a Resource Group

Create a new resource group in a specific Azure region.

```powershell
$resourceGroupName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $resourceGroupName -Location $location
```

## Step 3: Manage the Resource Group

Modify the resource group by adding a tag.

```powershell
Set-AzResourceGroup -Name $resourceGroupName -Tag @{ Project="AZ-104 Training" }
```

## Step 4: Delete the Resource Group

Remove the resource group when it is no longer needed.

```powershell
Remove-AzResourceGroup -Name $resourceGroupName -Force
```

## Step 5: Verify Deletion

Ensure that the resource group has been successfully deleted.

```powershell
Get-AzResourceGroup -Name $resourceGroupName -ErrorAction SilentlyContinue
```

## Conclusion

You have learned how to create, manage, and delete resource groups using PowerShell, which is essential for managing Azure resources efficiently.
