# Set Up Virtual Network Peering in Azure

## Step 1

1\. First we will have to create a second VNET for this lab. The complete steps are not in this Guide, follow the Guide in the previous lesson, Lesson "Lesson 5.0.2 Creating Virtual Networks and Subnets - Portal", to see how to create a VNET

**NOTE** The second VNET we create will have to be a differnt IP address Subnet then our first VNET. VNET Peering cannot be between overlapping IP addresses. **NOTE**

## Step2

1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click "Virtual networks"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/4364cbd8-66bb-43cb-8583-a36849ac9721/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=270,98)


3\. Click on the first Vnet you made in the first lab. In this example the name is: "my_name_Vnet"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/dad45a37-9ddc-4c9f-94ea-f3472fa21c48/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=52,395)


4\. Under "settings" on this Vnet click "peerings", and then click "add".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/0a9df402-4c44-4cfb-b0d8-734f27beac68/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=301,504)


5\. Click the "Remote peering link name" field. And set a name for the remote peer. "my-name-remote-peer"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/746c8162-4bff-44fb-84f8-81a341ede802/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=384,105)


6\. Leave "resource manager" clicked. And now we will set which virtual machine we want to make the peer too. Click "Select virtual network"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/6c64fd93-5837-4565-acf8-139cb6a13a8d/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=207,212)


7\. At this step we will select the second Vnet we made in step 1. Remember that VNET peerings cannot overlap IP addressing. So if the second VNET you made has the same IP addressing as the first VNET you made this peer will not work.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/87e12f6b-9a6b-4350-9d97-8956354466eb/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=575,285)


8\. Click the "Local peering link name" field. And set the local peering name. "my-name-local-peering"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/8ecccf34-56c1-4a11-a5cb-19f73cc914bb/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=296,262)


9\. Now that we have configured the peer we will click "Add"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/a795a998-0727-41ac-861b-8939f25549ea/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0)


10\. Now that we have created the peer lets go back to our virtual network and go inspect the second virtual network we made to ensure the peer is up.Click "Virtual networks"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d2d55a74-1868-4328-8895-1a0a7950a560/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=50,10)


11\. Click on the second VNET we created in step1, this is the VNET we made the peering too. In my example that is: "my-name-vnet2"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/24c192ea-f459-4588-8ddb-fd6b1954b814/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=73,286)


12\. Click "Peerings"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/168e1a15-73af-465b-bb13-5b28355fc6b1/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=302,509)


13\. If the peering is fully synchronized we have completed this lab!

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/edc8cbf5-7d74-4abc-969f-e9de9c602fea/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=90,163)


