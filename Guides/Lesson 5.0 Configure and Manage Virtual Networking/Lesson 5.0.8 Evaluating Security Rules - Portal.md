# Lab: Evaluate Effective Security Rules Using the Azure Portal

## Objective
Evaluate and analyze the effectiveness of security rules applied to NSGs using the Azure Portal.

## Prerequisites
- Azure subscription
- Access to the Azure Portal

## Instructions

### Step 1: Log in to the Azure Portal

Log in to the Azure Portal at https://portal.azure.com.

### Step 2: Navigate to Network Security Groups

1. Search for "Network Security Groups" in the portal's search bar.
2. Select the NSG you want to evaluate.

### Step 3: Review Inbound and Outbound Security Rules

1. Click on "Inbound security rules" to view and analyze the rules.
2. Check each rule's source, destination, port, and action settings.
3. Repeat for "Outbound security rules".

### Step 4: Identify and Resolve Conflicts or Redundancies

1. Look for rules that are overly permissive or conflicting with other rules.
2. To edit a rule, click on it and then click "Edit".
3. Adjust the settings as necessary and click "Save".

### Step 5: Document the Changes and Rationale

- Ensure to document why changes were made to maintain an audit trail and facilitate future reviews.

## Conclusion

This lab demonstrated how to evaluate the effectiveness of NSG rules using the Azure Portal. You learned to analyze, adjust, and document changes to ensure optimal security settings.
