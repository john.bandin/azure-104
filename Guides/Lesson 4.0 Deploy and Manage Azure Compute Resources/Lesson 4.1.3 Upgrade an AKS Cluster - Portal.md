# Lab 2: Upgrading an AKS Cluster using the Azure Portal

This lab will guide you through the process of upgrading an AKS cluster using the Azure Portal.

## Prerequisites
- You should have access to the Azure Portal.
- You should have an existing AKS cluster.

## Steps

### Step 1: Open the Azure Portal

Open your web browser and navigate to https://portal.azure.com. Log in with your credentials.

### Step 2: Navigate to your AKS cluster

1. Click on "All services" in the left-hand menu.
2. Search for "Kubernetes services" and select it.
3. Find your cluster in the list and click on it to open the cluster's dashboard.

### Step 3: Check the current Kubernetes version

1. In the cluster dashboard, look for the "Overview" section to see the current version of Kubernetes.

### Step 4: Upgrade the cluster

1. In the cluster dashboard, click on "Settings" and then "Upgrades."
2. You will see a list of available Kubernetes versions. Select the version you wish to upgrade to and click "Upgrade."

### Step 5: Monitor the upgrade

1. You can monitor the progress of the upgrade in the "Notifications" area at the top-right of the portal.

## Conclusion

After the upgrade is complete, verify the Kubernetes version in the "Overview" section to ensure your cluster is now running the new version.
