# Creating a Virtual Network in Azure


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click the "Search resources, services, and docs (G+/)" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/407f4fe6-b347-42af-afcd-bec43863ca37/ascreenshot.jpeg?tl_px=46,0&br_px=1422,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,-20)


3\. Type "virtual network" and Click on "virtual Networks"


4\. Now lets create a new VNET. Click "create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/048b99cd-511a-4dc2-94f7-917302462565/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=7,83)


5\. Just like any other resource in Azure we will set this Virtual Network in the correct subscription and create a new "resource group".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/1f3b07f0-8f3a-4be7-86fe-2d3f350f8eae/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=226,364)


6\. Set the name for the new Resource Group and select "ok"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/1729cfbc-f3dc-4dbe-8c23-4c70cba617c5/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=245,524)


7\. Now we will give our Vnet a name.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d64a102c-9f88-44ae-b973-0265f5b274a2/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=301,456)


8\. Set the name and click and "next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/fe366d8d-304c-46de-8f51-7ad7baeb2fc6/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=130,612)


9\. At the security section we will leave everything at default. We will come back to some of these options in later sections. Click "next" to move onto the "IP addresses" Section.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/e51645cb-369c-427a-957d-2a42075667d2/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=118,621)


10\. From here we will create a new "subnet" for our virtual machines.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/28d5ad39-7605-4af9-af10-bc2d3ce808bc/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=89,384)


11\. Click "Default" on the subnet's purpose.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/f96ac9aa-98b6-4449-bdda-c311ebd882c5/ascreenshot.jpeg?tl_px=496,0&br_px=1443,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,172)


12\. Click "Default"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/88062fed-2ac0-47c0-91b1-c7d44ee7205e/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=664,162)


13\. In the name field we will set the name as "Virtualmachines" And we will leave our starting address at the default of "10.1.0.0/16". In your VNet instance the IP addressing by default may be different. Make sure that subnetting is correct.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/b379c996-e6c4-4a7d-820f-419bc21dc551/ascreenshot.jpeg?tl_px=164,0&br_px=1541,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=602,160)


14\. The rest of the options we will leave at default. We will come back in later sections to some of these options.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d28dc8bf-52d6-430c-a0ab-8f225e754992/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=667,373)


15\. Click "Add"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d8cbefa8-4f91-438c-a8a7-67d824a6f91c/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=463,615)


16\. Click "Next" to move onto the "tags" section.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/7ab16382-9c5d-4142-9339-2f76e803d4d0/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=112,623)


17\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/5f3952e7-82c2-42ea-b7a5-a569758d09e8/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=111,623)


18\. And now we will create our Vnet. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/c1fd0ee8-651c-4ff8-87a6-e994475b9f5b/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=183,613)


19\. Click "Go to resource" to view our new Vnet.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/dbe2f89e-c618-4842-bd5d-0c8abe45e554/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=852,91)


20\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/6f041fb7-9fbf-4b3d-9d27-2e2f9a66c7f1/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=422,-22)


