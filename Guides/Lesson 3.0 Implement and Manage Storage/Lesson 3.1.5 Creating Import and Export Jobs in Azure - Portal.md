# Lab 2: Creating an Azure Export Job using the Azure Portal

## Objective
Learn how to create an Azure Export job using the Azure Portal to export data from Azure Blob Storage.

## Prerequisites
- Azure subscription
- Azure Storage account
- Data already stored in Azure Blob Storage

## Instructions

### Step 1: Open the Azure Portal
Go to [Azure Portal](https://portal.azure.com) and log in with your credentials.

### Step 2: Navigate to the Storage Account
Find the storage account from which you want to export data.

### Step 3: Initiate an Export Job
1. In the storage account's overview pane, click on 'Data Management' and then 'Import/Export jobs'.
2. Click on '+ Add' and then select 'Create export job'.
3. Fill in the job details:
   - Job name
   - Blob containers to export
   - Delivery and return addresses
4. Review the job details and confirm the job creation.

### Step 4: Ship empty drives to Azure
After creating the job, follow the instructions to ship your empty hard drives to the specified Azure data center.

### Step 5: Monitor the job status
Keep track of the export job status directly from the Import/Export job panel in the Azure Portal.

## Conclusion
Once the data is transferred to your drives, Azure will ship them back to your specified return address. Monitor the job status through the Azure Portal.
