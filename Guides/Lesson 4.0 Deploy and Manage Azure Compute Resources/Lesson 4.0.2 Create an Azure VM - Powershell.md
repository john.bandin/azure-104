# Lesson 4.0.2: Creating an Azure VM using PowerShell

- This lab will guide you through the steps to create a Virtual Machine in Microsoft Azure using PowerShell.

## Prerequisites

- Azure subscription
- PowerShell installed on your computer
- Azure PowerShell module installed

### Steps

1. Login to Azure Account

Open PowerShell and login to your Azure account with the following command:

```powershell
Connect-AzAccount
```

2. Create a Resource Group

Choose a name and location for your resource group:

```powershell
New-AzResourceGroup -Name "MyResourceGroup" -Location "EastUS"
```

3. Create Virtual Network

```powershell
$subnetConfig = New-AzVirtualNetworkSubnetConfig -Name "MySubnet" -AddressPrefix "10.0.0.0/24"
$vnet = New-AzVirtualNetwork -Name "MyVNET" -ResourceGroupName "MyResourceGroup" -Location "EastUS" -AddressPrefix "10.0.0.0/16" -Subnet $subnetConfig
```

4. Create a Public IP Address

```powershell
$publicIp = New-AzPublicIpAddress -Name "MyPublicIP" -ResourceGroupName "MyResourceGroup" -Location "EastUS" -AllocationMethod Static
```

5. Create a Network Security Group and Rule

```powershell
$nsgRuleRDP = New-AzNetworkSecurityRuleConfig -Name "MyNetworkSecurityGroupRuleRDP" -Protocol Tcp -Direction Inbound -Priority 1000 -SourceAddressPrefix * -SourcePortRange * -DestinationAddressPrefix * -DestinationPortRange 3389 -Access Allow
$nsg = New-AzNetworkSecurityGroup -Name "MyNetworkSecurityGroup" -ResourceGroupName "MyResourceGroup" -Location "EastUS" -SecurityRules $nsgRuleRDP
```

6. Create a Virtual Network Interface

```powershell
$nic = New-AzNetworkInterface -Name "MyNIC" -ResourceGroupName "MyResourceGroup" -Location "EastUS" -SubnetId $vnet.Subnets[0].Id -PublicIpAddressId $publicIp.Id -NetworkSecurityGroupId $nsg.Id
```

7. Create a Virtual Machine

Choose the virtual machine size, username, and password:

```powershell
$vmConfig = New-AzVMConfig -VMName "MyVM" -VMSize "Standard_D2s_v3"
$vmConfig = Set-AzVMOperatingSystem -VM $vmConfig -Windows -ComputerName "MyVM" -Credential (Get-Credential)
$vmConfig = Set-AzVMSourceImage -VM $vmConfig -PublisherName "MicrosoftWindowsServer" -Offer "WindowsServer" -Skus "2019-Datacenter" -Version "latest"
$vmConfig = Add-AzVMNetworkInterface -VM $vmConfig -Id $nic.Id
New-AzVM -ResourceGroupName "MyResourceGroup" -Location "EastUS" -VM $vmConfig
```

8. Confirm VM Creation

Check the state of your newly created VM:

```powershell
Get-AzVM -Name "MyVM" -ResourceGroupName "MyResourceGroup"
```

### Conclusion

This completes your Azure VM setup using PowerShell. You now have a functional Windows Server 2019 virtual machine in your Azure subscription.