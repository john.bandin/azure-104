# Lab 1: Configuring Storage in AKS using PowerShell

### Step 1: Login to Azure

Open PowerShell and run:
```powershell
Connect-AzAccount
```

### Step 2: Set Variables

Replace <yourResourceGroup> and <yourAKSCluster> with your actual resource group and AKS cluster names.

```powershell
$resourceGroup = "<yourResourceGroup>"
$aksCluster = "<yourAKSCluster>"
```

### Step 3: Create a new Azure Disk

```powershell
$diskConfig = New-AzDiskConfig -Location "East US" -CreateOption Empty -DiskSizeGB 128
$disk = New-AzDisk -Disk $diskConfig -ResourceGroupName $resourceGroup -DiskName "myAKSDisk"
```

### Step 4: Get the AKS Cluster and Attach the Disk

```powershell
$aks = Get-AzAks -ResourceGroupName $resourceGroup -Name $aksCluster
Attach-AzAksCluster -ResourceGroupName $resourceGroup -Name $aksCluster -DiskId $disk.Id
```

### Step 5: Create and Configure a Persistent Volume Claim (PVC) in AKS

Create a file named pvc.yaml with the following content:

```yaml
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: mypvc
spec:
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 120Gi
  storageClassName: default

```

Apply the PVC in AKS:

```powershell
kubectl apply -f pvc.yaml
```

## Conclusion

The Azure Disk is now configured and attached as a persistent volume in your AKS cluster. You can now use this PVC in your Kubernetes deployments.