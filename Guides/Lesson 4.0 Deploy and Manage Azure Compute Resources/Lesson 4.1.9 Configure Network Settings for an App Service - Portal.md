# Lab 2: Configure Networking Settings for Azure App Service Using the Azure Portal

## Objective:
Learn how to configure networking settings for Azure App Service using the Azure Portal.

## Prerequisites:
- Azure subscription and permissions to configure App Services

## Instructions:


### Step 1: Log in to Azure Portal

Navigate to [https://portal.azure.com](https://portal.azure.com) and sign in with your Azure account credentials.

### Step 2: Configure VNet Integration

1. In the Azure Portal, search for and select **App Services**.
2. Click on your app service, then under **Settings**, select **Networking**.
3. Under **VNet Integration**, click on **Click here to configure**.
4. Select **Add VNet**, choose your existing VNet and the appropriate subnet, then click **OK**.

### Step 3: Configure Access Restrictions

1. Still in the **Networking** menu, click on **Access Restrictions**.
2. Click **+ Add rule**.
3. Configure your rules:
   - Allow traffic from a specific IP by setting the priority, name, action to **Allow**, and specifying the IP range.
   - Add a high-priority rule to deny all other traffic by setting the action to **Deny** and the IP range to `0.0.0.0/0`.
4. Click **Add** after configuring each rule.

## Conclusion

You have now configured VNet integration and access restrictions for your Azure App Service using the Azure portal. Review all settings to ensure they are applied correctly.
