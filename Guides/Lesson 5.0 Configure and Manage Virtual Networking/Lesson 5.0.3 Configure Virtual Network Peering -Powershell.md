# Lab 1: Configuring Virtual Network Peering using PowerShell

## Objective:

Create and configure virtual network peering between two Azure Virtual Networks using PowerShell.

## Prerequisites:

- Azure subscription
- Azure PowerShell module installed
- Two virtual networks (VNet1 and VNet2) to be peered

### Step 1: Login to Azure Account
Open PowerShell and run the following command to log in to your Azure account:

```powershell
Connect-AzAccount
```

### Step 2: Declare Variables

Set the variables for the subscription, resource groups, and virtual network names:

```powershell
$subscriptionId = 'your-subscription-id'
$resourceGroup1 = 'your-resource-group-for-vnet1'
$resourceGroup2 = 'your-resource-group-for-vnet2'
$vnetName1 = 'VNet1'
$vnetName2 = 'VNet2'
```

### Step 3: Select Subscription


```powershell
Set-AzContext -SubscriptionId $subscriptionId
```

### Step 4: Get Virtual Networks

Retrieve the virtual network resources:

```powershell
$vnet1 = Get-AzVirtualNetwork -Name $vnetName1 -ResourceGroupName $resourceGroup1
$vnet2 = Get-AzVirtualNetwork -Name $vnetName2 -ResourceGroupName $resourceGroup2
```

### Step 5: Initiate Peering from VNet1 to VNet2

Create the peering from VNet1 to VNet2:

```powershell 
Add-AzVirtualNetworkPeering -Name 'LinkFromVNet1ToVNet2' -VirtualNetwork $vnet1 -RemoteVirtualNetworkId $vnet2.Id -AllowVirtualNetworkAccess $true -AllowForwardedTraffic $true
```

### Step 6: Initiate Peering from VNet2 to VNet1

Create the peering from VNet2 to VNet1:

```powershell
Add-AzVirtualNetworkPeering -Name 'LinkFromVNet2ToVNet1' -VirtualNetwork $vnet2 -RemoteVirtualNetworkId $vnet1.Id -AllowVirtualNetworkAccess $true -AllowForwardedTraffic $true
```

### Step 7: Verify Peering Status

Check the peering status:

```powershell
Get-AzVirtualNetworkPeering -ResourceGroupName $resourceGroup1 -VirtualNetworkName $vnetName1
Get-AzVirtualNetworkPeering -ResourceGroupName $resourceGroup2 -VirtualNetworkName $vnetName2
```

