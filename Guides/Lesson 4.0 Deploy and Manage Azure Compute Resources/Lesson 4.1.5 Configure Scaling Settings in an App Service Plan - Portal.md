
# Lab 2: Configure Scaling Settings in an App Service Plan Using Azure Portal

## Objective
Learn how to configure scaling settings for an Azure App Service Plan using the Azure Portal.

## Prerequisites
- Azure subscription
- Access to Azure Portal

## Steps

### Step 1: Login to Azure Portal
Log in to the Azure Portal (https://portal.azure.com).

### Step 2: Navigate to App Service Plan
In the search bar at the top, type "App Services" and select your App Service from the list. Then, click on the "Scale Out (App Service plan)" option under Settings.

### Step 3: Configure Manual Scaling
Select "Scale out" and then choose "Manual Scale". Set the desired instance count and save your settings.

### Step 4: Configure Auto Scaling
Select "Scale out", choose "Custom autoscale", and add a new rule based on the metrics such as CPU percentage or memory usage.
- Set the target range for the metric.
- Define the scale in and scale out rules.
- Save your settings.

### Step 5: Review and Save Configuration
Ensure all settings are correct and then click "Save" to apply the scaling settings.

## Conclusion
In this lab, you've learned how to configure manual and automatic scaling for an Azure App Service Plan using the Azure Portal interface.
