# How to Move Resources in Azure Portal

1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click "Resource groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/6fcf18db-5e12-45d6-a5b4-b2c735da3fcd/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=208,89)


3\. Search for the resource group that contains the VM you want to move.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/67755294-436c-4590-a888-a2583e8e4255/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=114,182)


4\. Click on the appropriate resource group. The VM I want to move is in "vm-resource-group".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/2d224717-ad15-415b-a5ce-c6a8eff974f6/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=64,253)


5\. At the resource group overview page we will click on the VM we want to move.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/7968f09a-1df6-446b-b1c2-789ba646e6e2/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=462,404)


6\. Once the VM is selected we will click on the "Move--&gt;" button on the top navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/0a2c87a8-922c-46a5-8782-c398e08d608c/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=884,83)


7\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/e42e81c9-5d3d-4385-b6f7-8bf1bf8f56e6/ascreenshot.jpeg?tl_px=164,0&br_px=1541,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=888,126)


8\. We will create a new resource group to move our VM too. Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d3d007f6-95e1-44c4-a3e3-7ad8f47b1e98/ascreenshot.jpeg?tl_px=0,178&br_px=946,707&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=334,233)


9\. Input your new Resource Group name. Click "OK"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/a3596080-3c17-4beb-babc-d6defdcc0476/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=230,448)


10\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/914c7504-c1c8-4fd7-93a2-723a1db00699/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=140,434)


11\. Click "Next". We will wait for the validation to process. And once it is done we will click next.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/568f7a9a-0547-4865-b4ef-f50a643a0a40/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=120,510)


12\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/ad865961-4dcd-43ac-9655-c6688605ee42/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-10,482)


13\. Click "Move"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/2dc2dd92-5308-4a99-855e-7629506151c3/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=113,509)
#### [Made with Scribe](https://scribehow.com/shared/How_to_Move_Resources_in_Azure_Portal__ttRgXo_zQ4CRNKUmtB0BHg)

14\. This will now take 5-15 minutes to move. 

### Step 4: Verify the Move

Navigate to the target resource group and ensure the VM is listed under its resources.

## Conclusion

You have successfully moved a VM from one resource group to another using the Azure Portal. This method provides a visual approach that may be preferable for users comfortable with graphical interfaces.
