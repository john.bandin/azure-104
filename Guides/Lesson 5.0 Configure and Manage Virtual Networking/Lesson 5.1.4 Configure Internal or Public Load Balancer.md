# Lab: Configure a Public Load Balancer Using Azure PowerShell

## Objective
Create and configure a public load balancer to distribute traffic among multiple VMs.

## Prerequisites
- Azure PowerShell module installed
- You must have Contributor or higher privileges in your Azure subscription

## Instructions

### Step 1: Login to Azure

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group

Replace <ResourceGroupName> and <Location> with your desired resource group name and Azure region, respectively.

```powershell
New-AzResourceGroup -Name <ResourceGroupName> -Location <Location>
```

### Step 3: Create a Public IP Address

This IP address will be used by the load balancer.

```powershell
$publicIp = New-AzPublicIpAddress -Name "MyPublicIP" -ResourceGroupName <ResourceGroupName> -AllocationMethod Static -Location <Location>
```

### Step 4: Create a Frontend IP Configuration

```powershell
$frontendIp = New-AzLoadBalancerFrontendIpConfig -Name "LB-Frontend" -PublicIpAddress $publicIp
```

### Step 5: Define the Backend Pool

```powershell
$backendPool = New-AzLoadBalancerBackendAddressPoolConfig -Name "LB-BackendPool"
```

### Step 6: Define a Load Balancing Rule

Replace <Port> with the port number you want to balance the load on.

```powershell
$loadBalancingRule = New-AzLoadBalancerRuleConfig -Name "HTTPRule" -FrontendIpConfiguration $frontendIp -BackendAddressPool $backendPool -Protocol Tcp -FrontendPort <Port> -BackendPort <Port> -EnableFloatingIp
```

### Step 7: Create the load balancer

```powershell
$lb = New-AzLoadBalancer -ResourceGroupName <ResourceGroupName> -Name "MyLoadBalancer" -Location <Location> -FrontendIpConfigurations $frontendIp -BackendAddressPools $backendPool -LoadBalancingRules $loadBalancingRule
```

### Step 8: Verify the load balancer

```powershell
Get-AzLoadBalancer -Name "MyLoadBalancer" -ResourceGroupName <ResourceGroupName>
```

## Conclusion

You have successfully created a public load balancer in Azure using PowerShell. This load balancer is now ready to distribute traffic among your VMs based on your configurations.

