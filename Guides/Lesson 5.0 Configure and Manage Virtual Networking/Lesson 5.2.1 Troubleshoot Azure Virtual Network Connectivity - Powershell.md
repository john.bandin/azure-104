# Lab 1: Troubleshooting Azure Virtual Network Connectivity Using PowerShell

## Objective
Learn how to troubleshoot virtual network connectivity issues using PowerShell.

## Prerequisites
- Azure PowerShell module installed.
- You must have Contributor or higher access to the Azure subscription.

## Steps

### Step 1: Install the Azure PowerShell Module

If you haven't already installed the Azure PowerShell module, run the following command:

```powershell
Install-Module -Name Az -AllowClobber -Scope CurrentUser
```

### Step 2: Login to Your Azure Account

```powershell
Connect-AzAccount
```

### Step 3: List All Virtual Networks

List all the virtual networks in your subscription to identify the one you need to troubleshoot.

```powershell
Get-AzVirtualNetwork
```

### Step 4: Check Network Security Group (NSG) Rules

Review the NSG rules applied to the subnet or network interface of the virtual machine.

```powershell
Get-AzNetworkSecurityGroup -Name <NSGName> -ResourceGroupName <ResourceGroupName>
```

### Step 5: Validate Effective Security Rules

Use the following command to list effective security rules for a network interface.

```powershell
Get-AzEffectiveNetworkSecurityGroup -NetworkInterfaceName <InterfaceName> -ResourceGroupName <ResourceGroupName>
```

### Step 6: Test Network Connectivity with Azure Network Watcher

Ensure the Network Watcher is enabled in your region, then run a connectivity test.

```powershell
Test-AzNetworkWatcherConnectivity -NetworkWatcherName <NetworkWatcherName> -ResourceGroupName <ResourceGroupName> -SourceId <SourceResourceId> -DestinationAddress <DestinationIP> -DestinationPort <DestinationPort>
```

## Conclusion

After completing these steps, you should be able to diagnose and resolve basic virtual network connectivity issues in Azure using PowerShell.