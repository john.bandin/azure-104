
# Lesson 3.2.2: Configure Azure Blob Storage using Azure Portal

![](/Images/Lesson%203.2.2%20Create%20Azure%20Blob%20Storage.png)

## Objectives

- Create a new storage account and a blob container
- Upload a blob to the container
- Retrieve the blob from the container

## Prerequisites

- Access to the Azure Portal

## Instructions

### Step 1: Login to Azure Portal

Log in to the Azure Portal at https://portal.azure.com.

### Step 2: Create a new storage account

1. In the Azure Portal, click on "Create a resource".
2. Search for "Storage Account" and select it.
3. Click "Create".
4. Fill in the necessary fields like Subscription, Resource Group, Storage Account Name, and Location.
5. Click "Review + Create" and then "Create".

### Step 3: Create a blob container

1. Once the storage account is created, go to it by clicking on "Go to resource".
2. Under "Blob service", select "Containers".
3. Click "+ Container".
4. Enter a name for your container and set the public access level.
5. Click "Create".

### Step 4: Upload a blob

1. Click on your container to open it.
2. Click "Upload".
3. Browse and select the file you want to upload.
4. Click "Upload".

### Step 5: Retrieve the blob

1. Click on the blob to open it.
2. Click "Download" to download the blob to your local system.

## Conclusion

In this lab, you've learned how to create and configure Azure Blob Storage using the Azure Portal, including how to upload and retrieve blobs.
