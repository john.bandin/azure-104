# Lab 2: Troubleshooting External Networking Using Azure Portal

**Objective**: Use the Azure Portal to diagnose and troubleshoot external networking issues.

## Prerequisites

- Azure subscription
- Appropriate permissions to view and modify network configurations

## Instructions

### Step 1: Open Network Watcher

Navigate to **All services** in the Azure portal. Search for and select **Network Watcher**.

### Step 2: Verify IP Flow

Verify IP flow to ensure there are no blocks in place that could affect connectivity.

1. In Network Watcher, go to **IP flow verify** under `Diagnostic tools`.
2. Select the subscription, resource group, and the virtual machine you want to test.
3. Configure the test parameters, such as protocol, local port, remote port, and remote IP address.
4. Click on **Check** to start the verification process.

### Step 3: View Effective Security Rules

Check the effective security rules applied to a network interface.

1. In Network Watcher, go to **Effective security rules** under `Diagnostic tools`.
2. Select the virtual network, then the subnet, and finally the specific VM.
3. Review the list of applied and effective network security rules.

### Step 4: Review Network Performance Monitor

If set up, use Network Performance Monitor to check for network performance issues.

1. Go to **Network Performance Monitor** under `Monitoring`.
2. Review the reports and metrics to identify any anomalies or drops in network traffic.
