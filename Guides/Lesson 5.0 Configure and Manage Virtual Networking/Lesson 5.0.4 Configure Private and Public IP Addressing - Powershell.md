# Lab 1: Configuring Private and Public IP Addresses using PowerShell

## Objective:

- Create and configure both private and public IP addresses for a virtual machine in Azure using PowerShell.

## Prerequisites:

- Azure subscription

- Azure PowerShell module installed

### Step 1: Login to Azure Account

Open PowerShell and run the following command to log in to your Azure account:

```powershell
   Connect-AzAccount
```

### Step 2: Declare Variables

Define variables for common values:

```powershell
$resourceGroup = 'your-resource-group'
$location = 'your-region'  # Example: 'East US'
$vnetName = 'yourVNet'
$subnetName = 'yourSubnet'
$publicIpName = 'yourPublicIP'
$nicName = 'yourNIC'
```

### Step 3: Create a Public IP Address

Create a public IP address:

```powershell
$publicIp = New-AzPublicIpAddress -Name $publicIpName -ResourceGroupName $resourceGroup -Location $location -AllocationMethod Dynamic
```

### Step 4: Get the Virtual Network

Retrieve the virtual network:

```powershell
$vnet = Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup
```

### Step 5: Get the Subnet

Retrieve the subnet:

```powershell
$subnet = Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet
```

### Step 6: Create a Network Interface

Create a network interface with both private and the newly created public IP:

```powershell
$nic = New-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroup -Location $location -SubnetId $subnet.Id -PublicIpAddressId $publicIp.Id
```

### Step 7: Verify the Configuration

Display the configured settings:

```powershell
Get-AzNetworkInterface -Name $nicName -ResourceGroupName $resourceGroup
```

