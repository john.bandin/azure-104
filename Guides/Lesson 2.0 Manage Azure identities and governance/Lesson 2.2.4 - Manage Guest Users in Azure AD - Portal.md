## Invite External User to Microsoft Azure Portal

## Step 1: Log in to the Azure Portal 

- Open your web browser and navigate to \[<https://portal.azure.com\](https://portal.azure.com)>. 
- Log in with your administrator credentials.


## Step 2: Navigate to Azure Active Directory.

-  From the home page of the Azure Portal, click on "Azure Active Directory" from the left-hand side navigation menu.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/01ae003f-8dac-4317-9e7d-177c5ce4b406/ascreenshot.jpeg?tl_px=128,0&br_px=1847,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,165)


## Step 3: Invite a Guest User 

- In the Azure Active Directory pane, click on "Users."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/8af1db37-9483-4d3e-aac8-f7b7a542362c/ascreenshot.jpeg?tl_px=0,157&br_px=1719,1118&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=164,277)


At the top of the "Users" blade, click on "New user."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2c39c7df-0ec4-4bf6-9912-cb8f90308188/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=468,158)


Click "Invite external user"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/cf06fefd-9d5a-4aa1-8886-63f6aa0c943f/ascreenshot.jpeg?tl_px=0,57&br_px=1719,1018&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=465,277)


Under the "Invite user" pane, fill in the details: 

- **Name**: Enter the name of the guest user. 
- **Email address**: Enter the email address of the guest user; this will be used to send the invitation. 
- **Personal message** (optional): Add a message to the invitation.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/4b081dc1-a756-4596-8de4-e9853e807044/ascreenshot.jpeg?tl_px=0,117&br_px=1719,1078&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=350,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/c74509d1-c31b-4b06-9f13-a3e6288fefbd/ascreenshot.jpeg?tl_px=0,139&br_px=1719,1100&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=384,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/e288f3f2-acf0-4677-9d70-2b965db7b547/ascreenshot.jpeg?tl_px=0,243&br_px=1719,1204&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=380,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/ab222609-9402-4925-bbb7-08969ae6b3e4/ascreenshot.jpeg?tl_px=0,521&br_px=1719,1482&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=358,277)


- Click "Next: Properties"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5d4ffbec-dae0-45d6-968a-4a894dd37b37/ascreenshot.jpeg?tl_px=0,940&br_px=1719,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=478,552)


## Step 4: Assign Roles or Permissions 

- Once the guest user has accepted the invitation, you can assign roles or specific permissions to control what resources the guest can access. 
- Navigate back to the "Users" blade, find the guest user, and click on their name to open their profile. 
- Click on "Assigned roles" or "Groups" to assign them to specific roles or add them to groups that have the necessary permissions.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/dddae849-4a68-4fa0-91ae-5c29ef7f75e3/ascreenshot.jpeg?tl_px=0,49&br_px=1719,1010&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=415,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/93c91463-9e55-4ea7-a166-daca870d72f0/ascreenshot.jpeg?tl_px=0,437&br_px=1719,1398&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=352,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/245516a9-92c6-4f8f-9c06-fd28c09336ac/ascreenshot.jpeg?tl_px=72,940&br_px=1791,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,537)


- Click "Add group"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/598ed71e-d05d-4a94-8fcc-ba1153f0fd11/ascreenshot.jpeg?tl_px=0,29&br_px=1719,990&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=77,277)




![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/55d30ea7-076b-4a10-8432-48bf2c46d1f6/ascreenshot.jpeg?tl_px=434,389&br_px=2154,1350&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,277)


- Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/fd490928-b1c6-42c7-93cd-4a763afd5bdb/ascreenshot.jpeg?tl_px=502,940&br_px=2222,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,552)


- Click "Add role"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/afeeea8a-f374-445c-b267-2ebbe7e64929/ascreenshot.jpeg?tl_px=0,45&br_px=1719,1006&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=204,277)


- Choose a role that fits the Guest user

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/1c12c4be-b9b1-4b09-8b9f-af5b5445b4e0/ascreenshot.jpeg?tl_px=944,187&br_px=2664,1148&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,277)


![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/23a7b6d7-eaf3-4072-b756-26b330b60f43/ascreenshot.jpeg?tl_px=1038,940&br_px=2758,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,556)


- Click "Review + invite"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/ce7da667-4e14-47fe-85d5-eab660aa7318/ascreenshot.jpeg?tl_px=156,940&br_px=1876,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,552)


- Click "Invite"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/397560c4-2d10-4a26-be5c-f3565c98d483/ascreenshot.jpeg?tl_px=0,940&br_px=1719,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=86,554)

## Step 5: Monitor Guest Activity
1. To monitor the activities of your guest users, you can utilize Azure AD's audit logs.
2. Go back to the main Azure AD pane and select "Audit logs" under the "Monitoring" section.
3. Use filters to view activities related to guest users.

## Conclusion
You have successfully invited a guest user and managed their access within Azure Active Directory. This process helps ensure that external collaborators can work effectively with your team while maintaining the security and integrity of your resources.

## Additional Tips
- Regularly review and update the access permissions of guest users to ensure they only have access to necessary resources.
- Set up alerts or automated reports to monitor the activities of guest users for unusual behavior.

