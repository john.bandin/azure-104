# Lesson 2.2.6: Configuring Azure Policy Using PowerShell

# Lab 1: Configuring Azure Policy Using PowerShell

## Objective
Learn how to create and assign a policy definition in Azure using PowerShell.

## Prerequisites
- Azure PowerShell module installed and configured.
- Appropriate permissions to manage policies in the Azure subscription.

## Step 1: Open PowerShell
Open your PowerShell console and ensure you are connected to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a Policy Definition

- Create a policy definition that ensures all resources have a specific tag.

```powershell
$policyRule = @{
    "if" = @{
        "field" = "tags.Department"
        "exists" = "false"
    }
    "then" = @{
        "effect" = "deny"
    }
}
$policyRule = $policyRule | ConvertTo-Json
$definition = New-AzPolicyDefinition -Name "RequireDepartmentTag" -DisplayName "Require Department Tag" -description "This policy ensures all resources are tagged with a Department." -Policy $policyRule -Mode Indexed
```

## Step 3: Assign the Policy

- Assign the created policy definition to a scope (e.g., subscription).

```powershell
$scope = Get-AzSubscription -SubscriptionName "Your Subscription Name"
New-AzPolicyAssignment -Name "AssignRequireDepartmentTag" -Scope $scope.Id -PolicyDefinition $definition
```

## Step 4: Verify the Policy Assignment

- Verify that the policy has been assigned successfully.

```powershell
Get-AzPolicyAssignment -Name "AssignRequireDepartmentTag"
```

## Conclusion

- You have successfully created and assigned a policy in Azure using PowerShell. This policy will ensure that all new resources within the assigned scope have a 'Department' tag.

