# Lab Exercise: Creating Custom RBAC and Azure AD Roles Using the Azure Portal

## Objective
This lab demonstrates how to create custom Role-Based Access Control (RBAC) roles and Azure Active Directory (AD) roles using the Azure portal. Participants will learn to tailor access control to meet specific organizational needs.

## Prerequisites
- Access to the Azure portal with administrative privileges.

## Part 1: Creating a Custom RBAC Role

# Add Custom Role with Virtual Network Contributor Permission


1\. Open your web browser and navigate to [https://portal.azure.com\\\](https://portal.azure.com)](https://portal.azure.com%5C%5D\(https://portal.azure.com\)).

Log in using your Azure administrator credentials.


2\. Click "Subscriptions"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/614817a1-30c6-4c89-b3cc-c196f6256910/ascreenshot.jpeg?tl_px=0,571&br_px=1719,1532&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=469,277)


3\. Select the subscription where you want to create the role.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/9d87768b-749f-4686-a83d-3b8ece77f60f/ascreenshot.jpeg?tl_px=0,137&br_px=1719,1098&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=121,277)


4\. Click this button.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/bdf4f528-25a7-4142-92d2-405ea0943b2f/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=333,78)


5\. Under the "Settings" section, click on "Access control (IAM)".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/916f522c-2cb9-4dbb-87b0-8ec00d91b2e5/ascreenshot.jpeg?tl_px=0,21&br_px=1719,982&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=210,277)


6\. Click on "Roles"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/a921855e-5c8e-411a-8832-7582d811afbe/ascreenshot.jpeg?tl_px=282,0&br_px=2002,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,229)


7\. Click "+ Add" and select "Add custom role".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/1efa2995-4f99-43c6-ad78-976cead272ab/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=457,158)


8\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/871e41fb-e450-4000-9459-f31604f9b20a/ascreenshot.jpeg?tl_px=0,5&br_px=1719,966&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=466,277)


9\. Enter a name for your role, such as "Custom Virtual Network Contributor", and optionally provide a description.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5ecccd85-2e4c-4b00-a222-d5992f858572/ascreenshot.jpeg?tl_px=0,75&br_px=1719,1036&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=311,276)


10\. Type "Custom Virtual Network Contributor"


11\. Click "Start from scratch" and then "Next".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/03c3aaa9-e5cc-42ed-9a8e-1846e6cac31e/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=178,569)


12\. In the "Permissions" tab, click "Add permissions". Search for and select "[Microsoft.Network/virtualNetworks/\*](http://Microsoft.Network/virtualNetworks/*)" to enable full management of virtual networks.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/3f5f13e5-845d-4bf3-a0a6-a910b9d6a704/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=351,241)


13\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/9caa8b27-1d0b-4ed4-85fc-ec78907e3784/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=502,74)


14\. Type "Microsoft.Network/virtual"


15\. Click this button

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/7184b299-df93-498d-b0d4-63c65db223eb/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=510,127)


16\. Check mark all. This is just an example of a Custom Role, you can be more granular when selecting permissions.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/523524f6-fe02-4cd4-911c-7242d3760648/ascreenshot.jpeg?tl_px=372,51&br_px=2092,1012&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,277)


17\. Click "add".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/9ea1b49e-ea45-4414-a6e7-4800ded918fc/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=395,562)


18\. Click "next".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/1c66a74f-f85e-460c-8a7a-dc0f06f0e248/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=194,567)


19\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/1e8d0a0a-3b62-4141-8ac1-31f915ce8a17/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=192,569)


20\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/a5d90695-1a7c-4a70-9c90-212c2d1d00aa/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=194,568)


21\. Click Create.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/6c7f606c-0e49-48e2-9105-8119064c44ad/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=22,570)


22\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/b7264770-b9a5-4616-931e-7838175af1a8/ascreenshot.jpeg?tl_px=0,0&br_px=2293,1281&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=45,174)


**Step 3: Assign the Custom RBAC Role**

Navigate back to "Access control (IAM)" in the subscription or resource group where you want to assign the role.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2d37d793-bb2e-4039-b2e5-03227436e883/ascreenshot.jpeg?tl_px=0,5&br_px=1719,966&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=194,277)


24\. Click "Role assignments"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5b9e8a23-16ef-4b8b-bf7d-a6e8ea9c39fe/ascreenshot.jpeg?tl_px=86,0&br_px=1805,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,225)


25\. Click on "Add role assignment".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/0bc78cd2-c938-456d-b85d-de82a3701b65/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=439,172)


26\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5bf5ee3a-3d5a-4653-a5d5-3c7c6a21ce98/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=458,202)


27\. Select the custom role you just created. Search for the Custom role in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/b800cf44-0005-4200-9f33-51236cc7d37a/ascreenshot.jpeg?tl_px=0,189&br_px=1719,1150&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=245,276)


28\. Type "Custom Virtual Network"


29\. Click "Custom Virtual Network Contributor"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/606683b2-7c22-4f15-986c-eab6d0e45793/ascreenshot.jpeg?tl_px=0,177&br_px=2293,1458&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=169,277)


30\. Click "Review + assign"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2acaae03-11fa-4c9f-af36-e07d8953cfdf/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=427,191)


31\. Click "Members"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/fd5e2e6e-bc45-430a-b2b4-e28bc4bcf400/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=54,74)


32\. Click "Select members"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/22b3a07c-1b0c-4dd5-9bfd-9daa401cb200/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=93,184)


33\. Select a group or member to be apart of this Custom role.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/26274160-7fbb-472f-a304-10ffbfb89abc/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=846,103)


34\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/da640a39-c9ab-4a0f-ad22-8442a9ff13dc/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=866,163)


35\. Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/13a6350a-9982-4611-a173-43baefe5f2bf/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=829,571)


36\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/b7bddc91-b7e9-4906-80a7-1b206fb53ddb/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=181,570)


37\. Click "review + assign"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/ef1b1245-7ce2-4ded-9f2a-2908957f7efd/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=60,564)


## Conclusion
This lab provided hands-on experience in creating and assigning custom roles using the Azure portal, equipping participants with the skills needed to manage access effectively within their Azure environment.

## Additional Tips
- Regularly review custom roles and their assignments to ensure they continue to meet your organizational needs.
- Use the "Preview" option to test role permissions before fully deploying them to users.
