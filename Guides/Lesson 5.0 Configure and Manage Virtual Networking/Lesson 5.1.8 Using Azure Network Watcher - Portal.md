# Lab: Using Azure Network Watcher with Azure Portal

## Objective
Learn how to enable and use Azure Network Watcher to monitor and diagnose conditions in Azure using the Azure Portal.

## Prerequisites
- Azure subscription
- Access to Azure Portal

## Steps

### Step 1: Enable Network Watcher
Navigate to the Azure Portal.
1. Go to **All services**.
2. Search for and select **Network Watcher**.
3. In the **Network Watcher** blade, ensure Network Watcher is enabled in your region.

### Step 2: Create a Packet Capture
Create a new packet capture session.
1. Under **Monitoring**, click on **Packet capture**.
2. Click **+ Add** to start a new capture session.
3. Configure the capture session settings:
   - Choose your VM.
   - Specify the storage account to save the capture.
   - Set time and size limits.
4. Click **OK** to start the packet capture.

### Step 3: Analyze the Packet Capture
Once the capture is complete, you can download and analyze the capture file.
1. Go to your storage account.
2. Navigate to the appropriate blob container to find your capture file.

### Step 4: Clean up Resources
If no longer needed, delete the packet capture to avoid unnecessary costs.
1. Return to the **Packet capture** session list.
2. Select your packet capture and click **Delete**.

## Conclusion
This lab demonstrated how to set up and use Azure Network Watcher from the Azure Portal to monitor network performance and diagnose network issues.
