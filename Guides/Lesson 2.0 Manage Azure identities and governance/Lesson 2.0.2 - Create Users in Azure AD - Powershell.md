# Creating a User in Azure Active Directory Using PowerShell

This guide demonstrates how to create a user in Azure Active Directory using PowerShell. This process requires administrative credentials to execute successfully.

## Prerequisites
- You must have access to an Azure account with administrative privileges.
- PowerShell should be installed on your system. For Windows 10 users, PowerShell comes pre-installed.

## Step 1: Open PowerShell
First, you need to open PowerShell. You can do this by searching for PowerShell in your Start menu and right-clicking to "Run as administrator".

## Step 2: Install the AzureAD Module
If you haven't already installed the AzureAD PowerShell module, you can install it using the following command:

```powershell
Install-Module -Name AzureAD
```

![](/Images/Screenshot%202024-04-17%20at%207.02.10%20PM.png)

## Step 3: Connect to Azure AD

Before you can create a user, you need to connect to Azure AD using your admin credentials:

```powershell
Connect-AzureAD
```

This command will prompt you to enter your Azure administrator credentials in a new login window.

## Step 4: Prepare User Details
Create a password profile object and set the required user details like display name and username:

![](/Images/Screenshot%202024-04-17%20at%207.36.22%20PM.png)

```powershell
$PasswordProfile = New-Object -TypeName Microsoft.Open.AzureAD.Model.PasswordProfile
$PasswordProfile.Password = "YourStrongPassword123!"  # Be sure to choose a strong, secure password.

$DisplayName = "New User"
$MailNickname = "newuser"
$UserPrincipalName = "newuser@example.com"
```
## Step 5: Create the User

Now, use the New-AzureADUser command to create the user:

![](/Images/Screenshot%202024-04-17%20at%207.42.54%20PM.png)

```powershell
New-AzureADUser -DisplayName $DisplayName -PasswordProfile $PasswordProfile -UserPrincipalName $UserPrincipalName -MailNickname $MailNickname -AccountEnabled $true
```

- DisplayName: The user's full name.
- PasswordProfile: Contains the user's password.
- UserPrincipalName (UPN): The name used by the user to log in.
- MailNickname: A nickname used internally by Azure AD.
- AccountEnabled: Indicates whether the user account is enabled immediately upon creation.

## Step 6: Verify the User Creation

To verify that the user was created, you can retrieve their details with the following command:

![](/Images/Screenshot%202024-04-17%20at%207.44.38%20PM.png)

```powershell
Get-AzureADUser -ObjectId $UserPrincipalName
```

This command fetches and displays the details of the newly created user.

## Conclusion

You have now successfully created a user in Azure AD using PowerShell. Remember to handle user credentials securely and follow your organization's policies for password management and user creation.