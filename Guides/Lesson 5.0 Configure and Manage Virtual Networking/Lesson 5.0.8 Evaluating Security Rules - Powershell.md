# Lab: Evaluate Effective Security Rules Using PowerShell

## Objective
Evaluate and analyze the effectiveness of security rules applied to NSGs using PowerShell.

## Prerequisites
- Azure subscription
- PowerShell installed
- Azure PowerShell module installed

## Instructions

### Step 1: Log in to Azure

```powershell
Connect-AzAccount
```

### Step 2: List Network Security Groups

```powershell
$resourceGroupName = "YourResourceGroupName"
Get-AzNetworkSecurityGroup -ResourceGroupName $resourceGroupName
```

### Step 3: Retrieve Security Rules for an NSG

```powershell
$nsgName = "YourNSGName"
$nsg = Get-AzNetworkSecurityGroup -Name $nsgName -ResourceGroupName $resourceGroupName
$nsg.SecurityRules
```

### Step 4: Analyze Rules for Potential Issues

```powershell
# Example analysis: List rules allowing traffic from the internet
$internetFacingRules = $nsg.SecurityRules | Where-Object { $_.SourceAddressPrefix -eq "Internet" -and $_.Access -eq "Allow" }
$internetFacingRules | Format-Table Name, Priority, SourceAddressPrefix, DestinationPortRange, Access -AutoSize
```

### Step 5: Adjust Rules If Necessary

```powershell
# Example: Modify an existing rule to restrict access further
$ruleToModify = $nsg.SecurityRules | Where-Object { $_.Name -eq "AllowSpecific" }
$ruleToModify.DestinationPortRange = "443"
Set-AzNetworkSecurityGroup -NetworkSecurityGroup $nsg
```

## Conclusion

This lab guided you through the process of evaluating NSG rules using PowerShell. You learned how to identify and analyze security rules and make necessary adjustments to enhance security.