# Create Azure Bastion Instance Step-by-Step Guide

1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click the "Search resources, services, and docs (G+/)" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/9d2b476c-ca03-4801-8112-ef60b37032e7/ascreenshot.jpeg?tl_px=331,0&br_px=1278,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,-4)


3\. Type "bastion"


4\. Click "Bastions"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/b067c940-1b07-457d-9523-11b4a4699f01/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0)


5\. Now lets create a new Azure Bastion. Click "create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/4dd39140-e4fc-46bd-85f7-a70cfa813c58/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=0,108)


6\. Just like all azure resources we will select a subscription, create  a new or select an existing resource group, and name the resource.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/3c336b85-c1e7-4aec-a153-a0b978790717/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=217,226)


7\. Type "my-name-azure-bastion"


8\. Click "OK"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/245bafa7-55b2-40ef-ab7b-d54f20b5c360/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=231,380)


9\. Click the "Name" field. And give this resource a name.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/4800f85a-6296-40ca-8d84-cb2f58a005cf/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=276,292)


10\. Type "my-name-azure-bastion"


11\. Select "basic" for our Tier.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/57ce497c-09b4-4c00-9133-10a2d24aaec2/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=261,389)


12\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/7db57f39-2b4e-4a8b-b4ee-a769e3c3c619/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=226,440)


13\. Now we will have to select a virtual network for our bastion host. Once we select a VNET we will have to go manage the subnets in that VNET and ensure that we have a subnet created for the specific purpose of a bastion host.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/ac26a363-8154-4f94-b8c7-53609a6cb753/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=221,483)


14\. I will select the VNET i created in step one. Click "my_name_Vnet"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/770c6363-63f5-4a54-b049-c3b9a3a91317/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=257,342)


15\. Click "Manage subnet configuration"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/4aceaf4e-35fe-4c7b-adf3-116955d89a89/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=385,239)


16\. Now let's create a new subnet for our azure bastion service/host.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/aaee6c3c-81f8-44bd-b64c-bbd5c037fe4d/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=322,139)


17\. Select the subnet purpose as "azure bastion."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/528435e2-b219-496a-89a0-231bba8a54c7/ascreenshot.jpeg?tl_px=452,0&br_px=1399,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,150)


18\. Click "Azure Bastion"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d80fd7c1-9a4d-49b9-a4c9-62e276d42cb7/ascreenshot.jpeg?tl_px=164,0&br_px=1541,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=621,217)


19\. By default the subnet should generate an IP address space that is not overlapping. Ensure it is not overlapping and click "add"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d98bf5b7-5604-410a-9a48-e95fa6cb0fc2/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=465,605)


20\. Now lets head back to our bastion creation screen. Click "Create a Bastion"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/3f2870bb-26ce-42d5-8970-0bc8905203f7/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=165,16)


21\. Now we will finish up our configuration of azure bastion. We will move onto the advanced settings and leave everything at its defaults. Click "Next : Advanced &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/d417c704-210a-433f-9af0-a2d15e76b740/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=277,557)


22\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/3ca86df0-b3fe-4af6-ad3e-f5361281b9bd/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=243,613)


23\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/287ce5d3-ef25-43bd-8182-85113310e0fa/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=243,613)


24\. Click "create".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-29/f8342c32-d824-4522-b460-dd3e98718225/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=66,462)


