# Lab 1: Configure Backup for an App Service using PowerShell

## Objective: Learn how to set up and configure backups for an Azure App Service using PowerShell.

## Prerequisites:

- Azure subscription
- Azure PowerShell module installed
- Existing Azure App Service and Storage Account

### Step 1: Log in to Azure

First, open your PowerShell console and log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Set Variables

Set the variables with your specific service and storage account names.

```powershell
$resourceGroupName = "YourResourceGroupName"
$appServiceName = "YourAppServiceName"
$storageAccountName = "YourStorageAccountName"
$containerName = "app-backups"
```

### Step 3: Get the Storage Account Context

Retrieve the storage account context to use in the backup configuration.

```powershell
$storageAccount = Get-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName
$storageAccountContext = $storageAccount.Context
```

### Step 4: Configure the Backup

Create a new backup configuration setting. Adjust the frequency and retention settings as needed.

```powershell
$freq = New-TimeSpan -Day 1
$retentionPeriod = 30

Set-AzWebAppBackupConfiguration -ResourceGroupName $resourceGroupName -Name $appServiceName -StorageAccountUrl $storageAccountContext.BlobEndPoint -Frequency $freq -RetentionPeriodInDays $retentionPeriod -Database $null -BackupName "AppServiceBackup"
```

### Step 5: Initiate the Backup

Optionally, you can initiate a manual backup using the following command.

```powershell
Start-AzWebAppBackup -ResourceGroupName $resourceGroupName -Name $appServiceName -BackupName "ManualBackup"
```

## Conclusion

Verify your backups in the Azure Portal within your storage account. Adjust the frequency and retention settings as necessary to suit your organizational needs.