
# Lesson 2.2.9 : Managing Resource Groups Using the Azure Portal

## Objective
Learn how to create, manage, and delete resource groups in Azure using the Azure portal.

## Prerequisites
- Access to the Azure portal with appropriate permissions.

## Step 1: Log in to the Azure Portal


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


## Step 2: Create a Resource Group

1\. On the portal's homepage, select "Resource groups" from the navigation pane or search for "Resource groups" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/4c7ddfec-d6a6-47dc-b453-5f8339ca8e4b/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-17,-25)


3\. Click "Resource groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/7ba1a0fe-4993-4e86-ba74-8b3d5e985bbd/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=29,126)


4\. Click "+ Create" to start the process of creating a new resource group.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c13ef7a9-fe44-4e90-9307-92c967851104/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=12,88)


5\. Enter the resource group name, such as "MyResourceGroup", select your subscription, and choose a region like "West US 2".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/5577f8ed-a894-451e-8f1d-0c0322ccb38e/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=192,189)


6\. Type "MyResourceGroup"


7\. Click this box to select the Region you want this Resource Group in.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/87603675-bc0c-4376-83b4-24772fc42d82/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=250,245)


8\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/74624f88-6813-4063-91b1-9ca6612ffc04/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=229,357)


9\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/a570c1ff-ea23-4cd2-aa60-d22fa5dc302d/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=197,573)


10\. Add a new tag with the key "Project" and value "AZ-104 Training".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/8fbbe737-ffc3-4d67-b7b8-99a42c81f401/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=130,213)


11\. Type "Project [[tab]] AZ-104 Training"


12\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/7a72d28a-6ce4-4a09-9279-dd3a9222a48a/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=235,567)


13\. Click "create".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/dd8ef95f-209b-4c53-b90c-761ae00b8f04/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=18,566)


## Step 3: How To Delete the Resource Group

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/806cf864-ebd8-40b5-a15c-31b48b380c22/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=58,217)


15\. Click on "Delete resource group" at the top of the page

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/4e090f76-6747-45f4-ba6c-70acdfc1c9e6/ascreenshot.jpeg?tl_px=21,0&br_px=1741,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,61)


16\. Type the resource group name to confirm and click "Delete" if you wanted to delete the resource group. We will not actually delete this resource group for this lab. Just wanted to show you how to do it.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/dd15424c-537d-433d-b59c-25ccfe96048d/ascreenshot.jpeg?tl_px=197,0&br_px=1917,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=1063,1)

## Conclusion

You have successfully created, managed, and deleted a resource group using the Azure portal. This skill is crucial for effective Azure management and operational efficiency.
