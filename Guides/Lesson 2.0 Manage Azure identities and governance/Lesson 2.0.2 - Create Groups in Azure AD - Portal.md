# Create a New Security Group in Microsoft Azure



1\. ### Step 1: Sign in to Azure Portal

First, you need to log into the Azure Portal. If you don't already have access, you will need to obtain credentials that have the necessary permissions to create and manage groups in Azure AD.

- Open your web browser and go to [https://portal.azure.com](https://portal.azure.com/).
- Enter your credentials to log in.


2\. ### **Step 2: Navigate to Azure Active Directory**

Once you're logged in, you need to navigate to the Azure Active Directory section.

- In the left-hand navigation pane, select "Azure Active Directory." This action takes you to the Azure Active Directory dashboard.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/235d7ce5-b572-492d-a1a5-9c5331f1b1eb/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=0,-8)


3\. Click "Microsoft Entra ID"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/a7bb3f14-c437-4e40-add7-e76d2cc0949e/ascreenshot.jpeg?tl_px=0,324&br_px=859,805&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=97,212)


4\. ### **Step 3: Access Groups Management**

From the Azure AD dashboard, you can manage all aspects related to users and groups.

- In the Azure Active Directory blade, look for and click on “Groups.” This opens the "Groups" management page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/7f56409d-342b-4e53-9772-95edb6be4afe/ascreenshot.jpeg?tl_px=0,92&br_px=859,573&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=64,212)


5\. ### **Step 4: Create a New Group**

On the Groups page, you can see all the groups that have been created. To add a new group:

- Click on “+ New group” at the top of the blade.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/73fa31a1-3a36-4d6f-933d-f881b65cf126/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=305,119)


6\. ### **Step 5: Configure Group Settings**

You’ll now see a form to enter details about the new group.

- **Group type**: Choose between "Security" and "Office 365". Security groups are used for managing member and computer access to shared resources for a group of users. Office 365 groups are used with Office 365 services.
- **Group name**: Enter a name for your group. This name will be visible to members.
- **Group description** (optional): Provide a description for the group which can help others understand the purpose of the group.
- **Membership type**: Select whether the group will have "Assigned" membership (where you manually add members) or a dynamic membership (where members are automatically added based on rules).
- **Group email address** (for Office 365 groups): This will be auto-generated based on the group name but can be edited.
- **Owners**: Assign at least one owner to the group who can manage membership and other settings.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/2458b6b0-8320-4b57-af7f-6fc5386a4598/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=256,194)


7\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/f266f1f0-805f-4541-b40a-38b3c0cb9752/ascreenshot.jpeg?tl_px=0,11&br_px=859,492&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=245,212)


8\. Click the "Group name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/a210cac7-b800-4557-a4ab-e3b3f482dac5/ascreenshot.jpeg?tl_px=0,50&br_px=859,531&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=186,212)


9\. Type "Test-group-for-test-users"


10\. Click the "Group description" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/0dcc5d59-f148-4bf4-99de-3ca6349635d0/ascreenshot.jpeg?tl_px=0,101&br_px=859,582&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=105,212)


11\. Click "Assigned"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/34c9fdf7-603d-4906-91cb-98f5dc670046/ascreenshot.jpeg?tl_px=0,163&br_px=859,644&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=155,212)


12\. Click "Assigned"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/e86cde72-8a54-4af5-9a88-c975ca806f29/ascreenshot.jpeg?tl_px=0,163&br_px=859,644&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=156,212)


13\. Type "Test group for our test users in our organization"


14\. Click "No owners selected"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/7741acce-7aa4-49e0-ace7-6eab76e5a7da/ascreenshot.jpeg?tl_px=0,224&br_px=859,705&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=54,212)


15\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/0180b0dc-feae-4391-96dd-5c13ba6ac3f8/ascreenshot.jpeg?tl_px=405,134&br_px=1265,615&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


16\. Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/7f719d6d-f84f-411f-9327-6e654c8d68c0/ascreenshot.jpeg?tl_px=436,470&br_px=1296,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,424)


17\. ### **Step 6: Add Members (Optional)**

If you chose "Assigned" membership type, you can add members during the group creation.

- Click on “No members selected” to open the member selection pane.
- Search for users by name or email address and select them to add them as members of the group.
- Click “Select” to add the chosen users to the group.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/4a2c4233-4d0c-4291-abbb-fdbd40f5fe91/ascreenshot.jpeg?tl_px=0,298&br_px=859,779&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=67,212)


18\. Click "No members selected"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/e852befc-612b-4e9f-8ff7-2c2522fe8b1f/ascreenshot.jpeg?tl_px=0,292&br_px=859,773&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=67,212)


19\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/236c7a30-3ddb-43f0-95fb-f639ea9fde61/ascreenshot.jpeg?tl_px=406,470&br_px=1266,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,263)


20\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/43b342c8-99fd-4ebf-8769-08b4ee396b00/ascreenshot.jpeg?tl_px=414,470&br_px=1274,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,250)


21\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/01d53018-8933-4a71-aa4b-dfad93a85ee7/ascreenshot.jpeg?tl_px=403,184&br_px=1263,665&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


22\. You can search for specific members in your subscription

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/e62873d2-0937-4946-8ce8-2d44f9236b4d/ascreenshot.jpeg?tl_px=509,0&br_px=1369,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,191)


23\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/b6683626-0940-4060-bb12-ba4bafc03f0b/ascreenshot.jpeg?tl_px=399,136&br_px=1259,617&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


24\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/9c6d08fd-3a4a-4e0a-83be-2e331c12c7c0/ascreenshot.jpeg?tl_px=411,180&br_px=1271,661&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


25\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/4f9b20bc-b6e7-45e6-8fad-edf413204bca/ascreenshot.jpeg?tl_px=410,285&br_px=1270,766&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


26\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/ff1170bb-3e97-49bf-b874-dc1941b12300/ascreenshot.jpeg?tl_px=406,335&br_px=1266,816&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


27\. Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/c911b0c4-20de-48e2-86ee-16f4bfbc60c7/ascreenshot.jpeg?tl_px=442,470&br_px=1302,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,427)


28\. ### **Step 7: Review and Create**

Review the information you’ve entered.

- Once everything is correct, click “Create” to finalize the creation of the group.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/92fd9e96-fd66-4a88-bfbf-efa6065d9c79/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=68,424)


29\. ### **Step 8: Verify Group Creation**

After creating the group, you can verify and manage it by:

- Navigating back to the “Groups” page in Azure Active Directory.
- Searching for your new group by name.
- Clicking on the group to view its details and manage its members and settings.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/ccbc3911-ee68-401e-a7d9-273242683fca/ascreenshot.jpeg?tl_px=134,0&br_px=993,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,123)
#### [Made with Scribe](https://scribehow.com/shared/Create_a_New_Security_Group_in_Microsoft_Azure__4sCbx6gDTZabg4OJKlVgCA)


