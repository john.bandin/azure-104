# Lab 1: Configure Scaling Settings in an App Service Plan Using PowerShell

## Objective
Learn how to configure scaling settings for an Azure App Service Plan using PowerShell.

## Prerequisites
- Azure subscription
- Azure PowerShell module installed

## Steps

### Step 1: Login to Azure

Open your PowerShell terminal and log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Set Variables

Set the variables for your resource group and App Service Plan.

```powershell
$resourceGroup = "<YourResourceGroupName>"
$planName = "<YourAppServicePlanName>"
```

### Step 3: Configure Manual Scaling

Set the instance count manually.

```powershell
Set-AzAppServicePlan -ResourceGroupName $resourceGroup -Name $planName -NumberofWorkers 2
```

### Step 4: Configure Auto Scaling

Create an auto-scale setting.

```powershell
$rule1 = New-AzAutoscaleRule -MetricName "CpuPercentage" -MetricResourceId "/subscriptions/{subscriptionId}/resourceGroups/$resourceGroup/providers/Microsoft.Web/serverfarms/$planName" -Operator GreaterThan -Threshold 70 -TimeGrain 00:01:00 -TimeWindow 00:10:00 -ScaleActionCooldown 00:05:00 -ScaleActionDirection Increase -ScaleActionValue 1
$autoscaleProfile = New-AzAutoscaleProfile -Name "autoProfile1" -DefaultCapacity 1 -MaximumCapacity 4 -MinimumCapacity 1 -Rules $rule1
Add-AzAutoscaleSetting -Location "East US" -Name "MyAutoScaleSetting" -ResourceGroupName $resourceGroup -TargetResourceId "/subscriptions/{subscriptionId}/resourceGroups/$resourceGroup/providers/Microsoft.Web/serverfarms/$planName" -AutoscaleProfiles $autoscaleProfile
```

### Step 5: Verify Configuration

Check the configuration settings.

```powershell
Get-AzAppServicePlan -ResourceGroupName $resourceGroup -Name $planName
```

## Conclusion