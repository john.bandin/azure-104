# Microsoft Azure Storage Explorer

![](/Images/Lesson%203.1.7%20Overview%20of%20Azure%20Storage%20Explorer%20and%20AzCopy.png)

1\. Go to the microsoft azure storage explorer page and download "azure storage explorer" for your operating sytem.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ded39b97-c455-4cdb-b1a6-e7e0bd3bcf38/user_cropped_screenshot.jpeg?tl_px=16,138&br_px=3456,2061&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=572,349)


2\. I am on a MacBook so the storage explorer will download as a zip folder, I will unzip the folder and then launch the application.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ac9c4697-36ec-464f-ba7a-59a0b6ee49b1/ascreenshot.jpeg?tl_px=202,0&br_px=2495,1281&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,198)


3\. Once the application is installed, launch it, and it will bring you to the home page of the application.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/d5b5e43d-5c21-4b00-89bd-39b7d4ee3607/ascreenshot.jpeg?tl_px=200,0&br_px=1920,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=557,97)


4\. Now we will sign in using our Azure account. Click the "profile" icon on the left navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/27c503f3-4eed-4802-8316-94bf9eff59de/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=2293,1281&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=9,53)


5\. Click "sign in with Azure"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/1e38cc1f-f6ec-4855-bcbc-d1be6d7509a9/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=148,165)


6\. Choose "azure".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/1bd65c95-1cf1-4441-8a71-83810d0a91ff/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=2539,1556&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=812,573)


7\. You will get redirected to your web browser to login with your Azure Credentials.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/5842cdb2-2322-4be3-b793-f77649e7472d/ascreenshot.jpeg?tl_px=591,548&br_px=2884,1829&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=524,277)


8\. Once you are signed in you can begin to manage storage from this desktop application.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/c4c2de75-1119-4c08-88c9-0127e116800f/user_cropped_screenshot.jpeg?tl_px=0,0&br_px=2293,1281&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=125,135)
#### [Made with Scribe](https://scribehow.com/shared/Microsoft_Azure_Storage_Explorer_Google_Chrome_and_Finder_Workflow__rhKlnJOlSbWS8ZUFK7B5hg)


