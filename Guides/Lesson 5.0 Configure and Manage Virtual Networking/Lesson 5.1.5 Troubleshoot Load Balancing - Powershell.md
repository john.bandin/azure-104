# Lab 1: Troubleshooting Load Balancing with PowerShell

## Objective
Learn how to troubleshoot issues related to Azure Load Balancer using PowerShell.

## Prerequisites
- Azure subscription
- PowerShell installed
- Azure PowerShell module installed

## Steps

### Step 1: Login to Azure
```powershell
Connect-AzAccount
```

### Step 2: Check the health of the Backend Pool

List the backend health of the load balancer.

```powershell
$resourceGroup = "<ResourceGroupName>"
$loadBalancerName = "<LoadBalancerName>"
Get-AzLoadBalancerBackendAddressPool -ResourceGroupName $resourceGroup -Name $loadBalancerName | Get-AzLoadBalancerBackendHealth
```

### Step 3: Verify Load Balancer Rules

Display the load balancing rules and probe settings.

```powershell
Get-AzLoadBalancer -Name $loadBalancerName -ResourceGroupName $resourceGroup | Select -ExpandProperty LoadBalancingRules
```

### Step 4: Examine NSG rules

Check if any Network Security Group rules are blocking the traffic.

```powershell
Get-AzNetworkSecurityGroup -ResourceGroupName $resourceGroup | Get-AzNetworkSecurityRuleConfig
```

### Step 5: Test connectivity to the backend instances

Use Test-NetConnection to verify if the backend instances can be reached.

```powershell
$backendIP = "<BackendIP>"
Test-NetConnection -ComputerName $backendIP -Port <PortNumber>
```

## Conclusion

In this lab, you learned how to check the health and configuration of your Azure Load Balancer using PowerShell, ensuring all components function correctly.