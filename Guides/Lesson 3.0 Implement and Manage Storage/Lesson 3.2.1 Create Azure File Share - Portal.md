
## Lab 2: Creating an Azure File Share using Azure Portal

![](/Images/Lesson%203.2.1%20Configure%20object%20replication.png)

### Objectives:
- Navigate the Azure Portal to create a file share.
- Configure and deploy a file share.

### Prerequisites:
- Access to Azure Portal.

```markdown
### Step 1: Log in to Azure Portal
Open your web browser and navigate to [https://portal.azure.com](https://portal.azure.com). Log in using your Azure account credentials.

### Step 2: Create a Storage Account
1. On the Azure Portal home page, click **Create a resource**.
2. Search for and select **Storage Account** and click **Create**.
3. Fill in the necessary fields such as Subscription, Resource group, Storage account name, and Location.
4. Click **Review + Create** and then **Create**.

### Step 3: Create a File Share
1. Once the storage account is created, navigate to it by clicking **Go to resource**.
2. Under the **Data storage** section, click **File shares**.
3. Click **+ File share**.
4. Enter a name for your file share and specify the quota if necessary.
5. Click **Create**.

### Conclusion
You've now successfully created an Azure File Share using the Azure Portal.
