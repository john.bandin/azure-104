# Lab 1: Troubleshooting External Networking Using PowerShell

## Objective: Use PowerShell to diagnose and troubleshoot external networking issues on Azure.

## Prerequisites
- Azure subscription
- PowerShell with Az module installed
- Appropriate permissions to view and modify network configurations

## Instructions

### Step 1: Login to Azure

```powershell
Connect-AzAccount
```


### Step 2: List All Virtual Networks

Identify the virtual network to troubleshoot.


```powershell
Get-AzVirtualNetwork
```


#### Step 3: Check Effective Network Routes

Inspect the effective routes for a specific subnet within a Virtual Network.

```powershell
Get-AzEffectiveRouteTable -ResourceGroupName "YourResourceGroupName" -VirtualNetworkName "YourVNetName" -SubnetName "YourSubnetName"
```


#### Step 4: Test Network Connectivity

Use Network Watcher's connection troubleshoot feature.

```powershell
Test-AzNetworkWatcherConnectivity -NetworkWatcherName "YourNetworkWatcherName" -ResourceGroupName "YourResourceGroupName" -SourceId "YourVMId" -DestinationAddress "8.8.8.8" -DestinationPort 80
```