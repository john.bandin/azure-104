# Implement Azure Bastion using PowerShell

This lab guide will demonstrate how to set up Azure Bastion using PowerShell.

## Prerequisites
- Azure PowerShell module
- An existing Azure Virtual Network with at least one subnet

## Steps

### 1. Login to Azure
Login to your Azure account and select the subscription where you wish to deploy Azure Bastion.

```powershell
Connect-AzAccount
Select-AzSubscription -SubscriptionName "<Your Subscription Name>"
```

### 2. Define Variables

Set variables for the resource group, location, VNet, and the Bastion subnet.

```powershell
$resourceGroup = "<Your Resource Group Name>"
$location = "<Azure Region>"
$vnetName = "<Your VNet Name>"
$subnetName = "AzureBastionSubnet" # This name is mandatory
$bastionName = "<Your Bastion Name>"
$publicIpName = "<Your Public IP Name>"
```

### 3. Create a Public IP Address

Create a public IP address required for Azure Bastion.

```powershell
$publicIp = New-AzPublicIpAddress -Name $publicIpName -ResourceGroupName $resourceGroup -AllocationMethod Static -Location $location -Sku Standard
```

### 4. Create Bastion Subnet

Ensure that the VNet has a dedicated subnet for the Bastion service.

```powershell
$subnetConfig = Add-AzVirtualNetworkSubnetConfig -Name $subnetName -AddressPrefix "10.0.0.0/27" -VirtualNetwork (Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup)
Set-AzVirtualNetwork -VirtualNetwork $subnetConfig
```

### 5. Deploy Azure Bastion

Deploy the Azure Bastion service.

```powershell
$bastion = New-AzBastion -ResourceGroupName $resourceGroup -Name $bastionName -PublicIpAddress $publicIp -VirtualNetworkId (Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup).Id -Location $location
```

## Conclusion

Once deployed, you can access your VMs through the Azure Portal using Azure Bastion without exposing them to the public internet.