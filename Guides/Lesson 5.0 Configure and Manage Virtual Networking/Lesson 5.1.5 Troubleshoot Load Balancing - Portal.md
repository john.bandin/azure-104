# Lab 2: Troubleshooting Load Balancing with Azure Portal

## Objective
Learn how to troubleshoot Azure Load Balancer issues using the Azure Portal.

## Prerequisites
- Azure subscription
- Access to Azure Portal

## Steps

### Step 1: Access Load Balancer
Navigate to the Azure Portal.
Search for and select **Load balancers**.
Choose your load balancer from the list.

### Step 2: Review Backend Health
Go to **Health probes** under **Settings**.
Check the probe status and the response status from backend instances.

### Step 3: Inspect Load Balancing Rules
Under **Settings**, select **Load balancing rules**.
Review the settings for frontend and backend pool configuration, and the associated health probe.

### Step 4: Analyze Network Security Group (NSG) Rules
Navigate to **Network security groups** from the resource list.
Examine the inbound and outbound security rules to ensure they allow the necessary traffic for load balancing.

### Step 5: Test the Load Balancer
From **Overview** or the load balancer's dashboard, use the public IP to test connectivity.

## Conclusion
This lab guided you through the process of inspecting various configurations of your Azure Load Balancer via the Azure Portal, ensuring it operates effectively.
