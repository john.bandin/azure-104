# Lab 2: Configure AKS Network Connections using Azure Portal

## Objectives:
- Create an AKS cluster using Azure CNI networking.
- Configure network policies.

## Requirements:
- Azure subscription
- Access to Azure Portal

## Instructions:


1. **Log in to Azure Portal**:
   Open your browser and navigate to `https://portal.azure.com/`. Log in with your credentials.

2. **Create a Resource Group**:
   Go to "Resource groups" -> "Add" -> Enter the details for Resource Group Name and Region -> "Review + create" -> "Create".

3. **Create a Virtual Network**:
   Search for "Virtual networks" -> "Add" -> Provide the necessary details like Name, Address space, and Subnet -> "Review + create" -> "Create".

4. **Create an AKS Cluster**:
   - Navigate to "Kubernetes services" -> "Add" -> "Create Kubernetes cluster".
   - Fill in the basics, such as Subscription, Resource group, and Kubernetes cluster name.
   - In "Networking", select "Azure CNI".
   - Under "Network configuration", select the virtual network and subnet created earlier.
   - In "Network policy", select "Calico" to enable network policy features.
   - Proceed through the other settings and click "Review + create" -> "Create".

5. **Verify the Installation**:
   After the deployment is complete, go to the resource and check under "Kubernetes resources" to see if the nodes are ready.

6. **Clean-up Resources**:
   If the resources are no longer needed, navigate to the resource group created and delete it to avoid unnecessary charges.
