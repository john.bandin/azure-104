# Lab 2: Configure Storage Tiers Using Azure Portal

![](/Images/Lesson%203.2.3%20Configure%20storage%20tiers.png)

## Objective
Learn how to change the storage tier of a blob in an Azure Storage account using the Azure Portal.

## Prerequisites
- Azure subscription
- Azure Storage account and a container

## Instructions

### Step 1: Open Azure Portal
Log in to the Azure Portal (`https://portal.azure.com`).

### Step 2: Navigate to Your Storage Account
Find and select your storage account from the dashboard or use the search bar.

### Step 3: Open the Blob Service
In the storage account dashboard, find and click on 'Blobs' under the 'Blob service' section.

### Step 4: Select the Container
Navigate to the container where your blob is stored.

### Step 5: Modify the Access Tier
Click on the specific blob. Under the 'Overview' tab, click on 'Change tier'. Select the desired tier (Hot, Cool, or Archive) and confirm the change.

## Conclusion
This lab demonstrates how to manually change the storage tier of a blob using the Azure Portal. This is useful for managing costs and access frequency of the data stored in Azure.
