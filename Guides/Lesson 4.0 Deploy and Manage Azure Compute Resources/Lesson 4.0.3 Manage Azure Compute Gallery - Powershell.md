# Lab 1: Managing Azure Compute Gallery Images with PowerShell

## Prerequisites
- Azure PowerShell module installed.
- Logged into Azure account using `Connect-AzAccount`.

## Steps

### Step 1: Create a Resource Group
Create a new resource group in a specific region.

```powershell
$resourceGroupName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $resourceGroupName -Location $location
```

### Step 2: Create a Compute Gallery

Create a gallery to store your images.

```powershell
$galleryName = "MyComputeGallery"
New-AzGallery -ResourceGroupName $resourceGroupName -GalleryName $galleryName -Location $location
```

### Step 3: Create an Image Definition

Set up an image definition within the gallery.

```powershell
$imageDefinitionName = "MyImageDefinition"
$osType = "Windows" # or "Linux"
$osState = "Generalized"
$publisher = "MyCompany"
$offer = "MyOffer"
$sku = "2022"
New-AzGalleryImageDefinition -ResourceGroupName $resourceGroupName -GalleryName $galleryName -GalleryImageDefinitionName $imageDefinitionName -Location $location -OsType $osType -OsState $osState -Publisher $publisher -Offer $offer -Sku $sku
```

### Step 4: Create an Image Version

Create a new version of the image.

```powershell
$imageVersion = "1.0.0"
$sourceImageId = "/subscriptions/<subscriptionId>/resourceGroups/<sourceResourceGroup>/providers/Microsoft.Compute/images/<sourceImageName>"
New-AzGalleryImageVersion -ResourceGroupName $resourceGroupName -GalleryName $galleryName -GalleryImageDefinitionName $imageDefinitionName -GalleryImageVersionName $imageVersion -Location $location -SourceImageId $sourceImageId
```

### Conclusion

This lab provided the steps to create and manage images using Azure Compute Gallery via PowerShell.