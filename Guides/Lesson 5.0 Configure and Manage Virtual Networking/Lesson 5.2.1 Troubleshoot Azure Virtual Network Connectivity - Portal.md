# Lab 2: Troubleshooting Azure Virtual Network Connectivity Using the Azure Portal

## Objective
Learn how to troubleshoot virtual network connectivity issues using the Azure Portal.

## Prerequisites
- You must have Contributor or higher access to the Azure subscription.

## Steps

### Step 1: Open Azure Portal

Navigate to [https://portal.azure.com](https://portal.azure.com) and log in with your credentials.

### Step 2: Navigate to Network Watcher

In the search bar at the top of the Azure portal, type "Network Watcher" and select it from the results.

### Step 3: Network Performance Monitor

Click on 'Network Performance Monitor' under 'Monitoring' and set up a monitoring scenario for your network.

### Step 4: Check Network Security Group (NSG) Rules

Navigate to the specific virtual network or VM that you need to troubleshoot. Click on the associated NSG to view and verify the rules.

### Step 5: Review Effective Routes

Navigate to the 'Effective routes' tab for the subnet or network interface card (NIC) to view and analyze the route tables.

### Step 6: Run a Connection Troubleshoot

Under Network Watcher, select 'Connection troubleshoot'. Specify the source and target of your network communication and run the test.

## Conclusion

This lab helps you understand how to utilize the Azure portal for diagnosing and addressing network connectivity issues, leveraging built-in tools for effective troubleshooting.
