# Lab: Manage Azure NSGs and ASGs Using PowerShell

## Objective
Create and configure Network Security Groups (NSGs) and Application Security Groups (ASGs) using Azure PowerShell.

## Prerequisites
- Azure subscription
- PowerShell installed
- Azure PowerShell module installed

## Instructions

### Step 1: Log in to Azure

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group

```powershell
$resourceGroupName = "TestResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $resourceGroupName -Location $location
```

### Step 3: Create an Application Security Group

```powershell
$asgName = "TestASG"
New-AzApplicationSecurityGroup -Name $asgName -ResourceGroupName $resourceGroupName -Location $location
```

### Step 4: Create a Network Security Group

```powershell
$nsgName = "TestNSG"
New-AzNetworkSecurityGroup -Name $nsgName -ResourceGroupName $resourceGroupName -Location $location
```

### Step 5: Create NSG Rules 

```powershell
# Allow HTTP
$rule1 = New-AzNetworkSecurityRuleConfig -Name "AllowHTTP" -Description "Allow HTTP" -Access Allow -Protocol Tcp -Direction Inbound -Priority 100 -SourceAddressPrefix Internet -SourcePortRange "*" -DestinationAddressPrefix "*" -DestinationPortRange 80
# Allow HTTPS
$rule2 = New-AzNetworkSecurityRuleConfig -Name "AllowHTTPS" -Description "Allow HTTPS" -Access Allow -Protocol Tcp -Direction Inbound -Priority 110 -SourceAddressPrefix Internet -SourcePortRange "*" -DestinationAddressPrefix "*" -DestinationPortRange 443

# Add rules to NSG
Add-AzNetworkSecurityGroupRuleConfig -NetworkSecurityGroup $nsg -SecurityRule $rule1
Add-AzNetworkSecurityGroupRuleConfig -NetworkSecurityGroup $nsg -SecurityRule $rule2

# Update NSG
Set-AzNetworkSecurityGroup -NetworkSecurityGroup $nsg
```

### Step 6: Associate ASG with a VM Network Interface

```powershell
$vmName = "TestVM"
$nic = Get-AzNetworkInterface -Name "$vmName-NIC" -ResourceGroupName $resourceGroupName
$nic.IpConfigurations[0].ApplicationSecurityGroups = @((Get-AzApplicationSecurityGroup -Name $asgName -ResourceGroupName $resourceGroupName))
Set-AzNetworkInterface -NetworkInterface $nic
```

## Conclusion

This lab guided you through the steps to create and configure NSGs and ASGs using Azure PowerShell. You have created rules to allow HTTP and HTTPS traffic and associated an ASG with a virtual machine.