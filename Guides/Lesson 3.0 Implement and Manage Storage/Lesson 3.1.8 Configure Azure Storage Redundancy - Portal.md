## Lesson 3.1.8: Configuring Azure Storage Redundancy Using the Azure Portal

![](/Images/Lesson%203.1.8%20Implement%20Azure%20Storage%20redundancy.png)

### Prerequisites
- Access to the Azure Portal

### Steps


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. First we will need to create a new "resource group" for our "storage account". You can use the left navigation pane or use the search bar to find resource groups.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/0a2d39c9-5b03-4520-97fc-812f5c457c77/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-26,-14)


3\. Click "Resource groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6e145a66-a57a-4740-9c0a-76d5bf47c68f/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=29,121)


4\. Click "+ create" in the top navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/b96e5b80-8937-4807-8513-7558276cb118/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=22,83)


5\. Next we will decide what subscription we want this storage account under.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/9c66f140-806d-443e-a00a-c36cdeaf4e7e/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=244,208)


6\. Click "Trepa AZ-104 Training"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/e6c08c75-e3c2-45ee-9ab5-71a7953fa002/ascreenshot.jpeg?tl_px=0,199&br_px=859,680&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=363,212)


7\. Next we will name and create our new "resource group."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/5c982d34-92e9-4a3e-8e96-78074cdab36f/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=239,249)


8\. Type "testForStorageRedundancy"


9\. Next we will click "next: Tags &gt;" and create a tag for this new resource.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/03f90e84-8173-4d96-957a-97443ad05df7/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=192,566)


10\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/630f0886-1ffb-499d-a147-c08e599a3725/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=114,204)


11\. Click "Project"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/8ff9949f-ee92-47a1-b477-db82f4eae7e5/ascreenshot.jpeg?tl_px=0,154&br_px=859,635&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=136,212)


12\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6f32cab5-ea6a-447c-87a2-784a0c0dc1e0/ascreenshot.jpeg?tl_px=0,57&br_px=859,538&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=266,212)


13\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/0012d176-ceab-4453-a396-39dd5af59a7a/ascreenshot.jpeg?tl_px=0,106&br_px=859,587&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=294,212)


14\. \*\*\*NOTE\*\*\* the tags are unique to your organization. We use this tag to identify all resources that are used for our AZ-104 boot camp.

Now click "Next: Review + Create &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/4b26e0f9-af48-4d69-9b59-643529fdaa2f/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=208,572)


15\. Click "create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/0e3e2607-c789-499d-829c-19abbd90ec20/ascreenshot.jpeg?tl_px=0,198&br_px=1376,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=29,561)


16\. Now that we have created our "resource group" lets go create a new "storage account" using the same parameters we just made for our resource group. Use the left navigation pane to find a "storage account"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ac7f943c-644f-4779-b3eb-cff669c2e6c2/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=10,0)


17\. Click "Storage accounts"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6752a89c-2766-48ee-b3b3-46c8f78c5d93/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=32,292)


18\. Now we will click "+ create" to make a new "storage account"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/b605e20f-3b12-4f5a-8e7f-5cc9633785ec/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=32,120)


19\. Click "Trepa AZ-104 Training". This is the same subscription we used to make our resource group in the previous steps.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/8b43b5a7-1440-4f61-a1a6-2dba87d563d6/ascreenshot.jpeg?tl_px=0,144&br_px=859,625&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=326,212)


20\. Click "Trepa AZ-104 Training"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/4ef0344b-ef77-456f-af39-3da69abef9a2/ascreenshot.jpeg?tl_px=0,275&br_px=859,756&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=331,212)


21\. Click "testForStorageRedundancy". This is the "resource group" we just made.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/5523c658-9212-4e96-8980-b3e6647f2234/ascreenshot.jpeg?tl_px=0,442&br_px=859,923&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=355,212)


22\. Now we will name our "storage account". We will name ours "trepatechredundancy". Remember this name is globally unique.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/6cffd19d-4350-4899-8759-ef5024cf5afb/ascreenshot.jpeg?tl_px=0,208&br_px=1146,849&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=306,277)


23\. Click "Zone-redundant storage (ZRS)". We will initially set our "storage account: to ZRS and then go change it after the account has been made. 

Next we will hit "review + create".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/aa27e810-85d1-483e-8c8b-7c1e91cb0b1a/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=291,415)


24\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/61f5a843-a089-42d2-87e7-5eaa9c2443b1/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=242,411)


25\. Click "Go to resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ef89821b-375b-4f5e-9bae-67349cc94c6e/ascreenshot.jpeg?tl_px=1057,0&br_px=1917,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=569,139)


26\. Click "Redundancy"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/552abb68-cb1d-40bf-a016-72aa5ab8d43a/ascreenshot.jpeg?tl_px=0,198&br_px=1376,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=36,316)


27\. Click "Zone-redundant storage (ZRS)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/19f51d2e-3fc4-4d2b-a1e8-1ba0cbb601ad/ascreenshot.jpeg?tl_px=224,4&br_px=1084,485&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


28\. Click "Locally-redundant storage (LRS)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/a3de6082-c074-4d42-9fa4-d20f3b60fd00/ascreenshot.jpeg?tl_px=224,26&br_px=1084,507&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


29\. Now we will hit save and see if our Storage account can change its redundancy. Notice how some of the options were greyed out. Thats because ZRS can only be change to LRS. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/605f74c6-0014-4c98-9d16-bc408bea6966/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=295,131)


30\. Click "Confirm"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/8d44c275-ddb6-4cda-b285-0f6e7f1bacc7/ascreenshot.jpeg?tl_px=0,61&br_px=859,542&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=324,212)


