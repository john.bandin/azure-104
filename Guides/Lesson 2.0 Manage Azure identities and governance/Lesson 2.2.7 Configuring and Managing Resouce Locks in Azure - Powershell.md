# Lesson 2.2.7: Configuring Resource Locks Using PowerShell

## Objective
Learn how to create and manage Azure resource locks using PowerShell.

## Prerequisites
- Azure PowerShell module installed and configured.
- Appropriate permissions to manage locks in the Azure subscription.

## Step 1: Open PowerShell
Open your PowerShell console and ensure you are connected to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Lock

You can apply a lock at various levels (resource, resource group, or subscription). For this example, we'll apply a Delete lock to a resource group.

```powershell
$resourceGroupName = "YourResourceGroupName"
$lockName = "PreventDeleteLock"
New-AzResourceLock -LockLevel CanNotDelete -LockName $lockName -ResourceGroupName $resourceGroupName
```

### Step 3: Verify the Lock

Check that the lock has been applied correctly.

```powershell
Get-AzResourceLock -ResourceGroupName $resourceGroupName
```

## Conclusion

You have successfully created a resource lock using PowerShell. This lock will help prevent accidental deletion of your resource group.