
# Creating an Azure VM Image in a Gallery


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. From the homepage Click "Azure compute galleries".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/daedc7cf-eb02-4562-8ae6-8c5ba1ca82da/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=270,111)


3\. From here we will create a new Azure Compute Gallery. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/52aefd17-0d1a-4bbd-82d8-8c055ef72443/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=10,85)


4\. Now we will choose the appropriate subscription, and create a new resource group. We will need to remember this Resource group when we place an image in our gallery.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c55647dc-f6b1-46cf-af40-6321fcf8616f/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=224,300)


5\. Type "name-azure-gallery", and click 'OK"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/7570d16e-8c5b-44eb-bfc1-ef4b1302d25e/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=236,433)


6\. Now we will give our Azure Gallery an Instance name. Click the "Name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/47d5fa9c-696d-4e82-b9bf-861c96177571/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=268,360)


7\. Type "name_vm_gallery". This name must only contain periods, letters, numbers and underscores. Once we set the name we will move on to the sharing method.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/24b8b0c3-f413-4786-af6b-95b7bf7b42ed/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=349,566)


8\. Click "Role based access control (RBAC)
Role based sharing through Identity Access control. Share based on permissions assigned to users, groups, and a..."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/10bef6ac-f01e-4d43-813f-2f1e455c6c5d/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=272,193)


9\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/552c0ab1-c186-4cce-a788-2d75a07ebf37/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=235,606)


10\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/b2409db5-deb4-42ff-a195-f20b9b3d4715/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=245,613)


11\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/eefd76c9-267f-4409-8c6c-e2e95e090421/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=16,562)


12\. Once the deployment is created we will now go to our virtual machines. Search virtual machines and go to our virtual machines page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d92a64c7-fe22-47c9-8971-54972e5a8096/ascreenshot.jpeg?tl_px=179,0&br_px=1126,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,-7)


13\. Now we will select the virtual machine that we built in our previous lab.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/052b6afb-89a4-47f8-9604-3a51b7f8ce47/ascreenshot.jpeg?tl_px=0,115&br_px=946,644&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=102,233)


14\. Now we will put this virtual machine into our newly created azure compute gallery. Click "capture"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/aa5836dc-438c-4921-890a-1af42593f6c8/ascreenshot.jpeg?tl_px=164,0&br_px=1541,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=823,95)


15\. And then click "image"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c8331040-c55f-4c5f-93cd-324d6ce3bac8/ascreenshot.jpeg?tl_px=594,0&br_px=1541,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=554,176)


16\. First we will change our Resource Group to the Resourece Group we created earlier in this lab.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/5032920e-83be-4d11-bf56-a23521502e4a/ascreenshot.jpeg?tl_px=0,85&br_px=946,614&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=413,233)


17\. Once we have selected the correct Resource Group we will need to select the correct gallery to place this image. Click "Select a gallery"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/a1708cdb-aea6-4ff5-b32e-7095ad9e5239/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=300,358)


18\. Select the correct azure image gallery.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/b6a10400-1982-4a82-aa89-1f4fb1b9a202/ascreenshot.jpeg?tl_px=0,78&br_px=1376,847&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=360,277)


19\. Now we will create a new "target VM image definition". Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/51b295c5-8809-43a6-b5e0-1fa77491dbee/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=254,326)


20\. Click the "VM image definition name" field. And type in a name. For example, "name_image_definition"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/9ffe9f54-eeaf-45b9-8dd4-c167afcd86f8/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=663,76)


21\. Then we will click "ok".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/e7fb2976-622f-4346-9a7f-6f6dfd78d211/ascreenshot.jpeg?tl_px=220,405&br_px=1167,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,469)


22\. Next we will have to make up a version number. We will use the standard naming convention for version numbers. For example "0.0.1". Click the "Version number" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/5d63b1ae-57c8-4be8-85e3-c057ca9ee929/ascreenshot.jpeg?tl_px=0,331&br_px=946,860&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=410,233)


23\. Now we will move on to Tags. Click "Next : Tags &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/91450697-24de-4440-8998-18bfcc928b5a/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=270,556)


24\. Input a tag if you would like, and then Click "Next : Review + create &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d681c631-8d4f-4b1a-ab75-646a1280fad3/ascreenshot.jpeg?tl_px=0,164&br_px=1376,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=271,557)


25\. Now we will create our image. Click "create".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c8dc6188-7587-4607-bd6d-28b66de5befd/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=29,469)
#### [Made with Scribe](https://scribehow.com/shared/Creating_an_Azure_VM_Image_in_a_Gallery__dYkT325USBqJNjBgINAhzA)




## Conclusion
This lab provided the steps to create and manage images using the Azure Compute Gallery via the Azure Portal.
