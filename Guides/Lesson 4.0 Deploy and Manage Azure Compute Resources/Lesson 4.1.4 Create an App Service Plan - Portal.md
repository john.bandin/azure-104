
# Lab 2: Creating an App Service Plan using the Azure Portal


## Objectives
- Create an Azure App Service Plan using the Azure Portal.

## Prerequisites
- Access to the Azure Portal.
- An active Azure subscription.

## Instructions

### Step 1: Log in to Azure Portal

Navigate to [https://portal.azure.com](https://portal.azure.com) and log in with your credentials.

### Step 2: Create a Resource Group

If you do not have a resource group:
1. Search for "Resource groups" in the top search bar and select it.
2. Click on "+ Create" to create a new resource group.
3. Enter a Resource Group name and select your preferred region.
4. Click on "Review + create", then "Create".

### Step 3: Create an App Service Plan

1. Search for "App Services" in the top search bar and select it.
2. Click on "+ Create".
3. Under the "Basics" tab, select your Subscription and the Resource Group you created.
4. Enter a unique name for your App Service under "Instance Details".
5. In "Publish", select "Code" or "Docker Container" depending on your requirements.
6. Under "Runtime stack", select the appropriate runtime for your app.
7. Choose a region that matches your Resource Group's location.
8. Click on "Next: Scaling".
9. Select the appropriate pricing tier by clicking on "Change size" under "Spec Picker".
10. Review your selections and click on "Review + create".
11. Once the validation passes, click on "Create".

### Step 4: Verify the Creation

Once the deployment is complete, go back to the "App Services" page and ensure your new App Service Plan is listed.

## Summary

In this lab, you have successfully created an App Service Plan using the Azure Portal. This plan will provide the compute resources needed to host and manage your web applications.
