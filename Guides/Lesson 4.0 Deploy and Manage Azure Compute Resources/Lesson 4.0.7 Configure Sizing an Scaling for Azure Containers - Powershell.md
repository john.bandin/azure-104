# Lab 1: Configure Sizing for Azure Container Instances Using PowerShell

## Objective
Learn how to configure the CPU and memory resources for an Azure Container Instance using PowerShell.

## Prerequisites
- Azure subscription
- PowerShell Azure module installed
- PowerShell logged in to Azure (`Connect-AzAccount`)

## Steps

### Step 1: Set up the Resource Group

Create a new resource group if you don’t have one:

```powershell
New-AzResourceGroup -Name myResourceGroup -Location EastUS
```

### Step 2: Create a Container Instance

Deploy a container instance with specified CPU and memory:

```powershell
$containerGroup = New-AzContainerGroup `
  -ResourceGroupName myResourceGroup `
  -Name myContainer `
  -Image mcr.microsoft.com/azure-cli `
  -OsType Linux `
  -Cpu 1 `
  -Memory 1.5 `
  -RestartPolicy OnFailure
```

### Step 3: Verify the Container Instance

Check the configuration of the deployed container:

```powershell
Get-AzContainerGroup -ResourceGroupName myResourceGroup -Name myContainer
```

### Step 4: Clean up Resources

Remove the container instance and resource group if no longer needed:

```powershell
Remove-AzContainerGroup -ResourceGroupName myResourceGroup -Name myContainer
Remove-AzResourceGroup -Name myResourceGroup
```

## Conclusion
Remove-AzContainerGroup -ResourceGroupName myResourceGroup -Name myContainer
Remove-AzResourceGroup -Name myResourceGroup
