# Lesson 2.2.8: Applying and Managing Tags Using PowerShell

## Objective
Learn how to apply and manage tags on Azure resources using PowerShell.

## Prerequisites
- Azure PowerShell module installed and configured.
- At least one Azure resource created that you can tag, like a virtual machine, storage account, or resource group.

### Step 1: Open PowerShell
Open your PowerShell console and ensure you are connected to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Tag a Resource

Assuming you have a resource group named 'ExampleResourceGroup', apply a new tag to it.

```powershell
$resourceGroupName = "ExampleResourceGroup"
$tags = (Get-AzResourceGroup -Name $resourceGroupName).Tags

if ($tags -eq $null) {
    $tags = @{}
}

$tags["Project"] = "AZ-104 Boot Camp"
$tags["Department"] = "IT"

Set-AzResourceGroup -Name $resourceGroupName -Tag $tags
```
### Step 3: Verify the Tags

Check that the tags have been applied correctly.

```powershell
Get-AzResourceGroup -Name $resourceGroupName | Select-Object -ExpandProperty Tags
```

## Conclusion

You have successfully applied tags to a resource group using PowerShell. These tags can now be used to organize and manage your Azure resources more effectively.