# Azure VM Management Lab using PowerShell

## Objective
This lab guides you through managing Azure VMs using PowerShell. You will learn to change VM sizes, add data disks, configure network settings, and set up availability options.

## Prerequisites
- Azure subscription
- PowerShell Az module installed
- Existing VM to configure

## Part 1: Manage VM Sizes

```powershell
# Login to Azure
Connect-AzAccount
```

```powershell
# List available VM sizes in a specific region
Get-AzVMSize -Location "East US"
```

```powershell
# Resize VM
$vm = Get-AzVM -ResourceGroupName "YourResourceGroup" -Name "YourVMName"
$vm.HardwareProfile.VmSize = "Standard_DS2_v2"
Update-AzVM -VM $vm -ResourceGroupName "YourResourceGroup"
```

## Part 2: Add Data Disks

```powershell
# Add a new data disk
$diskConfig = New-AzDiskConfig -Location "East US" -CreateOption Empty -DiskSizeGB 128
$dataDisk = New-AzDisk -Disk $diskConfig -ResourceGroupName "YourResourceGroup" -DiskName "MyDataDisk"
Add-AzVMDataDisk -VM $vm -Name "MyDataDisk" -CreateOption Attach -ManagedDiskId $dataDisk.Id -Lun 1
Update-AzVM -VM $vm -ResourceGroupName "YourResourceGroup"
```

## Part 3: Configure VM Network Settings

```powershell
# Set up a virtual network
$vnet = New-AzVirtualNetwork -Name "MyVNet" -ResourceGroupName "YourResourceGroup" -Location "East US" -AddressPrefix "10.0.0.0/16"
$subnet = Add-AzVirtualNetworkSubnetConfig -Name "MySubnet" -AddressPrefix "10.0.1.0/24" -VirtualNetwork $vnet
$vnet | Set-AzVirtualNetwork

# Configure the VM's network interface
$nic = Get-AzNetworkInterface -Name "YourNICName" -ResourceGroupName "YourResourceGroup"
$nic.IpConfigurations[0].SubnetId = $subnet.Id
Set-AzNetworkInterface -NetworkInterface $nic
```

## Part 4: Configure VM Availability Options

```powershell
# Set up an availability set
$avSet = New-AzAvailabilitySet -ResourceGroupName "YourResourceGroup" -Location "East US" -Name "MyAvailabilitySet" -PlatformFaultDomainCount 2 -PlatformUpdateDomainCount 2
$vm.AvailabilitySetId = $avSet.Id
Update-AzVM -VM $vm -ResourceGroupName "YourResourceGroup"
```

## conclusion

This lab covered essential VM management tasks using PowerShell. For more detailed tasks and options, refer to the official Azure PowerShell documentation.