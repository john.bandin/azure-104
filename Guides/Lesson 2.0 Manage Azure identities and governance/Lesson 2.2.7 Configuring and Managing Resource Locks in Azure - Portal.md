
# Lesson 2.2.7: Configuring Resource Locks Using the Azure Portal

## Objective
Learn how to create and manage Azure resource locks using the Azure portal.

## Prerequisites
- Access to the Azure portal with administrative privileges.

## Step 1: Navigate to the Azure Portal
1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click the "settings" bar at you Azure Homepage

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/657d923f-8d10-4e1b-b6a6-f56f11e380ff/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-23,-22)


3\. Click "Resource groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/24237c06-5b0b-4e92-bda8-a16067694e14/ascreenshot.jpeg?tl_px=0,19&br_px=859,500&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=58,212)


4\. Navigate to the specific resource group or resource page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/001a0c83-1e0f-4b14-b1f2-84948e5c8527/ascreenshot.jpeg?tl_px=0,57&br_px=859,538&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=187,212)


5\. In the settings menu, click on "Locks" under "Settings".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f8e41b71-2a45-4079-95ae-8948a81068bf/ascreenshot.jpeg?tl_px=0,335&br_px=859,816&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=378,212)

6\. Click on "+ Add" to create a new lock.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f336dfbe-21a5-4a81-a7a4-ad19b4ee9fda/ascreenshot.jpeg?tl_px=198,0&br_px=1058,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,121)


7\. Enter a name for the lock, such as "PreventDeleteLock".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c36c714b-3e52-4129-bf06-f3cde836d0b1/ascreenshot.jpeg?tl_px=265,45&br_px=1125,526&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


8\. Select the Lock type: "Delete" to prevent deletion, or "Read-only" to prevent any changes.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/54bc5a1a-1f16-4abd-90b7-c6b11eeada38/ascreenshot.jpeg?tl_px=441,47&br_px=1301,528&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


9\. Click "Delete"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/dabea652-d630-40aa-8b9a-9a79cabcd956/ascreenshot.jpeg?tl_px=476,101&br_px=1336,582&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


10\. Click "OK"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/b207c56b-c267-432d-b63d-8cac717f85a4/ascreenshot.jpeg?tl_px=399,145&br_px=1259,626&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)

## Step 4: Verify the Lock
1. Stay on the "Locks" page to see the newly created lock and ensure it is listed there.

![](/Images/Screenshot%202024-04-19%20at%2012.53.47%20PM.png)

## Conclusion
You have successfully created a resource lock using the Azure portal. This lock will prevent unintended deletions or modifications, enhancing the security of your Azure resources.
