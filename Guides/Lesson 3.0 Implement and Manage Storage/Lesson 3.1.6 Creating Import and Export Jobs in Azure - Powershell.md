# Lab 1: Creating an Azure Import Job using PowerShell

## Objective
Learn how to create an Azure Import job using Azure PowerShell to import data into Azure Blob Storage.

## Prerequisites
- Azure subscription
- Azure Storage account
- Azure PowerShell installed
- Hard drives prepared with data to be imported

## Instructions

### Step 1: Install the Azure PowerShell module
Ensure that the Azure PowerShell module is installed by running the following command:

```powershell
Install-Module -Name Az -AllowClobber -Scope CurrentUser
```

### Step 2: Login to Azure

Authenticate to your Azure account:

```powershell
Connect-AzAccount
```

### Step 3: Create a new import job

First, define the parameters such as the storage account name and resource group:

```powershell
$resourceGroupName = 'yourResourceGroup'
$storageAccountName = 'yourStorageAccount'
$storageAccount = Get-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName
```

### Step 4: Ship your drives to Azure

After creating the job, ship your hard drives to the Azure data center as specified in the instructions provided in the Azure portal.

### Step 5: Verify the job status

Check the status of your import job:

```powershell
Get-AzImportExportJob -ResourceGroupName $resourceGroupName -Name $jobName
```
### Conclusion

After your drives arrive at Azure, your data will be uploaded to your specified storage account. Monitor the job status using the command provided.
