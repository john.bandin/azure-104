# Lesson 3.1.5: Configuring Storage Encryption Using the Azure Portal

![](/Images/Lesson%203.1.5%20Configure%20storage%20encryption.png)

## Objectives

- Enable storage encryption using customer-managed keys in Azure Storage.

## Prerequisites

- Azure subscription
- Azure Storage account created

## Instructions

### Step 1: Open Azure Portal

Navigate to [Azure Portal](https://portal.azure.com) and log in with your credentials.

### Step 2: Navigate to Key Vault

Search for and select "Key Vault" from the services list, then click "Add" to create a new key vault if you don't already have one.

### Step 3: Create an Encryption Key

Inside your Key Vault, go to the "Keys" section, click "+ Generate/Import" to create a new key. Enter the necessary details and create the key.

### Step 4: Configure Storage Account

Navigate to your Storage account:
- Under the "Security + networking" section, select "Encryption".
- Choose "Customer-managed keys".
- Click on "Select a key".
- Choose the key you created in your Key Vault.
- Save your settings.

## Conclusion

You have now configured storage encryption using customer-managed keys through the Azure Portal.