# Lab 2: Monitoring On-Premises Connectivity Using Azure Portal

## Objectives:
- Utilize the Azure portal to set up and monitor connectivity using Azure Network Watcher.

## Prerequisites:
- You must have access to the Azure portal with at least Contributor permissions.

## Instructions:

### Step 1: Open Azure Network Watcher
- Navigate to the Azure portal at https://portal.azure.com.
- Search for "Network Watcher" in the search bar and select it.

### Step 2: Enable Network Watcher
- Check if Network Watcher is enabled in your region. If not, click on "+ Add" to enable it for your region.

### Step 3: Monitor VPN Gateway
- In the Network Watcher blade, scroll to the "Monitoring" section and click on "VPN diagnostics".
- Select the subscription, resource group, and the VPN gateway you want to monitor.
- Click on "Start Troubleshooting" to begin the diagnostic test.

## Conclusion:
This lab has guided you through using the Azure portal to enable Network Watcher and to utilize VPN diagnostics to monitor the health of on-premises connectivity.
