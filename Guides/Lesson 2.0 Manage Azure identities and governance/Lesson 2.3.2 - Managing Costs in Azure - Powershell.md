# Lesson 2.3.2: Managing Costs Using PowerShell

This lab will guide you through the steps of setting up alerts and budgets using Azure PowerShell.

## Prerequisites:

- Azure PowerShell module installed.
- Appropriate permissions to manage cost-related configurations in your Azure subscription.

## Objective:

Configure cost management settings using PowerShell to set up alerts and budgets.

## Step 1: Connect to Azure

First, open your PowerShell and connect to your Azure account:

```powershell
Connect-AzAccount
```

## Step 2: Create a Budget

You can create a budget for your Azure subscription using the New-AzConsumptionBudget command. Replace <SubscriptionId>, <Amount>, <StartDate>, and <EndDate> with your details.

```powershell
$budgetParams = @{
    Amount = 500 # The budget amount in your currency
    Name = "MonthlyBudget"
    Category = "Cost"
    TimeGrain = "Monthly"
    StartDate = "2023-04-01" # Use format YYYY-MM-DD
    EndDate = "2023-12-31" # Use format YYYY-MM-DD
    ResourceGroupName = "<ResourceGroupName>"
    NotificationKey = "FirstNotification"
    NotificationThreshold = 90
    ContactEmails = @("your-email@example.com")
    NotificationEnabled = $true
}

New-AzConsumptionBudget @budgetParams
```

## Step 3: Set Up an Alert Rule

Use the Add-AzMetricAlertRuleV2 command to create an alert based on cost metrics.

```powershell
Add-AzMetricAlertRuleV2 -Name "CostThresholdAlert" -ResourceGroupName "<ResourceGroupName>" -WindowSize 1h -Frequency 1h -MetricName "TotalCost" -Operator GreaterThanOrEqual -Threshold 100 -ActionGroup "/subscriptions/<SubscriptionId>/resourceGroups/<ResourceGroupName>/providers/microsoft.insights/actionGroups/<ActionGroupName>"
```

## Conclusion

You've now set up a budget and an alert for your Azure resources using PowerShell. Monitor your email for any alerts.