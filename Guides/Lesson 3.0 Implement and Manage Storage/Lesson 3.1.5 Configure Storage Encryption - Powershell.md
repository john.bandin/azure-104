# Lab 1: Configuring Storage Encryption Using PowerShell

![](/Images/Lesson%203.1.5%20Configure%20storage%20encryption.png)

## Objectives

- Enable storage encryption using customer-managed keys in Azure Storage.

## Prerequisites

- Azure subscription
- Azure PowerShell module installed
- Azure Storage account created

## Instructions

### Step 1: Login to Azure

Open your PowerShell console and log in to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a Resource Group

Create a new resource group, if you don't already have one.

```powershell
New-AzResourceGroup -Name 'MyResourceGroup' -Location 'EastUS'
```

## Step 3: Create a Key Vault

Create an Azure Key Vault to manage your encryption keys.

```powershell
New-AzKeyVault -Name 'MyKeyVault' -ResourceGroupName 'MyResourceGroup' -Location 'EastUS'
```

## Step 4: Create an Encryption Key

Generate a new key to be used for encryption.

```powershell
Add-AzKeyVaultKey -VaultName 'MyKeyVault' -Name 'MyEncryptionKey' -Destination 'Software'
```

## Step 5: Set the Storage Account to Use the Customer-Managed Key

Associate the customer-managed key with your storage account.

```powershell
$Key = Get-AzKeyVaultKey -VaultName 'MyKeyVault' -Name 'MyEncryptionKey'
Set-AzStorageAccount -ResourceGroupName 'MyResourceGroup' -Name 'MyStorageAccount' -KeyVaultEncryptionKeyUrl $Key.Key.kid
```

## Conclusion

You have now configured storage encryption using customer-managed keys through PowerShell.