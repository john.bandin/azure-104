# Lab 1: Configure Sizing and Scaling for Azure Container Instances Using PowerShell

## Objective
This lab will guide you through the process of setting up and configuring the sizing for Azure Container Instances using PowerShell.

## Prerequisites
- Azure subscription
- PowerShell with Azure module installed
- PowerShell logged in to Azure (`Connect-AzAccount`)

## Steps

### Step 1: Set up the Resource Group

Create a new resource group:

```powershell
New-AzResourceGroup -Name myResourceGroup -Location EastUS
```

### Step 2: Deploy a Container Instance

Create a container instance within the resource group with specific CPU and memory:

```powershell
$containerGroup = New-AzContainerGroup `
  -ResourceGroupName ACILabResourceGroup `
  -Name mycontainergroup `
  -Image mcr.microsoft.com/azure-cli `
  -OsType Linux `
  -Cpu 1 `
  -Memory 1.5 `
  -RestartPolicy OnFailure
```

### Step 3: Verify the Configuration

Check the properties of the container instance to ensure correct CPU and memory allocation:

```powershell
Get-AzContainerGroup -ResourceGroupName ACILabResourceGroup -Name mycontainergroup
```

### Step 4: Clean Up Resources

(Optional) Delete the container instance and resource group:

```powershell
Remove-AzContainerGroup -ResourceGroupName ACILabResourceGroup -Name mycontainergroup
Remove-AzResourceGroup -Name ACILabResourceGroup
```

## Conclusion

This lab demonstrated how to configure CPU and memory for an Azure Container Instance using PowerShell, providing control over the resources available to your containers.