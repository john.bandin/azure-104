# Lab 1: Managing Subscriptions Using PowerShell

## Objective
Learn how to view and manage Azure subscriptions using PowerShell.

## Prerequisites
- Azure PowerShell module installed and configured.

## Step 1: Open PowerShell and Connect to Azure
Ensure you are connected to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: List All Azure Subscriptions

List all the subscriptions available under your Azure account.

```powershell
Get-AzSubscription
```

## Step 3: Select a Subscription

Select a specific subscription to manage.

```powershell
$subscriptionId = "your-subscription-id"
Select-AzSubscription -SubscriptionId $subscriptionId
```

## Step 4: Manage Subscription Details

View and modify details of the selected subscription (for example, tagging).

```powershell
$tags = (Get-AzSubscription -SubscriptionId $subscriptionId).Tags

if ($tags -eq $null) {
    $tags = @{}
}

$tags["Department"] = "Finance"
Set-AzSubscription -SubscriptionId $subscriptionId -Tag $tags
```

## Step 5: Verify Changes

Check the tags to verify the changes.

```powershell
Get-AzSubscription -SubscriptionId $subscriptionId | Select-Object -ExpandProperty Tags
```

## Conclusion

You have successfully viewed and managed an Azure subscription using PowerShell. This includes selecting a subscription and modifying its tags for better resource management.