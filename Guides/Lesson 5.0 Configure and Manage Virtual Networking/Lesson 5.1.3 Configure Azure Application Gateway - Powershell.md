# Lab: Configure Azure Application Gateway using PowerShell

## Objective:
Learn how to set up Azure Application Gateway using PowerShell.

## Prerequisites:
- Azure PowerShell module installed
- Azure subscription and permissions to create resources

## Instructions:

### Step 1: Log in to Azure

Open PowerShell and log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Create a new resource group

Create a new resource group to hold the resources for this lab.

```powershell
$rgName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $rgName -Location $location
```

### Step 3: Create a virtual network and a subnet

Create a virtual network and a dedicated subnet for the application gateway.

```powershell
$vnetName = "MyVNet"
$subnetName = "AppGatewaySubnet"
$subnetConfig = New-AzVirtualNetworkSubnetConfig -Name $subnetName -AddressPrefix "10.0.1.0/24"
$vnet = New-AzVirtualNetwork -ResourceGroupName $rgName -Location $location -Name $vnetName -AddressPrefix "10.0.0.0/16" -Subnet $subnetConfig
```

### Step 4: Create an IP configuration and frontend port

Create the IP configuration and a frontend port for the application gateway.

```powershell
$gipConfig = New-AzApplicationGatewayIPConfiguration -Name "gatewayIPConfig" -Subnet $vnet.Subnets[0]
$fipConfig = New-AzApplicationGatewayFrontendIPConfig -Name "frontendIP" -PublicIPAddress $publicIP
$frontendPort = New-AzApplicationGatewayFrontendPort -Name "appGatewayFrontendPort" -Port 80
```

### Step 5: Create the backend pool and settings

Define the backend pool and configure the settings for the application gateway.

```powershell
$backendPool = New-AzApplicationGatewayBackendAddressPool -Name "appGatewayBackendPool" -BackendIPAddresses "192.168.1.1", "192.168.1.2"
$backendHttpSettings = New-AzApplicationGatewayBackendHttpSettings -Name "appGatewayBackendHttpSettings" -Port 80 -Protocol Http -CookieBasedAffinity Disabled
```

### Step 6: Create the application gateway

Finally, create the application gateway with a basic request routing rule.

```powershell
$listener = New-AzApplicationGatewayHttpListener -Name "appGatewayHttpListener" -FrontendIPConfiguration $fipConfig -FrontendPort $frontendPort -Protocol Http
$rule = New-AzApplicationGatewayRequestRoutingRule -Name "rule1" -RuleType Basic -HttpListener $listener -BackendAddressPool $backendPool -BackendHttpSettings $backendHttpSettings
$appGateway = New-AzApplicationGateway -Name "MyAppGateway" -ResourceGroupName $rgName -Location $location -GatewayIPConfigurations $gipConfig -FrontendIPConfigurations $fipConfig -FrontendPorts $frontendPort -BackendAddressPools $backendPool -BackendHttpSettingsCollection $backendHttpSettings -HttpListeners $listener -RequestRoutingRules $rule
```

### Step 7: Verify the deployment

Check the deployment and ensure the application gateway is set up correctly.

```powershell
Get-AzApplicationGateway -Name "MyAppGateway" -ResourceGroupName $rgName
```

## Conclusion:

You have successfully configured Azure Application Gateway using PowerShell. This setup now handles web traffic routing to your specified backend services efficiently.