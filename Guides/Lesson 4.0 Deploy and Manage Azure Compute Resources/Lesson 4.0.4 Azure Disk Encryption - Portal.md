# Creating a Key Vault for Azure Virtual Machine Disk Encryption

# **NOTE** **IF YOUR VM DOES NOT HAVE 8GB OF MEMORY THE AZURE DISK ENCRYPTION WILL FAIL. WE CAN STILL GO THROUGH THE STEPS OF CREATION TO GET THE FULL HANDS ON OF SETTING ADE** **NOTE**

\. Navigate to <https://portal.azure.com/#home>

### Step 1.

First we will have to create another VM. Create a new VM because the VM we used in our Compute Gallery will not be able to be started since we "generalized" the VM. Follow the steps in Lesson 4.0.2 to create another VM, once you have done that continue to Step 2.

### Step 2.

2\. Click "Virtual machines"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/90250172-dc86-4b81-81e9-d0efe906c8f5/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=229,94)


3\. Click on the appropriate VM. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/6dcc55ac-b914-4b46-a5a6-ea3fc401d8a0/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=72,236)


4\. If the VM is stopped we will have to start it. Azure Disk Encryption only works when the VM is turned on.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/a5dfbaa4-619e-4e2d-a598-7294f9115910/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=328,81)


5\. Click "Disks" from the left hand navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/ebf01726-520d-490c-8ae8-03862ecd7e42/ascreenshot.jpeg?tl_px=0,97&br_px=1376,866&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=92,277)


6\. Click here "additional settings" to access the encryption settings.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/9a596a99-add1-41a2-bf53-cd88d75ff1a4/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=373,103)


7\. Now from here we will select the "disks to encrypt". Click "OS disk"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/03051835-4599-4e55-a159-113ed1fb4008/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=241,335)


8\. Now we will need to select a key vault. If a key vault is not already made we will need to create a new key vault. Click "Select a key vault"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3c261c51-68a4-4829-957a-effe100385ad/ascreenshot.jpeg?tl_px=0,382&br_px=946,911&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=338,233)


9\. Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/2c5b149b-b970-4713-b2a0-daec13597762/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=304,240)


10\. Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/8ff7ab82-9bd3-45f5-ab99-f8e96bcce2a2/ascreenshot.jpeg?tl_px=0,293&br_px=946,822&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=333,233)


11\. Type "my-name-key-vault"


12\. Click "ok"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c77c42af-91c6-423c-81ff-2cb705fdff40/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=323,310)


13\. Click the "Key vault name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/84abf41d-7c2c-4501-bf30-12eef0cd816c/ascreenshot.jpeg?tl_px=0,363&br_px=946,892&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=361,233)


14\. Type "my-name-key-vault"


15\. Now we will leave everything else at default.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3f25acb8-0a85-47dc-9278-e80cf26b63c5/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,617)


16\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/6b92f34e-1d67-4709-a72c-1c22d0fdd695/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,617)


17\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d990f93f-10e3-47cd-8c1b-53ff2656902c/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,617)


18\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/85b148cf-aa61-4bfa-92ef-fbe8279667de/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,617)


19\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3ba534b8-f304-408b-b174-4a44db2935d8/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=162,610)


20\. Now we will have to select a key to use. If there is not a key available we will have to create a new key. Click "Select a key"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/08a8d417-d98d-40a7-a722-66b5e348774d/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=350,297)


21\. Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c5ff727b-7424-4e86-a756-abaf3eb73385/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=327,317)


22\. Click the "Name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/9110b2f6-11cc-4eaf-aea2-9eb3fdb87072/ascreenshot.jpeg?tl_px=469,0&br_px=1416,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,108)


23\. Type "my-name-key"


24\. We will leave the keys encryption standards at the defaults. And then click "create".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/9c19144d-f1c7-4e27-baaf-c89358f793bf/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=469,342)


25\. Click "Save"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/000fee58-9a6d-438a-ba7b-2d2682921226/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=48,448)




### Conclusion
By following these steps, you will have successfully enabled Azure Disk Encryption using the Azure Portal. This will help in protecting your data by ensuring that the VM's disks are encrypted with BitLocker, using keys managed in Azure Key Vault.
