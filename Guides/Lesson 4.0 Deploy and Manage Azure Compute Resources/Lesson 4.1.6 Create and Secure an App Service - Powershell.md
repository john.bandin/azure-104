# Lab 1: Create and Secure an Azure App Service using PowerShell

## Prerequisites

- Azure subscription
- PowerShell installed on your machine
- Azure PowerShell module installed

## Objectives

- Create an Azure App Service
- Secure the App Service with HTTPS and authentication

### Step 1: Log in to Azure

Log in to your Azure account using Azure PowerShell:

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group

Create a resource group, if you don't already have one:

```powershell
New-AzResourceGroup -Name MyResourceGroup -Location "East US"
```

### step 3: Create an App Service Plan

Create an App Service Plan with a free tier:

```powershell
New-AzAppServicePlan -Name MyPlan -Location "East US" -ResourceGroupName MyResourceGroup -Tier Free
```

### Step 4: Create an App Service

Create an App Service within the App Service Plan:

```powershell
New-AzWebApp -Name MyWebApp -ResourceGroupName MyResourceGroup -Location "East US" -AppServicePlan MyPlan
```

### Step 5: Enable HTTPS

Enable HTTPS only for the App Service:

```powershell
Set-AzWebApp -ResourceGroupName MyResourceGroup -Name MyWebApp -HttpsOnly $true
```

### Step 6: Configure Authentication

Enable and configure default authentication:

```powershell
Set-AzWebAppAuthSettings -ResourceGroupName MyResourceGroup -Name MyWebApp -Enabled $true -DefaultProvider AzureActiveDirectory
```

## Summary

You have now created and secured an Azure App Service using PowerShell. Check your Azure portal to see the deployed App Service.