# How to Create a Group in Azure Active Directory Using PowerShell

This guide provides instructions on how to create a new group in Azure Active Directory using PowerShell, which is useful for automating tasks in a managed environment.

## Prerequisites
- Access to a Microsoft Azure subscription with administrative privileges.
- Azure PowerShell module installed. You can install it by running `Install-Module -Name AzureAD` or `Install-Module -Name AzureADPreview` in your PowerShell terminal as an administrator.

## Step 1: Connect to Azure AD
Before you create a group, you must be connected to Azure AD through PowerShell.

```powershell
Connect-AzureAD
```
You will be prompted to enter your administrator credentials in a login window.

## Step 2: Prepare Group Information

Decide the details for your group, including its display name, mail nickname (required), and whether it should be a security or Office 365 group.

- **DisplayName:** The name displayed in the Azure portal and accessible by users.
- **MailNickname:** A unique nickname used in email addresses, if the group is mail-enabled.
- **SecurityEnabled:** True for security groups, false for Office 365 groups.

## Step 3: Create the Group

Execute the following command to create a new group. Adjust the parameters according to your requirements.

![](/Images/Screenshot%202024-04-17%20at%208.03.49%20PM.png)

```powershell
New-AzureADGroup -DisplayName "Your Group Name" -MailNickname "yourgroupnickname" -MailEnabled $false -SecurityEnabled $true -Description "Description of the Group"
```

## Step 4: Verify the Group Creation

To ensure the group was created successfully, you can retrieve it by using its display name or other properties.

![](/Images/Screenshot%202024-04-17%20at%208.05.17%20PM.png)

```powershell
Get-AzureADGroup -SearchString "Your Group Name"
```

This command will list the group details if the creation was successful.

## Conclusion

You have successfully created a group in Azure Active Directory using PowerShell. This method is ideal for automating group management tasks in large organizations or for those who prefer scripting to manual management.