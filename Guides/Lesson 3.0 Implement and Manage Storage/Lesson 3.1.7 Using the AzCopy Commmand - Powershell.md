# Lesson 3.1.7: Managing Data Using PowerShell and AzCopy

![](/Images/Lesson%203.1.7%20Overview%20of%20Azure%20Storage%20Explorer%20and%20AzCopy.png)

## Objectives

- Learn how to use PowerShell to manage Azure storage data using AzCopy.

## Requirements

- Windows PowerShell or PowerShell Core.
- AzCopy installed on your machine.

## Steps

1. Open PowerShell: Start by opening your PowerShell terminal.

2. Set up Environment Variables: Set the path to where AzCopy is installed. Example:

```powershell
$env:PATH += ";C:\Program Files\AzCopy\"
```

3. Upload Files to Azure Blob Storage:

```powershell
azcopy cp "C:\local\path\*" "https://<yourstorageaccount>.blob.core.windows.net/<container>/<SAS-token>" --recursive
```

4. Verify the Upload: Check your Azure portal or Storage Explorer to confirm the files have been uploaded.