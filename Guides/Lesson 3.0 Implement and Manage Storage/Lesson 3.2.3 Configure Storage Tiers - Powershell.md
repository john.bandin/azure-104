# Lesson 3.2.3: Configuring Storage Tiers using PowerShell

![](/Images/Lesson%203.2.3%20Configure%20storage%20tiers.png)

This lab demonstrates how to configure storage tiers using Azure PowerShell. Make sure you have the Azure PowerShell module installed and configured on your system.

## Objective
Learn how to change the storage tier of a blob in an Azure Storage account using PowerShell.

## Prerequisites
- Azure subscription
- Azure Storage account and a container
- Azure PowerShell installed

## Instructions

### Step 1: Log in to Azure
Open PowerShell and log in to your Azure account.
```powershell
Connect-AzAccount
```

### Step 2: List Storage Accounts

List all the storage accounts to find the name of the storage account you want to work with.

```powershell
Get-AzStorageAccount
```

### Step 3: Set the Context

Set the context to the specific storage account and container.

```powershell
$context = New-AzStorageContext -StorageAccountName "<StorageAccountName>" -StorageAccountKey "<StorageAccountKey>"
```

### Step 4: Change the Blob's Tier

Change the tier of a specific blob. Replace <containerName> and <blobName> with your actual container and blob name.

```powershell
Set-AzStorageBlobTier -Context $context -Container "<containerName>" -Blob "<blobName>" -StandardBlobTier Hot
```

## Conclusion

This PowerShell script changes the storage tier of a specified blob. You can modify the tier in the last command to Cool or Archive based on your requirements.