## Lab 1: Configure Azure DNS using PowerShell

### Step 1: Open PowerShell
Ensure that PowerShell is running with administrative privileges.

### Step 2: Log in to Azure
Log in to your Azure account using the following command:

```powershell
Connect-AzAccount
```

### Step 3: Create a Resource Group

Create a resource group, if you do not have one already:

```powershell
$resourceGroupName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $resourceGroupName -Location $location
```

### Step 4: Create a DNS Zone

Create a DNS zone in Azure. Replace "mydomain.com" with your domain name:

```powershell
$zoneName = "mydomain.com"
New-AzDnsZone -Name $zoneName -ResourceGroupName $resourceGroupName
```

### Step 5: Add DNS Records

Add an A record to your DNS zone:

```powershell
$recordName = "www"
$ipv4Address = "192.168.1.1"
New-AzDnsRecordSet -Name $recordName -RecordType "A" -ZoneName $zoneName -ResourceGroupName $resourceGroupName -Ttl 3600 -DnsRecords (New-AzDnsRecordConfig -Ipv4Address $ipv4Address)
```

### Step 6: Verify DNS Records

Check the list of DNS records in your zone:

```powershell
Get-AzDnsRecordSet -ZoneName $zoneName -ResourceGroupName $resourceGroupName
```

## Conclusion

You have successfully configured Azure DNS using PowerShell. Replace values accordingly to fit your configuration needs.