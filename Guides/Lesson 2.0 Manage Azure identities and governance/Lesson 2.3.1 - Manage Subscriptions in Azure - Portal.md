
# Lesson 2.3.1: Managing Subscriptions Using the Azure Portal

## Objective
Learn how to view and manage Azure subscriptions using the Azure portal.

## Prerequisites
- Access to the Azure portal with administrative privileges.

## Set Up Azure Subscription for AZ-104 Training


1\. Navigate to [https://portal.azure.com/?quickstart=True#home](https://portal.azure.com/?quickstart=True#home)


2\. Click the "subscriptions" icon or search for "subscriptions" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/ba2c0a40-3f6c-47df-ae3e-ac972ecd1213/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=346,51)


3\. From here we will create a new subscription. Click the "+add" icon.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/fc69dd40-6ef9-432d-b1c6-c77c8ba8d4eb/ascreenshot.jpeg?tl_px=0,0&br_px=1146,640&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=2,110)


4\. Click the "Subscription name" field. And give the subscription a name that makes sense for your organizational needs/requirements. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/acff7537-3d56-4789-b50a-133f3cad9c1e/ascreenshot.jpeg?tl_px=47,0&br_px=1423,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=523,252)


5\. Type "AZ-104 Training"


6\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/2ad397c4-9f1e-4f62-b5f5-5b33cf5661dc/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=369,569)


7\. Click the "Enter or select your subscription owner" field if you want to change the owner of the subscription.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/08231c09-5ff2-4b92-821f-b5b611a67fdd/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=494,230)


8\. Now we will click "next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/cf9139a4-dfa4-4f3e-9752-b87326383fd1/ascreenshot.jpeg?tl_px=0,0&br_px=1917,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=340,510)


9\. Now we will set a budget for our subscription. First we will give the budget a name that makes sense for our organization.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/01c04b22-4550-47fc-b8bd-c5864dc5f473/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=380,185)


10\. Type "Cheap-Budget-for-Training"


11\. Click the "Amount (in USD)" field. Here we will set our budget for the subscription. We will make our budget $50.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/43e70ef8-d05f-40c8-a49b-d51fb71fd2ee/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=369,212)


12\. Type "50"


13\. Next we will set up an alert condition. We will set our condition to alert us when our budget hits %80.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/8ecd2b30-186d-4f3b-8fe1-95ec613730c4/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=419,236)


14\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/8e1f9635-58af-434b-9e39-fd9ec7cc9c36/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=422,237)


15\. Type "80"


16\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/98e7b71c-83b4-418b-baf8-eb55474097b4/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=374,570)


17\. Next we will set some tags on our subscription.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c98be952-0a16-4ac7-92e7-60922cddb0d9/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=385,243)


18\. Type "Project [[tab]] AZ-104"


19\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/df10fd1f-c236-4045-8c6b-639a640c39ba/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=372,565)


20\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/d3a2e6cd-9d70-4047-acfc-98e0e7bb5348/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=217,573)


21\. Once our subscription is made we will get a "success" message. Lets click "go to subscription".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/37be856b-c370-44de-9d30-c57e9898e835/ascreenshot.jpeg?tl_px=540,0&br_px=1917,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=885,185)


22\. This will bring us to our subscriptions main landing page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/7eda73b3-b38b-48bf-9967-324637ffc9b9/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=61,255)



## Step 5: Review the Subscription
Navigate through different settings like "Cost analysis", "Budgets", and "Policies" to better understand the subscription management features.

## Conclusion
You have successfully navigated and managed an Azure subscription using the Azure portal. This skill is essential for tracking, organizing, and controlling your Azure resources effectively.
