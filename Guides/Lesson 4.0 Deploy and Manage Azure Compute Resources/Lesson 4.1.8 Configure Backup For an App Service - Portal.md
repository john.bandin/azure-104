
# Lab 2: Configure Backup for an App Service using the Azure Portal

## **Objective**: Learn how to set up and configure backups for an Azure App Service using the Azure Portal.

## **Prerequisites**:
- Azure subscription
- Existing Azure App Service and Storage Account

### Step 1: Open Azure Portal

Navigate to [https://portal.azure.com](https://portal.azure.com) and log in with your credentials.

### Step 2: Navigate to Your App Service

Find your App Service by searching for "App Services" in the top search bar, then select your App Service from the list.

### Step 3: Configure Backup

1. In the blade of your App Service, scroll down to the "Settings" group and click on "Backups."
2. Click on "Configure" and then "Add" to create a new backup configuration.
3. Select an existing Storage Account and specify the Container where backups will be stored.
4. Set the backup schedule (e.g., daily at 2 AM) and the retention period (e.g., 30 days).
5. Optionally, you can include the databases connected to your App Service in the backup.
6. Click "Save" to apply your backup settings.

### Step 4: Initiate Manual Backup

Optionally, you can perform a manual backup by clicking "Backup now" in the backup settings page.

### Conclusion

You have now configured scheduled backups for your Azure App Service using the Azure Portal. Ensure to monitor the backups regularly and verify their integrity.
