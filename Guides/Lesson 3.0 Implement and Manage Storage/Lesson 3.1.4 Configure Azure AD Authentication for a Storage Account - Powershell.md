# Lab 1: Configuring Azure AD Authentication Using PowerShell

This lab guides you through the steps to configure Azure AD authentication for an Azure Storage account using PowerShell.

## Prerequisites

- Azure PowerShell module

- An Azure subscription and appropriate permissions to create and manage Azure resources.

## Instructions

## Step 1: Login to Azure
Open PowerShell and run the following command to sign in to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a New Storage Account

Create a new storage account using the New-AzStorageAccount command.

```powershell
$resourceGroupName = "<YourResourceGroupName>"
$location = "<YourLocation>"
$storageAccountName = "<YourStorageAccountName>"

New-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName -Location $location -SkuName "Standard_LRS"
```

## Step 3: Enable Azure AD Authentication

Enable Azure AD directory service authentication for your storage account.

```powershell
Set-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName -EnableAzureActiveDirectoryDomainServicesForFile $true
```

## Step 4: Assign Azure RBAC Roles

Assign a role to an Azure AD user or group. Use Get-AzADUser or Get-AzADGroup to find the object ID of the user or group.

```powershell
$objectId = "<AzureADUserOrGroupObjectId>"
$roleDefinitionName = "Storage Blob Data Contributor"

New-AzRoleAssignment -ObjectId $objectId -RoleDefinitionName $roleDefinitionName -ResourceName $storageAccountName -ResourceType Microsoft.Storage/storageAccounts -ResourceGroupName $resourceGroupName
```

## Step 5: Test Access

To test access, the Azure AD user must use tools or applications that support Azure AD authentication to access the storage account.