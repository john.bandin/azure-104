# Lab: Configure Private Endpoints using PowerShell

## Objective:
Learn how to create a Private Endpoint in Azure using PowerShell.

## Prerequisites:
- PowerShell Azure module installed
- Azure subscription and permissions to create resources

## Instructions:

### Step 1: Log in to Azure

Open PowerShell and log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Create a new resource group

Create a new resource group to hold the resources for this lab.

```powershell
$rgName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $rgName -Location $location
```

### Step 3: Create a virtual network and subnet

Create a virtual network and a dedicated subnet for the private endpoint.

```powershell
$vnetName = "MyVNet"
$subnetName = "PrivateEndpointSubnet"
$subnetConfig = New-AzVirtualNetworkSubnetConfig -Name $subnetName -AddressPrefix "10.0.0.0/24"
$vnet = New-AzVirtualNetwork -ResourceGroupName $rgName -Location $location -Name $vnetName -AddressPrefix "10.0.0.0/16" -Subnet $subnetConfig
```

### Step 4: Create the private endpoint

Now, create the private endpoint. Replace <serviceResourceId> with the resource ID of the Azure service you want to connect.

```powershell
$privateEndpointName = "MyPrivateEndpoint"
$privateLinkServiceConnection = New-AzPrivateLinkServiceConnection -Name "MyServiceConnection" -PrivateLinkResourceId <serviceResourceId> -Subresource "sqlServer"
$privateEndpoint = New-AzPrivateEndpoint -Name $privateEndpointName -ResourceGroupName $rgName -Location $location -SubnetId $vnet.Subnets[0].Id -ManualRequest $false -PrivateLinkServiceConnection $privateLinkServiceConnection
```

### Step 5: Verify the deployment

Check the deployment and ensure the private endpoint is set up correctly.

```powershell
Get-AzPrivateEndpoint -Name $privateEndpointName -ResourceGroupName $rgName
```

## Conclusion:

You have successfully created a private endpoint using PowerShell. This endpoint now securely connects your virtual network to the Azure service specified.