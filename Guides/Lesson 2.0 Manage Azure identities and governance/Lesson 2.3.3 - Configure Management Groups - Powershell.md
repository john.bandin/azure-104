# Lab 1: Configuring Management Groups using PowerShell

This lab guides you through the process of creating and managing Azure Management Groups using PowerShell.

## Prerequisites

- Azure PowerShell module installed. You can install it by running Install-Module -Name Az -AllowClobber in your PowerShell.

- An Azure account with sufficient permissions to create management groups.

## Step 1  **Login to Azure**

Open your PowerShell console and log in to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2 Create a New Management GroupReplace MyManagementGroup with the desired name for your management group.

```powershell
New-AzManagementGroup -GroupName "MyManagementGroup" -DisplayName "My Management Group"
```

## Step 3 List Management GroupsTo view all the management groups you have access to:

```powershell
Get-AzManagementGroup
```
## Step 4 Add a Subscription to the Management GroupReplace SubscriptionId with your actual subscription ID.

```powershell
Add-AzManagementGroupSubscription -GroupName "MyManagementGroup" -SubscriptionId "YourSubscriptionId"
```

## Step 5 Assign a Policy to the Management GroupReplace PolicyId with the actual ID of the policy you want to assign.

```powershell
New-AzManagementGroupPolicyAssignment -GroupName "MyManagementGroup" -PolicyDefinitionId "YourPolicyId"
```

## Step 6 Verify the ConfigurationConfirm that the subscription has been added and the policy applied.
```powershell
Get-AzManagementGroup -GroupName "MyManagementGroup" -Expand -Recurse
```

## Step 7 Cleanup (Optional)Remove the management group if no longer needed.

```powershell
Remove-AzManagementGroup -GroupName "MyManagementGroup"
```

