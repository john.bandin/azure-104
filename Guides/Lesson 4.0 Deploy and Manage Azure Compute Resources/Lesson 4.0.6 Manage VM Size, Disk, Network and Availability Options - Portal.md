# Azure VM Management Lab using Azure Portal

## Objective
This lab guides you through managing Azure VMs using the Azure Portal. You will learn to change VM sizes, add data disks, configure network settings, and set up availability options.

## Prerequisites
- Azure subscription
- Access to Azure Portal
- Existing VM to configure

## Part 1: Manage VM Sizes

1. Log in to the Azure Portal.
2. Navigate to **Virtual Machines**, then select your VM.
3. Under **Settings**, click on **Size**.
4. Choose a new size from the list and click **Resize**.

## Part 2: Add Data Disks

1. In the Azure Portal, go to your VM's page.
2. Under **Settings**, click on **Disks**.
3. Click on **+ Add data disk**.
4. Select the type of disk you want to add and configure its size, then click **Save**.

## Part 3: Configure VM Network Settings

1. Navigate to **Virtual Networks** in the portal.
2. Create a new virtual network or select an existing one.
3. Add or configure subnets as needed.
4. Go back to your VM, click on **Networking** under **Settings**.
5. Attach your VM to the configured network/subnet.

## Part 4: Configure VM Availability Options

1. Go to **Availability options** in the settings of your VM.
2. Choose between an availability set or an availability zone.
3. If creating a new availability set, provide the required details and click **Create**.
4. Update your VM configuration.

## Conclusion
This lab covered essential VM management tasks using the Azure Portal. For more detailed tasks and options, refer to the official Azure documentation.
