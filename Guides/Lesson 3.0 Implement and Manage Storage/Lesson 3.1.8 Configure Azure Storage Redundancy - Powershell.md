# Lesson 3.1.8: Configuring Azure Storage Redundancy Using PowerShell

![](/Images/Lesson%203.1.8%20Implement%20Azure%20Storage%20redundancy.png)

## Prerequisites

- Azure PowerShell module installed

- An active Azure subscription

## Step 1 **Log in to your Azure Account**

Open PowerShell and run the following command to log into your Azure account.

```powershell
   Connect-AzAccount
```

## Step 2: Create a new resource group

Replace <ResourceGroupName> and <Location> with your desired resource group name and Azure region.

```powershell
New-AzResourceGroup -Name <ResourceGroupName> -Location <Location>
```

## Step 3: Create a storage account with redundancy

Replace <StorageAccountName>, <ResourceGroupName>, and <RedundancyOption> with your desired storage account name, the name of the resource group created in step 2, and the redundancy option (LRS, ZRS, GRS, or GZRS).

```powershell
New-AzStorageAccount -Name <StorageAccountName> -ResourceGroupName <ResourceGroupName> -Location <Location> -SkuName Standard_GRS -Kind StorageV2 -AccessTier Hot -EnableHttpsTrafficOnly $true -Location <Location>
```

## Step 4: Verify the redundancy setting

Confirm the redundancy setting of your storage account.

```powershell
Get-AzStorageAccount -Name <StorageAccountName> -ResourceGroupName <ResourceGroupName>
```