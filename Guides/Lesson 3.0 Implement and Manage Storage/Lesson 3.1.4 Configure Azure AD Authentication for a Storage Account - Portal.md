# Lesson 3.1.4: Configuring Azure AD Authentication Using Azure Portal

![](/Images/Lesson%203.1.4%20Configure%20Azure%20AD%20authentication%20for%20a%20storage%20account%20(1).png)

This lab guides you through the steps to configure Azure AD authentication for an Azure Storage account using the Azure Portal.

## Prerequisites
- Access to the Azure Portal
- An Azure subscription and appropriate permissions to create and manage Azure resources.

## Instructions

## Step 1: Log in to Azure Portal
Open a web browser and navigate to https://portal.azure.com. Log in with your Azure account credentials.

## Step 2: Create a New Storage Account (if you already a storage account created skip this step)
1. In the Azure Portal, search for and select **Storage Accounts**.
2. Click **+ Add** to create a new storage account.
3. Enter the required details such as subscription, resource group, storage account name, and location.
4. Click **Review + create** and then **Create** to deploy the storage account.

## Step 3: Configure role assignments under the storage account

2\. Click on "storage accounts" or search "storage accounts" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/602715e5-a35c-444c-8fe7-46736484f87c/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=347,53)


3\. Click on the "storage account" you want to configure Azure AD authentication for. We will be using my "trepatechstorageaz104" storage account

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/95117e5d-3568-4630-baab-69ad68ff107f/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=113,243)


4\. Next we will click on the "Access Control (IAM)" tab.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/fdc2c822-7e43-4de3-afcb-0ac132572eab/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=139,216)


5\. Then we will click the "+add" button on the top navigation pane and add a "role assignment"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/ba851fed-b465-42b5-a296-aafdc07f85a8/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=216,63)


6\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/fd8638c3-e499-437d-8aca-19fd5748cb70/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=355,148)


7\. Next we will select a job function role to add an Azure AD member. We will set the "storage blob data contributor" job function to a few of our Azure AD users.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/3bbcb82e-06ed-4418-be59-f5e70e834542/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=130,183)


8\. Type "storage blob"


9\. Click "Storage Blob Data Contributor"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/da928421-8a64-4215-8ceb-41b9617bafe8/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=107,251)


10\. Click "Members"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/87c04646-764d-4a1b-9e55-f8fa4dc22444/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=65,95)


11\. Click "Select members"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/7d182711-09fd-4bb2-9c13-21fc19079876/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=165,231)


12\. Now we will select the groups or users we will want to add to this "role assignment"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/faed9c98-28d9-491e-a35e-4ab1ef8f2ed7/ascreenshot.jpeg?tl_px=540,0&br_px=1917,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=842,118)


13\. Click "[testuser01@johnbandintrepatech.onmicrosoft.com](mailto:testuser01@johnbandintrepatech.onmicrosoft.com)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/deef5837-1929-4d0f-9395-823eeaaf35d8/ascreenshot.jpeg?tl_px=197,0&br_px=1917,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=881,231)


14\. Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/1a15069a-35c6-4189-b263-378d5cca45e8/ascreenshot.jpeg?tl_px=197,6&br_px=1917,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=846,567)


15\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/e53126cd-b075-4112-b17a-1eb19fe219a0/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=176,570)


16\. Click "Next"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/c4a0ca19-263e-46ad-a710-9232f0d15de2/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=181,571)


17\. Now we will click "review + assign" to create this role assignment and now we can use Azure AD to authenticate to our Storage Account.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/702343f4-4c4c-4f54-a379-2d045bf2c388/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=46,570)


18\. Now we will Click "Role assignments" to see our new Role assignments for our storage account.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/552019a1-2a7a-489e-b2c9-7e24b75d71b0/ascreenshot.jpeg?tl_px=52,0&br_px=912,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,180)



## Step 4: Test Access
To test access, the Azure AD user must use tools or applications that support Azure AD authentication to access the storage account.

