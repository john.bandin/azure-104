 Lab Exercise: Creating Custom RBAC and Azure AD Roles

## Objective
Learn how to create and assign custom RBAC roles for managing Azure resources and custom roles in Azure Active Directory for managing AD tasks.

## Prerequisites
- Access to the Azure portal with Owner or User Access Administrator permissions.
- Azure PowerShell installed and configured on your machine.

## Part 1: Creating a Custom RBAC Role in Azure

### Step 1: Define the Role
1. Identify the specific permissions needed for the role. For this lab, we'll create a custom role for managing virtual networks only.
2. Open Azure PowerShell and define the role. Replace the placeholders with your specific details.

```powershell
$role = @{
    Name = "Custom Virtual Network Contributor"
    IsCustom = $true
    Description = "Can manage virtual networks and nothing else"
    Actions = [
        "Microsoft.Network/virtualNetworks/*"
    ]
    NotActions = [
    ]
    AssignableScopes = [
        "/subscriptions/{subscription-id}"
    ]
}
New-AzRoleDefinition @role
```

### Step 2: Assign the Role

- Navigate to the Azure portal, go to the specific resource group or resource, select "Access control (IAM)" and then "Add role assignment."
- Choose the "Custom Virtual Network Contributor" role, search for a user or group, and assign the role.

## Conclusion
This lab provided practical experience in creating and managing custom roles in both Azure RBAC and Azure Active Directory. These skills are essential for administrating Azure environments securely and efficiently.

## Additional Tips

- Always test custom roles in a non-production environment to ensure they behave as expected before rolling them out in production.

- Regularly review and update custom roles to adapt to changing requirements and security practices.