# Lesson 3.1.9: Configuring Object Replication Using Azure PowerShell

![](/Images/Lesson%203.1.9%20Configure%20object%20replication.png)

## Objectives:

- Create source and destination storage accounts.

- Configure object replication using Azure PowerShell.

## Prerequisites:

- Azure PowerShell module installed.

- Azure subscription and permissions to create and manage storage accounts.

## Instructions

### Step 1: Log in to Azure

```powershell
Connect-AzAccount
```

### Step 2: Create Resource Groups

```powershell
# Create a resource group for the source storage account
New-AzResourceGroup -Name SourceRG -Location "East US"

# Create a resource group for the destination storage account
New-AzResourceGroup -Name DestinationRG -Location "West US"

```

## Step 3: Create Storage Accounts

```powershell
# Create the source storage account
$sourceStorageAccount = New-AzStorageAccount -ResourceGroupName SourceRG -Name "sourceaccount" -Location "East US" -SkuName "Standard_LRS"

# Create the destination storage account
$destinationStorageAccount = New-AzStorageAccount -ResourceGroupName DestinationRG -Name "destinationaccount" -Location "West US" -SkuName "Standard_LRS"
```

## Step 4: Configure Object Replication

```powershell
# Get the resource ID of the destination storage account
$destResourceId = (Get-AzStorageAccount -ResourceGroupName DestinationRG -AccountName "destinationaccount").Id

# Create a new replication policy
$policy = New-AzStorageAccountObjectReplicationPolicy -ResourceGroupName SourceRG -StorageAccountName "sourceaccount" -DestinationAccountId $destResourceId

# Add a replication rule to the policy
Add-AzStorageAccountObjectReplicationPolicyRule -Policy $policy -SourceContainer "source-container" -DestinationContainer "destination-container"
```

## Step 5: Verify the Replication Settings

```powershell 
# Retrieve and display the replication policy
Get-AzStorageAccountObjectReplicationPolicy -ResourceGroupName SourceRG -StorageAccountName "sourceaccount"
```

