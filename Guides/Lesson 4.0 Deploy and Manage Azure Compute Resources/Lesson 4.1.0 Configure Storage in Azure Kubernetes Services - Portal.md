
# Lab 2: Configuring Storage in AKS using Azure Portal

## Objectives
- Configure Azure Files and integrate it with an AKS cluster using the Azure Portal.

## Prerequisites
- An existing AKS cluster.

## Steps

#### Step 1: Create an Azure File Share
1. Sign in to the Azure Portal.
2. Navigate to **Storage Accounts** and create a new storage account or use an existing one.
3. In the storage account, under **File shares**, click **+ File share**.
4. Enter a name for your file share and specify the quota. Click **Create**.

#### Step 2: Connect to AKS
1. Navigate to your AKS cluster in the Azure Portal.
2. Under **Settings**, click on **Storage Classes** to view or add new storage classes.

#### Step 3: Create a Kubernetes Persistent Volume and Claim
Create a new file `azure-file-pv-pvc.yaml` with the following content:
```yaml
kind: PersistentVolume
apiVersion: v1
metadata:
  name: azurefile
spec:
  capacity:
    storage: 100Gi
  accessModes:
    - ReadWriteMany
  azureFile:
    secretName: azure-secret
    shareName: <your-file-share-name>
    readOnly: false
  storageClassName: azurefile

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: azurefile-pvc
spec:
  accessModes:
    - ReadWriteMany
  storageClassName: azurefile
  resources:
    requests:
      storage: 100Gi
```

Replace <your-file-share-name> with your actual Azure File share name.

#### Step 4: Apply this configuration:

```bash
kubectl apply -f azure-file-pv-pvc.yaml
```

## Conclusion

You have successfully configured Azure Files as a persistent storage option for your AKS cluster.