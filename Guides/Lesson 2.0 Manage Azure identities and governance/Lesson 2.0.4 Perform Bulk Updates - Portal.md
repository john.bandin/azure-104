# Lab Exercise: Performing a Bulk Update in Azure AD Using the Azure Portal

## Objective
This lab exercise will demonstrate how to perform a bulk update of user attributes in Azure Active Directory using the Azure Portal. We will update the "Job Title" and "Department" for multiple users.

## Prerequisites
- Access to the Azure portal with administrative privileges.
- A prepared CSV file with user updates.

## Step 1: Prepare the CSV File
To perform a bulk update, you first need to prepare a CSV file with the required changes. The CSV file should include at least the `UserPrincipalName` to identify the user, along with the attributes you wish to update. You can download the template from the Azure Portal once you click the "bulk" create option.

This is a screenshot of the .csv file downloaded from the azure portal.

![](/Images/Screenshot%202024-04-18%20at%2012.18.50%20AM.png)

## Bulk User Creation in Microsoft Azure Portal


### Step 2: Log in to the Azure Portal**

Open your web browser and navigate to \[<https://portal.azure.com\](https://portal.azure.com)>.

Log in with your administrator credentials.

### Step 3: Navigate to Azure Active Directory

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5bd15340-badd-4db3-bc1f-86e6d63f9c47/user_cropped_screenshot.jpeg?tl_px=172,0&br_px=1891,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,151)


### Step 4: Bulk Update Users

In the Azure Active Directory pane, click on "Users."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/cf667999-106a-4188-9c05-7a00f5e169d2/ascreenshot.jpeg?tl_px=0,125&br_px=1719,1086&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=148,277)


At the top of the "Users" blade, click on "Bulk operations" and then select "Bulk update."

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/7fc77292-7762-44f5-9bb6-6bc7119c00db/ascreenshot.jpeg?tl_px=456,0&br_px=2176,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,159)


Click "Bulk create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2279a68a-367d-4ddc-92df-77c42051cadd/ascreenshot.jpeg?tl_px=444,0&br_px=2164,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,207)


Click on "Download" to get a template CSV file or use your prepared CSV file.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/d647515f-3667-4ccc-9ea6-023b2cd1fff5/ascreenshot.jpeg?tl_px=1734,0&br_px=3454,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=796,201)


Click on "Upload" and select your CSV file with the user updates.

Review the uploaded information to ensure correctness.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/7f6d0def-1963-4f1e-93f0-c0d0af3b8c5a/ascreenshot.jpeg?tl_px=1734,79&br_px=3454,1040&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=813,277)


Click "Submit" to apply the bulk updates.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/33fc07bf-ec78-42c8-a4a7-c533bd887f50/ascreenshot.jpeg?tl_px=1734,940&br_px=3454,1902&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=783,536)


### Step 5: Verify the Updates

After submitting the bulk update, stay on the "Bulk operation results" page to monitor the status of the update.

Once the operation is complete, manually verify a few user accounts from the "Users" page to ensure that their job titles and departments have been updated correctly.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/347f667e-1bfc-476c-934e-fee852d49616/ascreenshot.jpeg?tl_px=570,0&br_px=2290,961&force_format=png&width=1120.0&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=524,145)

## Conclusion
You have successfully performed a bulk update of user attributes in Azure Active Directory. This process can be adapted to update other user attributes as needed.

## Additional Tips
- Always backup user data before performing bulk updates to prevent data loss.
- Regularly review bulk update results to catch and correct any errors in the update process.