# Lab 1: Configure Network Access using PowerShell

## Step 1: Login to Azure

Open your PowerShell window and login to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a new storage account (if necessary)

Replace <resourceGroupName>, <storageAccountName>, and <location> with your desired values.

```powershell
$resourceGroupName = "<resourceGroupName>"
$storageAccountName = "<storageAccountName>"
$location = "<location>"

New-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName -Location $location -SkuName "Standard_LRS"
```

## Step 3: Configure Network Access

Enable a service endpoint for Azure Storage on a subnet.

```powershell
$subnetId = "/subscriptions/{subscriptionId}/resourceGroups/{vnetResourceGroup}/providers/Microsoft.Network/virtualNetworks/{vnetName}/subnets/{subnetName}"
Set-AzVirtualNetworkSubnetConfig -Name {subnetName} -VirtualNetworkName {vnetName} -ResourceGroupName {vnetResourceGroup} -AddressPrefix {subnetAddressPrefix} -ServiceEndpoints Microsoft.Storage
```

Restrict access to the subnet:

```powershell
Update-AzStorageAccountNetworkRuleSet -ResourceGroupName $resourceGroupName -Name $storageAccountName -DefaultAction Deny -VirtualNetworkResourceId $subnetId
```

## Step 4: Verify the configuration

List the network rules to verify that the settings have been applied correctly.

```powershell
Get-AzStorageAccountNetworkRuleSet -ResourceGroupName $resourceGroupName -AccountName $storageAccountName
```
