
# Lab 2: Configuring Azure DNS using the Azure Portal

#### Objectives:
- Set up a DNS zone.
- Create DNS records within the zone.

#### Prerequisites:
- Azure subscription.
- Access to the Azure Portal.

#### Steps:


### Step 1: Log in to Azure Portal
Log in to the Azure Portal at https://portal.azure.com.

### Step 2: Create a Resource Group
Navigate to Resource groups > Add.
- Enter a Resource group name, e.g., `MyResourceGroup`.
- Select your Subscription.
- Select a Region, e.g., `East US`.
- Click `Review + create`, then `Create`.

### Step 3: Create a DNS Zone
Navigate to All services > Networking > DNS zones > Add.
- Enter the Name for your DNS zone, e.g., `mydomain.com`.
- Select the Resource group you created earlier.
- Click `Review + create`, then `Create`.

### Step 4: Add DNS Records
Once your DNS zone is created, click on it to open its overview.
- Click `+ Record set`.
- For an A record, specify:
  - Name: e.g., `www`.
  - Type: `A`.
  - TTL (seconds): `3600`.
  - IP address: `192.168.1.1`.
- Click `OK`.

### Step 5: Verify DNS Records
Go back to the DNS zone overview to see the list of DNS records.

### Conclusion
You have successfully configured Azure DNS using the Azure Portal. Modify the steps as necessary for different types of DNS records.
