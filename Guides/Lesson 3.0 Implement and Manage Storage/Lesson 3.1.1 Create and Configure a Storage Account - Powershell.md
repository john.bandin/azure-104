# Lesson 3.1.1: Creating and Configuring a Storage Account using PowerShell

## Objective
In this lab, you will learn how to create and configure an Azure Storage Account using PowerShell.

## Prerequisites
- Azure PowerShell module installed on your machine.
- You must have an Azure subscription.

## Instructions

### Step 1: Login to Azure

Open PowerShell and log in to your Azure account.

```powershell
Connect-AzAccount
```

## Step 2: Create a Resource Group

Create a new resource group, if you don’t have one already.

```powershell
$resourceGroupName = "MyResourceGroup"
$location = "East US"
New-AzResourceGroup -Name $resourceGroupName -Location $location
```

## Step 3: Create a Storage Account

Now, create a new storage account within the resource group.

```powershell
$storageAccountName = "mystorageaccount$(Get-Random)"
New-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName -Location $location -SkuName "Standard_LRS"
```

## Step 4: Configure the Storage Account

Set the access tier of the storage account to "Hot".

```powershell
Set-AzStorageAccount -ResourceGroupName $resourceGroupName -AccountName $storageAccountName -AccessTier "Hot"
```

## Step 5: Verify the Storage Account Creation

Retrieve the details of the storage account to verify its creation and configurations.

```powershell
Get-AzStorageAccount -ResourceGroupName $resourceGroupName -AccountName $storageAccountName
```

## Conclusion

In this lab, you have learned how to create and configure a storage account using PowerShell. You have set properties such as the access tier and verified the configuration.