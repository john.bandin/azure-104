# Lab 1: Configure Custom Domain Names using PowerShell

This lab guides you through configuring a custom domain name for an Azure service using PowerShell.

## Prerequisites

- Azure PowerShell module installed
- Administrator access to the Azure subscription and domain registrar

### Step 1: Log in to Azure

First, open your PowerShell and log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Set Your Subscription

Specify the subscription where your resource is located.

```powershell
Set-AzContext -Subscription "Your Subscription Name/ID"
```

### Step 3: Get Resource Information

Retrieve information about the resource to which you want to add a custom domain.

```powershell
Get-AzWebApp -ResourceGroupName "YourResourceGroupName"
```

### Step 4: Add Custom Domain

Add the custom domain to your resource.

```powershell
Set-AzWebApp -ResourceGroupName "YourResourceGroupName" -Name "YourAppName" -HostNames @("www.yourcustomdomain.com")
```

### Step 5: Verify Domain Ownership

Add a verification ID as a TXT record in your domain registrar's DNS settings.

```powershell
Type: TXT
Name: @
Value: "MS=msXXXXXXXX"
```

Wait for DNS propagation which may take some time. Verify the domain in Azure using the following command:

```powershell
Confirm-AzWebAppDomainOwnership -ResourceGroupName "YourResourceGroupName" -WebAppName "YourAppName" -DomainName "www.yourcustomdomain.com"
```

## Conclusion

After completing these steps, your custom domain should be successfully configured and verified in Azure.