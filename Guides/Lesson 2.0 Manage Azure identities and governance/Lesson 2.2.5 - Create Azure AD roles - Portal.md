# Part 2: Creating a Custom Azure AD Role


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


### Step 1: Navigate to Azure Active Directory

From the home page of the Azure portal, click on "Azure Active Directory".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/065dcb65-ddfe-4578-8633-59095e93ef5d/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=279,336)


3\. In the Azure AD directory, select "Roles and administrators" from the left-hand menu.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/25fddeb3-68b3-4b81-abcd-40c0f65076fb/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=55,235)

### Step 2: Create the Custom Azure AD Role

Click "+ New custom role" at the top of the page.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/95ad2098-5538-4338-9664-5e122e97ea1a/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=203,66)


5\. Provide a name and description for your role, such as "Custom User Profile Manager".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/77fe9a1e-a8dc-4878-a5a4-9eadd14e294d/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=177,170)


6\. Type "Custom User Profile Manager"


7\. Click "next".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/b5019dfa-0371-4ac6-9e8c-90f992b8e806/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=24,570)


8\. Under "Permissions", click "Add permissions". Select the relevant permissions, such as `microsoft.directory/users/basic/update`, to allow the role to Update basic properties on users

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/40bc1a03-9893-4cd7-9c84-bd9703998eaf/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=127,178)


9\. Type "microsoft.directory/users/"


10\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/2aa3d310-2e9e-4094-933e-54eac1188b08/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-7,491)


11\. microsoft.directory/users/basic/update

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/9d1e1355-db38-4c59-b67a-5459151b390a/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=-6,160)


12\. Click "next".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5ce2cd78-3f0e-4beb-af6a-ef7ee21549e2/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=78,564)


13\. Click "Create" to finalize the custom role.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/57f88c02-a2b1-45c2-8ab2-cd88a82603eb/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=79,571)


### Step 3: Assign the Custom Azure AD Role

Find your newly created role under "Roles and administrators".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/7c3fb4c5-18e3-4efb-8dda-f1b2114adcbf/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=328,264)


15\. Click "Add assignments".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/0844bdb0-b4c6-4612-b0e4-f4b037ce8f75/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=206,63)


16\. Search for and select the users or groups you want to assign the role to.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/a0add43b-2842-4f57-8271-a399af26a5f2/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=375,231)


17\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5d11d28e-4d85-4f05-a6c9-9a1f3ad9a1e2/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=372,233)


18\. Click "Add" to complete the assignment.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-18/5f6b7b38-ac41-41ec-8999-f934703a64cb/ascreenshot.jpeg?tl_px=0,0&br_px=3396,1900&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=378,573)



## Conclusion
This lab provided hands-on experience in creating and assigning custom roles using the Azure portal, equipping participants with the skills needed to manage access effectively within their Azure environment.

## Additional Tips
- Regularly review custom roles and their assignments to ensure they continue to meet your organizational needs.
- Use the "Preview" option to test role permissions before fully deploying them to users.