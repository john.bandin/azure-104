# Lab: Create an App Service Plan using PowerShell

## Objectives
- Create an Azure App Service Plan using Azure PowerShell.

## Prerequisites
- Azure PowerShell module installed.
- An active Azure subscription.

## Instructions

### Step 1: Login to Azure

Open your PowerShell console and login to your Azure account:

```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group

If you do not already have a resource group, create one using the following command. Replace <ResourceGroupName> and <Location> with your desired resource group name and Azure region:

```powershell
New-AzResourceGroup -Name <ResourceGroupName> -Location <Location>
```

### Step 3: Create an App Service Plan

Now, create an App Service Plan. Replace <AppServicePlanName>, <ResourceGroupName>, and <Location> with your values. The Sku parameter defines the pricing tier (e.g., B1, S1, P1):

```powershell
New-AzAppServicePlan -Name <AppServicePlanName> -ResourceGroupName <ResourceGroupName> -Location <Location> -Tier Basic -WorkerSize Small
```

### Step 4: Verify the Creation

To ensure the App Service Plan has been created, use the following command:

```powershell
Get-AzAppServicePlan -ResourceGroupName <ResourceGroupName>
```

## Summary

In this lab, you have successfully created an App Service Plan using PowerShell in Azure. This setup is critical for hosting web apps and other services in Azure.