## Lab: Creating an Azure VM using the Azure Portal

This lab provides a step-by-step guide to create a Virtual Machine (VM) in Microsoft Azure using the Azure Portal.

### Prerequisites
- An active Azure subscription.
- Internet access.

### Step-by-Step Instructions

#### Step 1: Create an Azure Virtual Machine

1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. From the home screen click "Virtual machines". Or search for Virtual Machines in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/0400d2e4-323b-459d-88e6-29d7d72e4503/ascreenshot.jpeg?tl_px=16,0&br_px=963,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,151)


3\. Once you're at the "Virtual Machines" page click "create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d3c44442-4cfb-49c6-a055-8607452b2264/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=26,136)


4\. Click "azure virtual machine". We will be creating a custom machine and setting all the configuration.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3399c807-a034-4f5c-a8cb-0ab9cede342c/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=52,172)


5\. Click on the correct subscription for your environment. In this example I am using "az-104-boot-camp".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/6b56e71a-351a-4cb3-a339-71e27cb37616/ascreenshot.jpeg?tl_px=65,161&br_px=1012,690&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


6\. Now we will click the appropriate resource group or create a new resource group. If you are in our Boot Camp create a new resource group named "trepa-\[your-name\]-vm-rg"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/cb2b9300-7930-4314-b868-e418641cd212/ascreenshot.jpeg?tl_px=0,266&br_px=946,795&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=431,233)


7\. Click "Create new"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/b0fa103d-5c55-4b2b-87db-f55467d2a639/ascreenshot.jpeg?tl_px=0,237&br_px=946,766&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=310,233)


8\. Click the "Name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/984ca482-b625-4453-8ee7-aebdc9c65d21/ascreenshot.jpeg?tl_px=0,374&br_px=946,903&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=335,233)


9\. "trepa-\[your-name\]-vm-rg"


10\. Click "OK"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/dd3363d7-ec45-4b92-abb8-94441f3a99c9/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=337,244)


11\. Click the "Virtual machine name" field. Now we will set the actual Virtual machine name.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/01e4a991-87c5-4bf4-8736-1363485b62f3/ascreenshot.jpeg?tl_px=0,314&br_px=946,843&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=352,233)


12\. Type "my-vm-for-class-\[your-name\]"


13\. Now we will set the region. Select the appropriate region for your deployment. This VM should be deployed in the region geographically closest to you.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/20a3feea-e5c3-4cdf-86fd-dbd35f5e1746/ascreenshot.jpeg?tl_px=0,358&br_px=946,887&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=341,233)


14\. Click "(US) East US"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/937584a4-2249-4f20-a581-29c854bc0824/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=339,128)


15\. Now we will select the Availability Zone we want to use. We will just a single one for our boot camps. Select "Zone 1".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/f136e9fe-6f6b-43b6-a569-451ae7158a7e/ascreenshot.jpeg?tl_px=0,246&br_px=946,775&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=359,233)


16\. Now we will set the Security type. Click "Trusted launch virtual machines"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/94134608-e033-4f0d-b14c-67f53213ac8e/ascreenshot.jpeg?tl_px=0,271&br_px=946,800&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=385,233)


17\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/9dca9053-d9b0-4f75-aa1d-98a3f6a71c67/ascreenshot.jpeg?tl_px=0,371&br_px=946,900&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=365,233)


18\. Now we have to select the appropriate OS/Image. For our Boot camps we will use an Ubuntu Server OS. Click "Ubuntu Server 24.04 LTS - x64 Gen2"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/6d3a6f32-43b7-43b4-878f-58786e61a610/ascreenshot.jpeg?tl_px=0,327&br_px=946,856&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=374,233)


19\. Next we will have to select our "SKU/Size". We will stick with the default size. Click "Standard_D2s_v3 - 2 vcpus, 8 GiB memory ($70.08/month)"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/68f8b91b-8057-4295-8927-cf4b93faed82/ascreenshot.jpeg?tl_px=0,337&br_px=946,866&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=352,233)


20\. Next we will set the username for logging in via SSH to our new VM. We will keep the default username of "azureuser" for our Boot Camps to keep things simple.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/4bd79f1e-b988-4f24-87fd-07875cbbe194/ascreenshot.jpeg?tl_px=0,101&br_px=946,630&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=379,233)


21\. We will leave the default of "generate new key pair" for our SSH sessions.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/a3163b4b-b73b-47f4-ac67-34baeee7a579/ascreenshot.jpeg?tl_px=0,159&br_px=946,688&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=412,233)


22\. Now that we have finished with our VM basic Settings, we will move on to our disk size.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/e161135f-6240-4a8a-944c-559d9eeba2c8/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=205,468)


23\. We will leave everything at default at this screen. Take note of the OS Disk Type and the Disk Size. This is a persistent disk and the increase in size will increase our cost. Next we will move on to the "networking" configuration.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/e325b1fc-ae83-4abe-b219-63a3c2546bd9/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=160,615)


24\. We will also leave things at default for the network settings. During our Networking lessons we will come back and configure advanced networking features and segmentation for our  VMs. Next we will move onto "Management". Click "Next : Management &gt;"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/8d38f110-8881-45bb-a998-47bc2473cdb0/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=174,616)


25\. At the management screen the only thing we will adjust is the "enable auto-shutdown" option. We will set our shutdown time to 6:00pm UST.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/fa326a50-f2e6-4aea-ada2-525962ab0dc7/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=294,272)


26\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d63c5b2a-eee2-4388-af3c-6240b9c7e243/ascreenshot.jpeg?tl_px=0,330&br_px=946,859&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=291,233)


27\. Now we will move onto "monitoring". Click "Monitoring".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d96b4fa5-2ac8-4c47-9be2-e3cf81abe1c9/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=257,453)


28\. We will leave these settings at default. Next we will click "advanced"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/60b4e98e-9c49-4eab-872b-7efe5c151750/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=159,613)


29\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/283009f8-2446-4b1d-9a0b-91d5fc92fb36/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=136,615)


30\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3992ca18-aaa5-4fdd-a697-4c53101f6c93/ascreenshot.jpeg?tl_px=0,0&br_px=1541,934&force_format=jpeg&q=100&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=173,607)


31\. Now we will click "create". A pop-up will appear prompting you to download the "SSH key".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/c071b1a1-6b8c-4584-951a-ac87d09858e5/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=248,450)


32\. Click "Download private key and create resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/fe7f75b1-c32d-4b22-bd2e-311d7dbf5d46/ascreenshot.jpeg?tl_px=203,352&br_px=1150,881&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


33\. The VM will now be provisioned and deployed.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/b3f44959-8689-4b70-be93-48f0c1772b5d/ascreenshot.jpeg?tl_px=0,257&br_px=946,786&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=393,233)

#### Access the Virtual Machine
- Once the deployment is complete, go to the resource and connect to your VM using Remote Desktop Protocol (RDP) available under the **'Connect'** button.

### Conclusion
This completes your Azure VM setup using the Azure portal. You now have a fully functional Windows Server 2019 virtual machine ready for use in your Azure environment.
