## Lesson 2.3.3: Configuring Management Groups using Azure Portal

This lab demonstrates how to create and manage Azure Management Groups using the Azure Portal.

### Prerequisites
- Access to Azure Portal with appropriate permissions.




1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


## Step 2: Navigate to Management Group

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f32dd3b9-dff0-4827-acee-8141a1decbd4/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=468,-11)


3\. In the portal, search for "Management Groups" in the search bar and select it from the results.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/b8062beb-4166-48a0-b961-4856bc446be7/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=475,-23)


## Step 4: Create a New Management Group

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/09645a9c-2495-4fdb-8651-bc08c5241d5e/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=309,126)


5\. Click on "+ Add management group" button. Enter a Management Group ID and display name. Then click "Save".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f76bd460-5612-46af-b4d3-1c730716fc19/ascreenshot.jpeg?tl_px=1057,61&br_px=1917,542&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=456,212)


6\. Click "Submit"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/99930313-3a54-49d2-8709-9ec18b28aa79/ascreenshot.jpeg?tl_px=197,6&br_px=1917,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=567,565)


7\. Click "Refresh"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/d212e280-ae17-4078-9797-dc87fc93758b/ascreenshot.jpeg?tl_px=139,0&br_px=999,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,124)


## Add a Subscription to the Management Group

Click on the newly created management group. Under "Settings", select "Subscriptions", then "Add Subscription". Choose the subscriptions you wish to add and then click "Save".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f8d32ff5-f0ac-4576-86dc-f6d1e43c2ecd/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=245,61)


9\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/29f042eb-6eea-46f9-a1c0-a034dac41e86/ascreenshot.jpeg?tl_px=197,0&br_px=1917,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=830,88)


10\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/4b3d3cb8-78d6-4d03-8c44-be84c4695b0f/ascreenshot.jpeg?tl_px=961,56&br_px=1821,537&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


11\. After choosing which "subscription" to be managed click "save".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/27af9582-e9fb-4abb-8211-fc0afb431fea/ascreenshot.jpeg?tl_px=0,0&br_px=1917,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=630,514)


12\. Click "Management groups"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/caf6752e-a86e-4626-93a3-59b9b659bafb/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=88,10)


13\. Click "Test Management Group 01"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/81a6bc90-7c87-4aae-9d93-8e2d434dea6f/ascreenshot.jpeg?tl_px=0,198&br_px=1376,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=386,296)


14\. Click "Subscriptions"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/ea49be63-83c1-43ac-b02f-75e41841c75f/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=67,189)


15\. And now you can view the subscription that is a part of the management group

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/b0d943ec-1e1a-4ced-9ba1-3cd88ecd8013/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=165,25)
