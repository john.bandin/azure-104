# Lab: Configure a Public Load Balancer Using the Azure Portal

## Objective
Create and configure a public load balancer to distribute traffic among multiple VMs.

## Prerequisites
- Access to Azure Portal with at least Contributor privileges

## Instructions

### Step 1: Log in to Azure Portal

Navigate to `https://portal.azure.com` and sign in with your credentials.

### Step 2: Create a Resource Group

1. Search for and select **Resource groups**.
2. Click **+ Create**.
3. Enter a Resource Group name and select your preferred region.
4. Click **Review + create**, then **Create**.

### Step 3: Create a Public IP Address

1. Search for **Public IP addresses** and select it.
2. Click **+ Create**.
3. Provide the necessary information like Name, SKU, and Assignment (choose Static).
4. Select the Resource Group you created.
5. Click **Review + create**, then **Create**.

### Step 4: Create the Load Balancer

1. Search for **Load balancers** and select it.
2. Click **+ Create**.
3. For the type, choose **Public**.
4. Set up the IP address by selecting the public IP created earlier.
5. Define the backend pool and add VMs to the pool.
6. Define load balancing rules specifying which ports to balance.
7. Click **Review + create**, then **Create**.

### Step 5: Verify the Load Balancer

1. Navigate to **Load balancers**.
2. Select your load balancer to check the settings and the health of the backend pool.

## Conclusion

You have successfully created and configured a public load balancer using the Azure Portal. This load balancer will help distribute incoming traffic to your designated VMs.
