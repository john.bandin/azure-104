# Lab: Using Azure Network Watcher with PowerShell

## Objective
Learn how to enable and use Azure Network Watcher to monitor and diagnose conditions in Azure using PowerShell.

## Prerequisites
- Azure subscription
- Azure PowerShell module installed
- PowerShell session with login to Azure

## Steps

### Step 1: Log in to Azure
Open PowerShell and connect to your Azure account.
```powershell
Connect-AzAccount
```

### Step 2: Enable Network Watcher in Your Region

Check if Network Watcher is enabled in your region; if not, enable it.

```powershell
$location = "East US" # specify your region
$networkWatcher = Get-AzNetworkWatcher -Location $location
if (-not $networkWatcher) {
    $networkWatcher = New-AzNetworkWatcher -Name "NetworkWatcher_$location" -Location $location
}
```

### Step 3: Start Network Packet Capture

Create a packet capture session on a virtual machine.

```powershell
$vm = Get-AzVM -ResourceGroupName "YourResourceGroupName" -Name "YourVMName"
$networkInterface = Get-AzNetworkInterface -ResourceId $vm.NetworkProfile.NetworkInterfaces.Id[0]
New-AzNetworkWatcherPacketCapture -NetworkWatcher $networkWatcher -TargetVirtualMachineId $vm.Id -CaptureLimitInBytes 10000 -CaptureDurationInSeconds 60 -StorageAccountId "/subscriptions/your-subscription-id/resourceGroups/your-resource-group/providers/Microsoft.Storage/storageAccounts/yourstorageaccount"
```

### Step 4: View Packet Capture

Check the status and review the capture results.

```powershell
Get-AzNetworkWatcherPacketCapture -NetworkWatcherName "NetworkWatcher_$location" -ResourceGroupName $networkWatcher.ResourceGroupName
```

### Step 5: Clean Up Resources

Delete packet capture resources if no longer needed.

```powershell
Remove-AzNetworkWatcherPacketCapture -NetworkWatcherName "NetworkWatcher_$location" -ResourceGroupName $networkWatcher.ResourceGroupName
```

## Conclusion

This lab provided a basic walkthrough of how to set up and use Azure Network Watcher with PowerShell to monitor network performance and diagnose issues in Azure.