# Lesson 3.1.3: Configuring SAS Tokens Using PowerShell

- This lab will guide you through the process of generating a Shared Access Signature token for a blob container using PowerShell.

## Prerequisites

- Azure PowerShell module installed. You can install it by running Install-Module -Name Az -AllowClobber in your PowerShell terminal.

- Access to an Azure subscription and appropriate permissions to manage storage accounts.

## Step 1: Login to AzureOpen your PowerShell terminal and log in to your Azure account using the following command:

```powershell
Connect-AzAccount
```

## Step 2: Create a New Storage Account and Blob Container (if necessary)If you already have a storage account and blob container, skip to step 3.

```powershell
$resourceGroupName = 'YourResourceGroupName'
$storageAccountName = 'YourStorageAccountName'
$location = 'YourRegion'
$containerName = 'YourContainerName'

# Create a new resource group if necessary
New-AzResourceGroup -Name $resourceGroupName -Location $location

# Create the storage account
$storageAccount = New-AzStorageAccount -ResourceGroupName $resourceGroupName -Name $storageAccountName -Location $location -SkuName "Standard_LRS"

# Get the context
$ctx = $storageAccount.Context

# Create the blob container
New-AzStorageContainer -Name $containerName -Context $ctx
```

## Step 3: Generate the SAS Token

```powershell
$sasToken = New-AzStorageContainerSASToken -Name $containerName -Context $ctx -Permission "rwlacup" -ExpiryTime (Get-Date).AddHours(24)
Write-Output "SAS Token: $sasToken"
```

This command will generate a SAS token that expires in 24 hours with read, write, list, add, create, update, process permissions.

## Explanation

- Permission "rwlacup" specifies the allowed permissions.
- ExpiryTime (Get-Date).AddHours(24) sets the token to expire 24 hours after creation.


