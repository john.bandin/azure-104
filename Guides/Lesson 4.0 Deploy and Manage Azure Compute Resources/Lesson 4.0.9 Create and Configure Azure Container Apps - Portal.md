## Deploying a Container Image on Azure



1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click the "Search resources, services, and docs (G+/)" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/031e5f76-9464-4c67-a5f5-f3e98950e31e/ascreenshot.jpeg?tl_px=252,0&br_px=1199,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,-5)


3\. Type "container"


4\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/7eed959d-0384-48bb-9377-edd0f90f7964/ascreenshot.jpeg?tl_px=171,0&br_px=1118,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,123)


5\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/878fd61d-e947-4718-8931-0e1c04a2175a/ascreenshot.jpeg?tl_px=0,0&br_px=946,528&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=31,145)


6\. Click this text field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/2c995102-c6d6-4246-8c8a-36a46664bf80/ascreenshot.jpeg?tl_px=0,229&br_px=946,758&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=318,233)


7\. Click "Container image"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/485da8f0-0e92-4b8f-bb0f-c0141dd44b97/ascreenshot.jpeg?tl_px=0,290&br_px=946,819&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=357,233)


8\. Click "Next : Container >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/f07b0a1a-a1c6-44b0-be45-ff858adeb923/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=338,461)


9\. Click "Docker Hub or other registries"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/7d41e8b6-3102-4e3b-aff2-dfdec7cf7fb1/ascreenshot.jpeg?tl_px=0,81&br_px=946,610&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=365,233)


10\. Click the "Example: hello-world:latest" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/cc5778e2-2473-4fe9-b326-913ee08876bc/ascreenshot.jpeg?tl_px=0,275&br_px=946,804&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=341,233)


11\. Click "Consumption - Up to 4 vCPUs, 8 Gib memory"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/0567445c-f985-4e6e-a771-643ee49e8c4f/ascreenshot.jpeg?tl_px=41,336&br_px=988,865&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


12\. Click "Consumption - Up to 4 vCPUs, 8 Gib memory"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/8bd6bf5c-e3e9-4cbd-aee1-dedadd5d23fd/ascreenshot.jpeg?tl_px=22,360&br_px=969,889&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


13\. Click "0.5 CPU cores, 1 Gi memory"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/fb397b43-406e-4461-8663-e19f5471e961/ascreenshot.jpeg?tl_px=15,379&br_px=962,908&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


14\. Click "0.5 CPU cores, 1 Gi memory"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/a185f574-3ad3-444e-a0cb-d9243e796177/ascreenshot.jpeg?tl_px=459,80&br_px=1406,609&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=442,233)


15\. Click "Next : Bindings >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/629abe63-4b23-4ecc-bfee-cdc5664755ea/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=398,462)


16\. Click "Next : Ingress >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/fc97705c-23cb-4197-8b93-f11686ddfec4/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=333,456)


17\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/1efe252c-ee48-480e-ad61-048b58fff907/ascreenshot.jpeg?tl_px=0,11&br_px=946,540&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=283,233)


18\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/3f3af390-5b23-4e59-bb3a-4a133146c490/ascreenshot.jpeg?tl_px=0,16&br_px=946,545&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=279,233)


19\. Click "Next : Tags >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/905adef4-9950-44d6-a288-42075298a76c/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=350,463)


20\. Click "Next : Review + create >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/33ee1afe-e613-4359-aca8-38e3f5e800db/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=361,458)


21\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/80e95abb-1dc6-4c3c-8fe6-9232b5f319e1/ascreenshot.jpeg?tl_px=0,405&br_px=946,934&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=16,461)


22\. Click "Go to resource"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-08-28/d8764727-eaaa-4278-98b4-3ad0c5917f77/ascreenshot.jpeg?tl_px=0,199&br_px=946,728&force_format=jpeg&q=100&width=946&wat_scale=84&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=439,233)
#### [Made with Scribe](https://scribehow.com/shared/Deploying_a_Container_Image_on_Azure__HBHH1PDTRBO8trpAKhNI1w)




## Conclusion
This lab demonstrated how to use the Azure Portal to create and configure a container app. This GUI-based approach offers an intuitive way to manage container settings and monitor deployment.
