# Lesson 2.3.2: Managing Costs Using the Azure Portal

This lab will guide you through setting up budgets and configuring alerts using the Azure portal.

**Prerequisites:**
- Access to the Azure portal with the necessary permissions.

**Objective:**
Use the Azure portal to configure cost management settings such as budgets and alerts.


1\. Navigate to [https://portal.azure.com/?quickstart=True#home](https://portal.azure.com/?quickstart=True#home)


## Step 2: Create a Budget

Click the "cost management" icon or search "cost management" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/b5d0c12a-9d5b-4830-ab81-c3a8e856f97b/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=341,36)


3\. Click "Budgets"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/66740166-0a0d-46cf-b58d-e05b3cd13452/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=27,216)


4\. Click "add"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/21b8b0b5-9b22-4533-9330-74fef1a12367/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=217,89)


5\. Ensure you select the correct scope. In this example we only have one scope.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c1786d98-896b-4771-a11a-e0a823bbc2a9/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=103,206)


6\. Click the "Enter a unique name" field. Here we will create a name for our budget.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/4968589c-3466-4713-bade-7f63e6aaaeb5/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=117,338)


7\. Type "Cheap-Budget-For-AZ104"


8\. Next we will specify the start and end date for our budget.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c82ffa8c-8957-48b5-a09d-2787808f16a6/ascreenshot.jpeg?tl_px=0,394&br_px=859,875&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=231,212)


9\. First we will set the reset period. We will make ours monthly.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/61a9cb23-dc07-410f-974f-7685f7e26229/ascreenshot.jpeg?tl_px=0,422&br_px=859,903&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=227,212)


10\. Next we will set our creation and expiration dates to be one year apart starting on April 2024.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/ab8f0c32-7b5a-40bf-8bc3-5173bc5b348f/ascreenshot.jpeg?tl_px=0,468&br_px=859,949&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=265,212)


11\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/aca1f4ef-c9c9-4f6a-ba77-5fbf5043d0b8/ascreenshot.jpeg?tl_px=0,146&br_px=859,627&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=213,212)


12\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f3c6e51c-8e61-4840-b970-780902a9f619/ascreenshot.jpeg?tl_px=21,461&br_px=880,942&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


13\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/3dd4ee17-35fa-46d5-8bf3-8921c15a608b/ascreenshot.jpeg?tl_px=0,176&br_px=859,657&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=347,212)


14\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/821c7857-00ff-43e7-9e5e-b9e5dbba9d62/ascreenshot.jpeg?tl_px=179,456&br_px=1039,937&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


15\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/c2f42bdc-c50f-45ed-b63a-fef9660efef9/ascreenshot.jpeg?tl_px=82,0&br_px=941,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,87)


16\. Now we will give our budget a dollar amount. We will set our budget to $250.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/fe9d5206-0193-4e5b-aa38-5178d654ab4a/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=237,296)


17\. Type "250"


18\. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/1a29f7b8-ef5a-4bc9-be94-423906f0b134/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=212,371)


19\. Click "Next >"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/4d207a18-1d36-4f28-be83-325f6520695f/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=135,414)


20\. Now we will set the "alert conditions". First we will select the type as "actual".

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/9a793ebe-1abb-4e67-bbc7-ebcf307e7166/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=86,235)


21\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/a2bb8c18-01d2-4725-91f3-3de6873b2c65/ascreenshot.jpeg?tl_px=0,115&br_px=859,596&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=117,212)


22\. Next we will set the percentage of the budget.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/9ce83012-88c5-4fa2-9b71-86dd4bbe2649/ascreenshot.jpeg?tl_px=0,89&br_px=859,570&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=187,212)


23\. Type "75"


24\. Now lets set the alert email to use. I will use a shared inbox email.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/f13e500e-1b67-4a6d-b23e-8ccd14573844/ascreenshot.jpeg?tl_px=0,124&br_px=1376,893&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=99,277)


25\. Type "[contact@trepatech.com](mailto:contact@trepatech.com)"


26\. Click "Default"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/ab74372b-a7c5-4e36-b2aa-afda0d848d9c/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=207,261)


27\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/d6f0699d-9528-4261-b248-f22d48101171/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=114,275)


28\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/90a4a8fc-5608-4483-8e69-03f6f3c40e0e/ascreenshot.jpeg?tl_px=0,487&br_px=859,968&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=121,410)


## Step 3: Explore Cost Recommendations

Navigate to **Advisor** > **Cost** to view personalized recommendations for optimizing your costs.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-19/6d3a80b1-1398-4379-80c9-cd60394ac279/ascreenshot.jpeg?tl_px=0,177&br_px=859,658&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=108,212)

- Review the suggestions provided.
- Implement recommended actions to reduce costs, such as resizing or shutting down underutilized instances.

## Conclusion

You have successfully configured budgets and explored cost optimization recommendations using the Azure portal.
