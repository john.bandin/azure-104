# Lab 1: Configure User-Defined Network Routes using PowerShell

## Objectives
- Create a route table.
- Add a custom route to the route table.
- Associate the route table with a subnet.

## Prerequisites
- You should have the Azure PowerShell module installed.
- You must have at least Contributor access to your Azure subscription.

## Instructions

### Step 1: Login to Azure
Open your PowerShell console and login to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Create a Route Table

Specify the resource group, location, and the name for the route table.

```powershell
$resourceGroup = "YourResourceGroupName"
$location = "YourAzureRegion"
$routeTableName = "MyRouteTable"

New-AzRouteTable -Name $routeTableName -ResourceGroupName $resourceGroup -Location $location
```

### Step 3: Add a Custom Route

Define the address prefix and the next hop type. This example directs traffic destined for 10.1.0.0/16 to a virtual appliance.

```powershell
$addressPrefix = "10.1.0.0/16"
$nextHopType = "VirtualAppliance"
$nextHopIpAddress = "10.0.0.4"  # IP address of your virtual appliance

Add-AzRouteConfig -Name "MyCustomRoute" -RouteTable $routeTableName -AddressPrefix $addressPrefix -NextHopType $nextHopType -NextHopIpAddress $nextHopIpAddress -ResourceGroupName $resourceGroup
Set-AzRouteTable -RouteTable $routeTableName
```

### Step 4: Associate the Route Table with a Subnet

First, get the virtual network and the subnet, then associate the route table.

```powershell
$vnetName = "YourVNetName"
$subnetName = "YourSubnetName"

$vnet = Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup
$subnet = Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork $vnet
Set-AzVirtualNetworkSubnetConfig -VirtualNetwork $vnet -Name $subnetName -AddressPrefix $subnet.AddressPrefix -RouteTable $routeTableName
Set-AzVirtualNetwork -VirtualNetwork $vnet
```

### Step 5: Verify the Configuration

Confirm that the route table is correctly associated with the subnet.

```powershell
Get-AzRouteTable -Name $routeTableName -ResourceGroupName $resourceGroup
```

## Conclusion

This lab has guided you through the process of setting up a user-defined route using Azure PowerShell. You've created a route table, added a custom route, and associated it with a subnet in your virtual network.