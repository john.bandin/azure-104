# Lab 1: Creating and Configuring Azure Container Apps using PowerShell

## Objective
Learn how to create and configure an Azure Container App using PowerShell.

## Prerequisites
- Azure subscription
- Azure PowerShell module installed
- PowerShell 7.0 or higher

## Steps

### Step 1: Login to Azure
```powershell
Connect-AzAccount
```

### Step 2: Create a Resource Group

Replace <ResourceGroupName> and <Location> with your preferred values.

```powershell
$ResourceGroupName = "<ResourceGroupName>"
$Location = "<Location>"
New-AzResourceGroup -Name $ResourceGroupName -Location $Location
```

### Step 3: Create a Container App Environment

```powershell
$EnvironmentName = "MyContainerEnvironment"
New-AzContainerAppEnvironment -Name $EnvironmentName -ResourceGroupName $ResourceGroupName -Location $Location
```

### Step 4: Deploy a Container App

```powershell
$AppName = "MyContainerApp"
$ContainerImage = "mcr.microsoft.com/azuredocs/aci-helloworld"
New-AzContainerApp -Name $AppName -ResourceGroupName $ResourceGroupName -EnvironmentName $EnvironmentName -Image $ContainerImage
```

### Step 5: Configure the App

Set scaling rules and other configurations as needed.

```powershell
Set-AzContainerApp -Name $AppName -ResourceGroupName $ResourceGroupName -ScaleRule "<YourScaleRule>"
```

## Conclusion

This lab demonstrated how to create and configure a basic container app using PowerShell. Further customization can be applied based on specific requirements.