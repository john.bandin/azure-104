# Lab 2: Configuring Azure Monitor for Networks Using the Azure Portal

### Objectives

- Enable network monitoring features.
- Access and analyze network performance data through the Azure Portal.

### Prerequisites

- Access to the Azure Portal.
- Appropriate permissions to manage resources in Azure.

### Instructions

### Step 1: Open Azure Portal

Navigate to [Azure Portal](https://portal.azure.com) and log in with your credentials.

### Step 2: Enable Network Watcher

- Search for **Network Watcher** in the portal search bar and select it.
- Under **Network Watcher**, find your region and click on the **...** next to it, then select **Enable Network Watcher**.

### Step 3: Configure NSG Flow Logs

- Go to the **NSG Flow Logs** tab under Network Watcher.
- Click on the **Add flow log** button.
- Select the NSG you wish to monitor.
- Configure the settings:
  - Set **Storage Account** to save the logs.
  - Enable **Traffic Analytics** if needed.
  - Click **Save**.

### Step 4: Review Network Performance

- Return to the Network Watcher overview.
- Go to **Topology**, **Metrics**, or **Logs** to analyze the network performance and activity.

### Step 5: Set up Alerts (Optional)

- Navigate to **Alerts** under Monitoring in the Azure Portal.
- Set up new alert rules based on your network monitoring needs.

### Step 6: Clean Up Resources

Optional: Remove or disable resources if they are no longer needed.

