
---

# Lab 2: Create and Secure an Azure App Service using Azure Portal

## Prerequisites
- Azure subscription
- Access to Azure Portal

## Objectives
- Create an Azure App Service
- Secure the App Service with HTTPS and authentication

### Step 1: Log in to Azure Portal

Open your web browser and navigate to https://portal.azure.com. Log in with your Azure credentials.

### Step 2: Create a Resource Group

Navigate to Resource groups > Add. Provide the necessary information (Name and Region) and click "Review + create", then "Create".

### Step 3: Create an App Service Plan

Navigate to App Services > Create. Start by setting up an App Service plan:
- Select "Create new" under the App Service plan, then provide a name.
- Choose a Region close to you.
- Select "F1 Free" under Pricing tier.
- Click "Next".

### Step 4: Create an App Service

Continue from the previous steps:
- Enter a unique name for your App Service under "Instance Details".
- Select previously created Resource Group.
- Click "Review + create", then "Create".

### Step 5: Enable HTTPS

Once the App Service is created, navigate to its settings:
- Select "TLS/SSL settings" from the sidebar.
- Turn on "HTTPS Only" to enforce secure connections.

### Step 6: Configure Authentication

Navigate to "Authentication" under the settings:
- Click "Add identity provider".
- Choose "Microsoft" as the identity provider.
- Click "Add" to set up the default authentication method.

## Summary

You have now created and secured an Azure App Service using the Azure Portal. The web application is configured to accept secure connections and require user authentication.
