# Lab 1: Upgrading an AKS Cluster using PowerShell

This lab will guide you through the process of upgrading an AKS cluster using PowerShell commands.

## Prerequisites

- You must have the Azure PowerShell module installed.
- You should have an existing AKS cluster.

### Step 1: Login to Azure

Open PowerShell and run the following command to log in to your Azure account:

```powershell
Connect-AzAccount
```

### Step 2: Set the subscription

Specify the subscription that contains your AKS cluster:

```powershell
$subscriptionId = "<Your-Subscription-ID>"
Set-AzContext -SubscriptionId $subscriptionId
```

### Step 3: Get the current version of your AKS cluster

List the available AKS clusters and identify the current Kubernetes version of your cluster:

```powershell
Get-AzAksCluster -ResourceGroupName <Your-Resource-Group-Name>
```

### Step 4: Check for available upgrades

Check which Kubernetes versions are available for upgrade:

```powershell
Get-AzAksKubernetesVersion -Location <Your-Cluster-Location> -KubernetesVersion ""
```

### Step 5: Upgrade the cluster

Start the upgrade process by specifying the desired Kubernetes version:

```powershell
$clusterName = "<Your-Cluster-Name>"
$resourceGroupName = "<Your-Resource-Group-Name>"
$targetVersion = "<Desired-Kubernetes-Version>"
Start-AzAksUpgrade -ResourceGroupName $resourceGroupName -Name $clusterName -KubernetesVersion $targetVersion
```

### Step 6: Monitor the upgrade

Check the status of the upgrade:

```powershell
Get-AzAksCluster -ResourceGroupName $resourceGroupName -Name $clusterName
```

## Conclusion

After the upgrade is complete, your AKS cluster will be running on the new Kubernetes version.

