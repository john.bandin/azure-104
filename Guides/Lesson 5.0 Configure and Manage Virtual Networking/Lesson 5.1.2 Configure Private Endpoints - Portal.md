# Lab: Configure Private Endpoints using the Azure Portal

## Objective:
Learn how to create a Private Endpoint in Azure using the Azure Portal.

## Prerequisites:
- Azure subscription and permissions to create resources

## Instructions:

### Step 1: Log in to Azure Portal

Log in to the Azure Portal at https://portal.azure.com.

### Step 2: Create a new resource group

Navigate to "Resource groups" and click on "+ Create". Provide the necessary details to create a new resource group.

### Step 3: Create a virtual network and subnet

1. Go to "Virtual networks" and click on "+ Create".
2. Enter the required details, including the address space and a dedicated subnet for the private endpoint.

### Step 4: Create the private endpoint

1. Navigate to "Private endpoints" and click on "+ Create".
2. Follow the wizard to select your newly created virtual network and the dedicated subnet.
3. Select the resource for which you want to create the private endpoint by providing the resource type and resource name.
4. Review and create the private endpoint.

### Step 5: Verify the deployment

After creation, navigate back to "Private endpoints" to see your newly created endpoint and verify that it is connected properly to your specified Azure service.

## Conclusion:

You have successfully created a private endpoint using the Azure Portal, enhancing the security of your services by ensuring private access within your virtual network.
