# Lesson 4.0.5: Moving VMs Using PowerShell

## Objectives
- Learn to move Azure VMs from one resource group to another using PowerShell.

## Prerequisites
- Azure subscription
- PowerShell Azure module installed
- At least one VM created in an Azure resource group

## Instructions

### Step 1: Login to Azure

First, open your PowerShell window and login to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: List Available VMs

It's helpful to know exactly what VMs are available in your subscription:

```powershell
Get-AzVM
```

### Step 3: Move the VM
Identify the source resource group and the target resource group. Then use the Move-AzResource command to move the VM.

Replace <SourceResourceGroupName>, <TargetResourceGroupName>, and <VMName> with your actual resource group names and VM name.

```powershell
$sourceResourceGroup = "<SourceResourceGroupName>"
$targetResourceGroup = "<TargetResourceGroupName>"
$vmResource = Get-AzResource -ResourceGroupName $sourceResourceGroup -ResourceName "<VMName>"
Move-AzResource -DestinationResourceGroupName $targetResourceGroup -ResourceId $vmResource.ResourceId
```

### Step 4: Verify the Move

To confirm that the VM has been moved to the target resource group:

```powershell
Get-AzVM -ResourceGroupName $targetResourceGroup
```

## Conclusion

You have successfully moved a VM from one resource group to another using PowerShell. This process can be replicated for any other Azure resources.