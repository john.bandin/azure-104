# Create a new user in Microsoft Azure portal


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click "Microsoft Entra ID"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/08e065cb-8b24-4efd-a6b0-494f8b23c501/ascreenshot.jpeg?tl_px=424,0&br_px=1284,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,133)


3\. Click "Users"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/242ab1a2-abe3-4e05-ae87-a2d1003515fe/ascreenshot.jpeg?tl_px=0,65&br_px=859,546&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=65,212)


4\. Click "New user"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/b29d6686-269e-4399-9c5d-af58f1fc05c8/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=297,123)


5\. Click "Create new user"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/8baca2ba-a8c2-4cc6-a1bc-66be93b87402/ascreenshot.jpeg?tl_px=0,0&br_px=859,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=341,168)


6\. Create the users "principal name"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/c840f7c3-ee58-4892-998e-31f330e577d4/ascreenshot.jpeg?tl_px=0,74&br_px=859,555&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=318,212)


7\. Create a "display name"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/8f03a3c5-3bd1-4e09-9bd0-243bba041322/ascreenshot.jpeg?tl_px=0,260&br_px=859,741&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=273,212)


8\. Leave the auto-generated password or create your own.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/a60bae65-3d0f-4102-907c-81dd86017d09/ascreenshot.jpeg?tl_px=138,300&br_px=997,781&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


9\. Click "Next: Properties"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/4e4f2461-31e5-458d-9b04-210d5c1b17d7/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=390,420)


10\. Fill in the necessary "properties" fields.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/172e49f9-b076-43f8-8a62-f7d39e3c8cfc/ascreenshot.jpeg?tl_px=0,15&br_px=859,496&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=267,212)


11\. Click "Member"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/1da3d533-fa31-41bd-bdee-a8471df18f2f/ascreenshot.jpeg?tl_px=47,87&br_px=907,568&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,212)


12\. Click "Member"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/8ada362c-3e8f-471f-a719-366b0f691f40/ascreenshot.jpeg?tl_px=0,125&br_px=859,606&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=387,212)


13\. Click "Next: Assignments"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/bac583cd-f455-442c-899f-42ca305bfb73/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=393,428)


14\. Click "Add role"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/f46dc112-d987-456e-af59-ecb3e9de5fe4/ascreenshot.jpeg?tl_px=0,13&br_px=859,494&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=305,212)


15\. Select the appropriate roles for the user

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/5c054cc1-8049-42da-8c28-08246ee870ff/ascreenshot.jpeg?tl_px=670,470&br_px=1530,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,320)


16\. Click "Select"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/a39ec3fb-8c7c-4de1-b44f-a33daaffb902/ascreenshot.jpeg?tl_px=691,470&br_px=1551,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,426)


17\. Click "Next: Review + create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/4e330cff-e1de-41b9-85c6-9e1a6085a9d9/ascreenshot.jpeg?tl_px=87,470&br_px=946,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=402,424)


18\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-17/990c7058-1742-4b51-a640-37697a972855/ascreenshot.jpeg?tl_px=0,470&br_px=859,951&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=0.7&wat_gravity=northwest&wat_url=https://colony-recorder.s3.us-west-1.amazonaws.com/images/watermarks/FB923C_standard.png&wat_pad=69,426)
#### [Made with Scribe](https://scribehow.com/shared/Create_a_new_user_in_Microsoft_Azure_portal__E8fh9m6CR5SW-9m92PASZg)


