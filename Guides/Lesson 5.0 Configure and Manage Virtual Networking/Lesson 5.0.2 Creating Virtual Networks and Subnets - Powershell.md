# Lab: Create and Configure Virtual Network and Subnets Using PowerShell

## Objectives
- Create a Virtual Network
- Create Subnets within the Virtual Network

## Prerequisites
- Azure subscription
- Azure PowerShell module installed

## Instructions

### Step 1: Login to Azure

Open PowerShell and run the following command to log in to Azure:

```powershell
Connect-AzAccount
```

### Step 2: Create a Virtual Network

Use the following PowerShell commands to create a new virtual network:

```powershell
# Define parameters
$resourceGroupName = "MyResourceGroup"
$location = "East US"
$vnetName = "MyVNet"
$addressSpace = "10.0.0.0/16"

# Create resource group
New-AzResourceGroup -Name $resourceGroupName -Location $location

# Create virtual network
$vnet = New-AzVirtualNetwork -ResourceGroupName $resourceGroupName -Location $location -Name $vnetName -AddressPrefix $addressSpace
```

### Step 3: Create Subnets

Now, add subnets to the virtual network:

```powershell
# Define subnet parameters
$subnet1Name = "Subnet1"
$subnet1AddressPrefix = "10.0.1.0/24"
$subnet2Name = "Subnet2"
$subnet2AddressPrefix = "10.0.2.0/24"

# Create subnets
$subnet1 = Add-AzVirtualNetworkSubnetConfig -Name $subnet1Name -AddressPrefix $subnet1AddressPrefix -VirtualNetwork $vnet
$subnet2 = Add-AzVirtualNetworkSubnetConfig -Name $subnet2Name -AddressPrefix $subnet2AddressPrefix -VirtualNetwork $vnet

# Update virtual network with new subnets
Set-AzVirtualNetwork -VirtualNetwork $vnet
```

### Step 4: Verify Configuration

To verify the virtual network and subnets are configured correctly, use the following command:

```powershell
Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroupName
```

## Conclusion

You have successfully created a virtual network and configured two subnets using Azure PowerShell.

