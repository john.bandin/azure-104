# Lab: Configure Azure Application Gateway using the Azure Portal

## Objective:
Learn how to set up Azure Application Gateway using the Azure Portal.

## Prerequisites:
- Azure subscription and permissions to create resources

## Instructions:

### Step 1: Log in to Azure Portal

Log in to the Azure Portal at https://portal.azure.com.

### Step 2: Create a new resource group

Navigate to "Resource groups" and click on "+ Create". Provide the necessary details to create a new resource group.

### Step 3: Create a virtual network and a subnet

1. Go to "Virtual networks" and click on "+ Create".
2. Enter the required details, including the address space and a dedicated subnet for the application gateway.

### Step 4: Create the application gateway

1. Navigate to "Application gateways" and click on "+ Create".
2. Follow the wizard to configure the settings for your application gateway. Include configurations for the listener, rules, backend pools, and health probes as needed.
3. Review and create the application gateway.

### Step 5: Verify the deployment

After creation, navigate back to "Application gateways" to see your newly created gateway and verify that it is configured properly and running.

## Conclusion:

You have successfully configured Azure Application Gateway using the Azure Portal. This gateway now provides advanced routing features and security enhancements for your web applications.
