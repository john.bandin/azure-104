# Configure Network Access using Azure Portal

This lab demonstrates how to configure network access using the Azure Portal.

**Prerequisites:**
- Access to Azure Portal
- You should have an Azure subscription and appropriate permissions to create and manage resources.


1\. Navigate to [https://portal.azure.com/#home](https://portal.azure.com/#home)


2\. Click "Storage accounts" or search for "Storage account" in the search bar.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/7accea20-aedf-4816-81c1-5119b721a9c5/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=434,85)


3\. Click on the "storage account" that you want to configure network access too.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/65bbb274-a003-4414-b930-f499ac08ef71/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=97,261)


4\. Click this button to clean up the page a bit.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/3656f1fe-d34c-49e1-8ad6-e393b40ab5c0/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=145,26)


5\. Scroll down on the left navigation pane and click "networking"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/20204278-a1a1-46ce-a546-79c32cb876f0/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=53,410)


6\. Here we will configure some custom networking options for our "storage account"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/697b2cf7-ca94-4e9a-ae5e-960133d61ba0/ascreenshot.jpeg?tl_px=0,0&br_px=1376,769&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=238,204)


7\. If you don't have any virtual network configured already, you can create a new one here. We will be creating a new Virtual Network for this specific purpose.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/e3568c45-6bb8-435b-8dc0-88d0805f8cd9/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=333,273)


8\. Under the "create virtual network" page we will fill in the following information:

- Unique name
- The address space that will be apart of thr RFC1918 address space I.E. 10.1.0.0/24 for example
- The correct subscription we want to use for this storage account and VNET
- The correct resource we want to use for this storage account and VNET

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/54cf1744-9bca-41af-8bb9-8a356edccddb/ascreenshot.jpeg?tl_px=197,0&br_px=1917,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=798,59)


9\. Type "VNET-for-Storagetest01"


10\. Click the "Address space" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/a27ccea4-e4c7-42dc-b304-eb665513cb3b/ascreenshot.jpeg?tl_px=1057,0&br_px=1917,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=450,168)


11\. Click the "Address space" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/a10ed237-0cd0-41f7-bb72-0721eab29c90/ascreenshot.jpeg?tl_px=1057,0&br_px=1917,480&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=423,176)


12\. Click "Trepa AZ-104 Training"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/69108b4a-c49d-42c1-a26f-7cb7b05d0726/ascreenshot.jpeg?tl_px=1057,36&br_px=1917,517&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=428,212)


13\. Click here.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/3581e5cb-51ca-4e20-bfd1-1e38e4123d54/ascreenshot.jpeg?tl_px=1057,156&br_px=1917,637&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=409,212)


14\. Click the "Name" field.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/8206af36-b63c-4d26-a018-9c0160d6554d/ascreenshot.jpeg?tl_px=1013,251&br_px=1873,732&force_format=png&width=860&wat_scale=76&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=402,212)


15\. Type "SecureStorageTest01"


16\. Click "Create"

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/480d9cc6-3b23-4445-be1c-ec08bfda4bff/ascreenshot.jpeg?tl_px=919,418&br_px=1902,968&force_format=png&width=983&wat_scale=87&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=459,494)


17\. Now we can see that this VNET was added to this storage space. 

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/747a8f07-b046-4ea9-b33c-9b6ad21f7ca4/ascreenshot.jpeg?tl_px=0,0&br_px=1917,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=163,288)


18\. We have another option to allow the Public IP address that we are connected to, be allowed to access this storage account.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/4e2941fd-7e33-475a-a2d8-f181ad9dec47/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=188,370)


19\. We have another additional option to add Resource Types to our storage account to give us more granular permissions.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/0811b678-448e-4c1d-bc41-71971a80191f/ascreenshot.jpeg?tl_px=0,6&br_px=1719,968&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=303,374)


20\. Now to complete and finish these change we will click "save" in the top navigation pane.

![](https://ajeuwbhvhr.cloudimg.io/colony-recorder.s3.amazonaws.com/files/2024-04-20/5a89c1d6-5657-472a-8b2f-9c6dcb688dec/ascreenshot.jpeg?tl_px=0,0&br_px=1719,961&force_format=png&width=1120.0&wat=1&wat_opacity=1&wat_gravity=northwest&wat_url=https://colony-recorder.s3.amazonaws.com/images/watermarks/0EA5E9_standard.png&wat_pad=203,100)


## Step 5: Verify the configuration

Ensure that the settings are applied correctly by accessing the storage account from an allowed IP or through the virtual network.
