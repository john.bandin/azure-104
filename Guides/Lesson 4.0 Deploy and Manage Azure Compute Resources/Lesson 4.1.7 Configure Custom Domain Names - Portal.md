
# Lab 2: Configure Custom Domain Names using the Azure Portal

This lab demonstrates how to configure a custom domain name for an Azure service using the Azure Portal.

## Prerequisites
- Access to the Azure portal
- Administrator access to the domain registrar

### Step 1: Log in to Azure Portal

Go to the Azure Portal (https://portal.azure.com) and sign in with your credentials.

### Step 2: Navigate to Your Resource

Find the resource (e.g., Azure App Service) where you want to add the custom domain.

1. In the portal's search bar, type and select your service.
2. Click on the specific resource.

### Step 3: Add Custom Domain

1. In the resource menu, click on **Custom domains**.
2. Click **+ Add custom domain**.

### Step 4: Enter Your Domain

Type your custom domain name in the provided field and click **Validate**. This checks if the domain is available and prompts you for verification.

### Step 5: Verify Domain Ownership

Azure will provide you with a TXT or MX record to add to your DNS settings at your domain registrar.

- Add the record provided by Azure.
- Once added, return to the Azure portal and click **Verify**.

### Step 6: SSL/TLS Certificate Configuration

After verification, configure SSL/TLS to secure your domain.

1. Under **Custom domains**, select your domain.
2. Click on **Add binding** under the SSL settings.
3. Choose your certificate or upload a new one and save the settings.

## Conclusion

Your custom domain is now configured and secured. Verify by navigating to your custom domain in a web browser.
