# Lab 1: Creating an Azure File Share using PowerShell

![](/Images/Lesson%203.2.1%20Configure%20object%20replication.png)

## Objectives:

- Connect to Azure with PowerShell.

- Create a new storage account.

- Create a file share in the storage account.

## Prerequisites:

- PowerShell Azure module installed.

- An active Azure subscription.

### Step 1: Install Azure PowerShell Module

If you haven't installed the Azure PowerShell module yet, run the following command in your PowerShell:

```powershell
Install-Module -Name Az -AllowClobber -Scope CurrentUser
```

### Step 2: Connect to Azure

Login to your Azure account:

```powershell
Connect-AzAccount
```

### Step 3: Create a Storage Account

Replace <ResourceGroupName>, <StorageAccountName>, and <Location> with your desired values.

```powershell
$resourceGroup = "<ResourceGroupName>"
$storageAccountName = "<StorageAccountName>"
$location = "<Location>"

# Create the storage account
New-AzStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName -Location $location -SkuName Standard_LRS
```

### Step 4: Create a File Share

Replace <FileShareName> with the name you want for your file share.

```powershell
$fileShareName = "<FileShareName>"
$storageAccount = Get-AzStorageAccount -ResourceGroupName $resourceGroup -Name $storageAccountName

# Create the file share
New-AzStorageShare -Name $fileShareName -Context $storageAccount.Context
```

## Conclusion

You've now successfully created an Azure File Share using PowerShell.