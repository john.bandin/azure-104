# Lab: Configure Service Endpoints using PowerShell

## Objectives
In this lab, you will learn how to configure service endpoints on a subnet within an Azure Virtual Network using PowerShell.

## Prerequisites
- You should have the Azure PowerShell module installed.
- You must have an existing Azure Virtual Network and a subnet.

## Instructions

### Step 1: Login to Azure
First, you need to log in to your Azure account.

```powershell
Connect-AzAccount
```

### Step 2: Declare Variables

Replace <YourResourceGroupName>, <YourVNetName>, and <YourSubnetName> with your actual resource group, virtual network, and subnet names.

```powershell
$resourceGroup = "<YourResourceGroupName>"
$vnetName = "<YourVNetName>"
$subnetName = "<YourSubnetName>"
```

### Step 3: Get the Subnet

Retrieve the subnet where the service endpoint will be added.

```powershell
$subnet = Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork (Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup)
```

### Step 4: Add the Service Endpoint

You can add a service endpoint for Azure SQL Database as an example. For other services, replace Microsoft.Sql with the appropriate service.

```powershell
$subnet.ServiceEndpoints.Add("Microsoft.Sql")
Set-AzVirtualNetworkSubnetConfig -VirtualNetwork (Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup) -Name $subnetName -AddressPrefix $subnet.AddressPrefix -ServiceEndpoints $subnet.ServiceEndpoints
```

### Step 5: Update the Virtual Network

Apply the changes to your virtual network.

```powershell
Set-AzVirtualNetwork -VirtualNetwork $subnet.VirtualNetwork
```

### Step 6:  Verify the Configuration

Check that the service endpoint has been added.

```powershell
$updatedSubnet = Get-AzVirtualNetworkSubnetConfig -Name $subnetName -VirtualNetwork (Get-AzVirtualNetwork -Name $vnetName -ResourceGroupName $resourceGroup)
$updatedSubnet.ServiceEndpoints
```

## Conclusion

You have successfully configured an Azure Service Endpoint using PowerShell. This will help secure your Azure resources by limiting access to them only from your specified virtual network.